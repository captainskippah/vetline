<?php

use Carbon\Carbon;
use App\Models\Pet;
use App\Models\Cage;
use App\Models\Phone;
use App\Models\Medical;
use App\Models\Vaccine;
use App\Models\Barangay;
use App\Models\Customer;
use App\Models\Procedure;
use App\Models\Treatment;
use App\Models\Appointment;
use App\Models\Auth\Employee;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Customer::class, function (Faker\Generator $faker) {
	return [
		'firstname' => $faker->firstName,
		'middlename' => $faker->firstName,
		'lastname' => $faker->lastName,
		'password' => bcrypt('secret'),
		'username' => $faker->username . rand(1,99)
	];
});

$factory->define(App\Models\Phone::class, function (Faker\Generator $faker) {
	return [
		'customer_id' => function () {
			return factory(Customer::class)->create()->id;
		},
		'number' => $faker->e164PhoneNumber,
	];
});

$factory->define(App\Models\Address::class, function (Faker\Generator $faker) {
	return [
		'customer_id' => function () {
			return factory(Customer::class)->create()->id;
		},
		'street' => $faker->streetAddress(),
		'barangay_id' => Barangay::where('citymunCode', '35401')->inRandomOrder()->first()->brgyCode	
	];
});

$factory->define(App\Models\Pet::class, function (Faker\Generator $faker) {
	$species = ['canine', 'feline', $faker->word];

	return [
		'customer_id' => function () {
			return factory(Customer::class)->create()->id;
		},
		
		'name' => $faker->firstName,

		'species' => $species[rand(0,2)],
		'breed' => $faker->word,

		'birthday' => $faker->date(),
		'gender' => rand(1,2),
		'weight' => number_format(rand(20,50), 2)
	];
});

$factory->define(Cage::class, function (Faker\Generator $faker) {
	return [
		'status_id' => Cage::AVAILABLE,
		'pet_id' => null
	];
});

$factory->state(Cage::class, 'boarded', function () {
	return [
		'status_id' => Cage::BOARDED,
		'pet_id' => function () {
			return factory(Pet::class)->create()->id;
		}
	];
});

$factory->state(Cage::class, 'reserved', function () {
	return [
		'status_id' => Cage::RESERVED,
		'pet_id' => function () {
			return factory(Pet::class)->create()->id;
		},
		'customer_id' => function () {
			return factory(Customer::class)->create()->id;
		}
	];
});

$factory->define(Appointment::class, function (Faker\Generator $faker) {
	$start = Carbon::instance($faker->dateTimeBetween('-2 months', '2 months'));
	$status = rand(0,2);
	$scheduled = Appointment::STATUS[$status] === 'scheduled';

	return [
		'pet_id' => function () {
			return factory(Pet::class)->create()->id;
		},
		'status_id' => $status,
		'notes' => $faker->sentence(),
		'start_time' => $start->toIso8601String(),
		'end_time' => $scheduled ? null : $start->addMinutes(rand(10,20))->toIso8601String()
	];
});

$factory->define(Procedure::class, function (Faker\Generator $faker) {
	return [
		'name' => $faker->word()
	];
});

$factory->define(Treatment::class, function (Faker\Generator $faker) {
	return [
		'name' => $faker->word()
	];
});

$factory->define(Vaccine::class, function (Faker\Generator $faker) {
	return [
		'medical_id' => function () {
			return factory(Medical::class)->create()->id;
		},
		'name' => $faker->word(),
		'next' => $faker->dateTimeBetween('1 month', '2 months')->format('M d, Y')
	];
});

$factory->define(Medical::class, function (Faker\Generator $faker) {
	return [
		'author_id' => function () {
			return factory(Employee::class)->create()->id;
		},
		'pet_id' => function () {
			return factory(Pet::class)->create()->id;
		},

		'complaint' => $faker->sentence(),
		'history' => $faker->sentence(),
		'observation' => "<p>{$faker->paragraph()}</p>",
		'diagnosis' => $faker->sentence(),
		'date' => $faker->dateTimeBetween('-1 month', '1 month')->format(\DateTime::ATOM)
	];
});

$factory->define(Employee::class, function (Faker\Generator $faker) {
	return [
		'username' => $faker->username(),
		'password' => $faker->password(),
		'firstname' => $faker->firstname(),
		'lastname' => $faker->lastname()
	];
});
