<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cities')->delete();
        
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'citymunCode' => 12801,
                'citymunDesc' => 'ADAMS',
                'provCode' => 128,
            ),
            1 => 
            array (
                'citymunCode' => 12802,
                'citymunDesc' => 'BACARRA',
                'provCode' => 128,
            ),
            2 => 
            array (
                'citymunCode' => 12803,
                'citymunDesc' => 'BADOC',
                'provCode' => 128,
            ),
            3 => 
            array (
                'citymunCode' => 12804,
                'citymunDesc' => 'BANGUI',
                'provCode' => 128,
            ),
            4 => 
            array (
                'citymunCode' => 12805,
                'citymunDesc' => 'CITY OF BATAC',
                'provCode' => 128,
            ),
            5 => 
            array (
                'citymunCode' => 12806,
                'citymunDesc' => 'BURGOS',
                'provCode' => 128,
            ),
            6 => 
            array (
                'citymunCode' => 12807,
                'citymunDesc' => 'CARASI',
                'provCode' => 128,
            ),
            7 => 
            array (
                'citymunCode' => 12808,
                'citymunDesc' => 'CURRIMAO',
                'provCode' => 128,
            ),
            8 => 
            array (
                'citymunCode' => 12809,
                'citymunDesc' => 'DINGRAS',
                'provCode' => 128,
            ),
            9 => 
            array (
                'citymunCode' => 12810,
                'citymunDesc' => 'DUMALNEG',
                'provCode' => 128,
            ),
            10 => 
            array (
                'citymunCode' => 12811,
            'citymunDesc' => 'BANNA (ESPIRITU)',
                'provCode' => 128,
            ),
            11 => 
            array (
                'citymunCode' => 12812,
            'citymunDesc' => 'LAOAG CITY (Capital)',
                'provCode' => 128,
            ),
            12 => 
            array (
                'citymunCode' => 12813,
                'citymunDesc' => 'MARCOS',
                'provCode' => 128,
            ),
            13 => 
            array (
                'citymunCode' => 12814,
                'citymunDesc' => 'NUEVA ERA',
                'provCode' => 128,
            ),
            14 => 
            array (
                'citymunCode' => 12815,
                'citymunDesc' => 'PAGUDPUD',
                'provCode' => 128,
            ),
            15 => 
            array (
                'citymunCode' => 12816,
                'citymunDesc' => 'PAOAY',
                'provCode' => 128,
            ),
            16 => 
            array (
                'citymunCode' => 12817,
                'citymunDesc' => 'PASUQUIN',
                'provCode' => 128,
            ),
            17 => 
            array (
                'citymunCode' => 12818,
                'citymunDesc' => 'PIDDIG',
                'provCode' => 128,
            ),
            18 => 
            array (
                'citymunCode' => 12819,
                'citymunDesc' => 'PINILI',
                'provCode' => 128,
            ),
            19 => 
            array (
                'citymunCode' => 12820,
                'citymunDesc' => 'SAN NICOLAS',
                'provCode' => 128,
            ),
            20 => 
            array (
                'citymunCode' => 12821,
                'citymunDesc' => 'SARRAT',
                'provCode' => 128,
            ),
            21 => 
            array (
                'citymunCode' => 12822,
                'citymunDesc' => 'SOLSONA',
                'provCode' => 128,
            ),
            22 => 
            array (
                'citymunCode' => 12823,
                'citymunDesc' => 'VINTAR',
                'provCode' => 128,
            ),
            23 => 
            array (
                'citymunCode' => 12901,
                'citymunDesc' => 'ALILEM',
                'provCode' => 129,
            ),
            24 => 
            array (
                'citymunCode' => 12902,
                'citymunDesc' => 'BANAYOYO',
                'provCode' => 129,
            ),
            25 => 
            array (
                'citymunCode' => 12903,
                'citymunDesc' => 'BANTAY',
                'provCode' => 129,
            ),
            26 => 
            array (
                'citymunCode' => 12904,
                'citymunDesc' => 'BURGOS',
                'provCode' => 129,
            ),
            27 => 
            array (
                'citymunCode' => 12905,
                'citymunDesc' => 'CABUGAO',
                'provCode' => 129,
            ),
            28 => 
            array (
                'citymunCode' => 12906,
                'citymunDesc' => 'CITY OF CANDON',
                'provCode' => 129,
            ),
            29 => 
            array (
                'citymunCode' => 12907,
                'citymunDesc' => 'CAOAYAN',
                'provCode' => 129,
            ),
            30 => 
            array (
                'citymunCode' => 12908,
                'citymunDesc' => 'CERVANTES',
                'provCode' => 129,
            ),
            31 => 
            array (
                'citymunCode' => 12909,
                'citymunDesc' => 'GALIMUYOD',
                'provCode' => 129,
            ),
            32 => 
            array (
                'citymunCode' => 12910,
            'citymunDesc' => 'GREGORIO DEL PILAR (CONCEPCION)',
                'provCode' => 129,
            ),
            33 => 
            array (
                'citymunCode' => 12911,
                'citymunDesc' => 'LIDLIDDA',
                'provCode' => 129,
            ),
            34 => 
            array (
                'citymunCode' => 12912,
                'citymunDesc' => 'MAGSINGAL',
                'provCode' => 129,
            ),
            35 => 
            array (
                'citymunCode' => 12913,
                'citymunDesc' => 'NAGBUKEL',
                'provCode' => 129,
            ),
            36 => 
            array (
                'citymunCode' => 12914,
                'citymunDesc' => 'NARVACAN',
                'provCode' => 129,
            ),
            37 => 
            array (
                'citymunCode' => 12915,
            'citymunDesc' => 'QUIRINO (ANGKAKI)',
                'provCode' => 129,
            ),
            38 => 
            array (
                'citymunCode' => 12916,
            'citymunDesc' => 'SALCEDO (BAUGEN)',
                'provCode' => 129,
            ),
            39 => 
            array (
                'citymunCode' => 12917,
                'citymunDesc' => 'SAN EMILIO',
                'provCode' => 129,
            ),
            40 => 
            array (
                'citymunCode' => 12918,
                'citymunDesc' => 'SAN ESTEBAN',
                'provCode' => 129,
            ),
            41 => 
            array (
                'citymunCode' => 12919,
                'citymunDesc' => 'SAN ILDEFONSO',
                'provCode' => 129,
            ),
            42 => 
            array (
                'citymunCode' => 12920,
            'citymunDesc' => 'SAN JUAN (LAPOG)',
                'provCode' => 129,
            ),
            43 => 
            array (
                'citymunCode' => 12921,
                'citymunDesc' => 'SAN VICENTE',
                'provCode' => 129,
            ),
            44 => 
            array (
                'citymunCode' => 12922,
                'citymunDesc' => 'SANTA',
                'provCode' => 129,
            ),
            45 => 
            array (
                'citymunCode' => 12923,
                'citymunDesc' => 'SANTA CATALINA',
                'provCode' => 129,
            ),
            46 => 
            array (
                'citymunCode' => 12924,
                'citymunDesc' => 'SANTA CRUZ',
                'provCode' => 129,
            ),
            47 => 
            array (
                'citymunCode' => 12925,
                'citymunDesc' => 'SANTA LUCIA',
                'provCode' => 129,
            ),
            48 => 
            array (
                'citymunCode' => 12926,
                'citymunDesc' => 'SANTA MARIA',
                'provCode' => 129,
            ),
            49 => 
            array (
                'citymunCode' => 12927,
                'citymunDesc' => 'SANTIAGO',
                'provCode' => 129,
            ),
            50 => 
            array (
                'citymunCode' => 12928,
                'citymunDesc' => 'SANTO DOMINGO',
                'provCode' => 129,
            ),
            51 => 
            array (
                'citymunCode' => 12929,
                'citymunDesc' => 'SIGAY',
                'provCode' => 129,
            ),
            52 => 
            array (
                'citymunCode' => 12930,
                'citymunDesc' => 'SINAIT',
                'provCode' => 129,
            ),
            53 => 
            array (
                'citymunCode' => 12931,
                'citymunDesc' => 'SUGPON',
                'provCode' => 129,
            ),
            54 => 
            array (
                'citymunCode' => 12932,
                'citymunDesc' => 'SUYO',
                'provCode' => 129,
            ),
            55 => 
            array (
                'citymunCode' => 12933,
                'citymunDesc' => 'TAGUDIN',
                'provCode' => 129,
            ),
            56 => 
            array (
                'citymunCode' => 12934,
            'citymunDesc' => 'CITY OF VIGAN (Capital)',
                'provCode' => 129,
            ),
            57 => 
            array (
                'citymunCode' => 13301,
                'citymunDesc' => 'AGOO',
                'provCode' => 133,
            ),
            58 => 
            array (
                'citymunCode' => 13302,
                'citymunDesc' => 'ARINGAY',
                'provCode' => 133,
            ),
            59 => 
            array (
                'citymunCode' => 13303,
                'citymunDesc' => 'BACNOTAN',
                'provCode' => 133,
            ),
            60 => 
            array (
                'citymunCode' => 13304,
                'citymunDesc' => 'BAGULIN',
                'provCode' => 133,
            ),
            61 => 
            array (
                'citymunCode' => 13305,
                'citymunDesc' => 'BALAOAN',
                'provCode' => 133,
            ),
            62 => 
            array (
                'citymunCode' => 13306,
                'citymunDesc' => 'BANGAR',
                'provCode' => 133,
            ),
            63 => 
            array (
                'citymunCode' => 13307,
                'citymunDesc' => 'BAUANG',
                'provCode' => 133,
            ),
            64 => 
            array (
                'citymunCode' => 13308,
                'citymunDesc' => 'BURGOS',
                'provCode' => 133,
            ),
            65 => 
            array (
                'citymunCode' => 13309,
                'citymunDesc' => 'CABA',
                'provCode' => 133,
            ),
            66 => 
            array (
                'citymunCode' => 13310,
                'citymunDesc' => 'LUNA',
                'provCode' => 133,
            ),
            67 => 
            array (
                'citymunCode' => 13311,
                'citymunDesc' => 'NAGUILIAN',
                'provCode' => 133,
            ),
            68 => 
            array (
                'citymunCode' => 13312,
                'citymunDesc' => 'PUGO',
                'provCode' => 133,
            ),
            69 => 
            array (
                'citymunCode' => 13313,
                'citymunDesc' => 'ROSARIO',
                'provCode' => 133,
            ),
            70 => 
            array (
                'citymunCode' => 13314,
            'citymunDesc' => 'CITY OF SAN FERNANDO (Capital)',
                'provCode' => 133,
            ),
            71 => 
            array (
                'citymunCode' => 13315,
                'citymunDesc' => 'SAN GABRIEL',
                'provCode' => 133,
            ),
            72 => 
            array (
                'citymunCode' => 13316,
                'citymunDesc' => 'SAN JUAN',
                'provCode' => 133,
            ),
            73 => 
            array (
                'citymunCode' => 13317,
                'citymunDesc' => 'SANTO TOMAS',
                'provCode' => 133,
            ),
            74 => 
            array (
                'citymunCode' => 13318,
                'citymunDesc' => 'SANTOL',
                'provCode' => 133,
            ),
            75 => 
            array (
                'citymunCode' => 13319,
                'citymunDesc' => 'SUDIPEN',
                'provCode' => 133,
            ),
            76 => 
            array (
                'citymunCode' => 13320,
                'citymunDesc' => 'TUBAO',
                'provCode' => 133,
            ),
            77 => 
            array (
                'citymunCode' => 15501,
                'citymunDesc' => 'AGNO',
                'provCode' => 155,
            ),
            78 => 
            array (
                'citymunCode' => 15502,
                'citymunDesc' => 'AGUILAR',
                'provCode' => 155,
            ),
            79 => 
            array (
                'citymunCode' => 15503,
                'citymunDesc' => 'CITY OF ALAMINOS',
                'provCode' => 155,
            ),
            80 => 
            array (
                'citymunCode' => 15504,
                'citymunDesc' => 'ALCALA',
                'provCode' => 155,
            ),
            81 => 
            array (
                'citymunCode' => 15505,
                'citymunDesc' => 'ANDA',
                'provCode' => 155,
            ),
            82 => 
            array (
                'citymunCode' => 15506,
                'citymunDesc' => 'ASINGAN',
                'provCode' => 155,
            ),
            83 => 
            array (
                'citymunCode' => 15507,
                'citymunDesc' => 'BALUNGAO',
                'provCode' => 155,
            ),
            84 => 
            array (
                'citymunCode' => 15508,
                'citymunDesc' => 'BANI',
                'provCode' => 155,
            ),
            85 => 
            array (
                'citymunCode' => 15509,
                'citymunDesc' => 'BASISTA',
                'provCode' => 155,
            ),
            86 => 
            array (
                'citymunCode' => 15510,
                'citymunDesc' => 'BAUTISTA',
                'provCode' => 155,
            ),
            87 => 
            array (
                'citymunCode' => 15511,
                'citymunDesc' => 'BAYAMBANG',
                'provCode' => 155,
            ),
            88 => 
            array (
                'citymunCode' => 15512,
                'citymunDesc' => 'BINALONAN',
                'provCode' => 155,
            ),
            89 => 
            array (
                'citymunCode' => 15513,
                'citymunDesc' => 'BINMALEY',
                'provCode' => 155,
            ),
            90 => 
            array (
                'citymunCode' => 15514,
                'citymunDesc' => 'BOLINAO',
                'provCode' => 155,
            ),
            91 => 
            array (
                'citymunCode' => 15515,
                'citymunDesc' => 'BUGALLON',
                'provCode' => 155,
            ),
            92 => 
            array (
                'citymunCode' => 15516,
                'citymunDesc' => 'BURGOS',
                'provCode' => 155,
            ),
            93 => 
            array (
                'citymunCode' => 15517,
                'citymunDesc' => 'CALASIAO',
                'provCode' => 155,
            ),
            94 => 
            array (
                'citymunCode' => 15518,
                'citymunDesc' => 'DAGUPAN CITY',
                'provCode' => 155,
            ),
            95 => 
            array (
                'citymunCode' => 15519,
                'citymunDesc' => 'DASOL',
                'provCode' => 155,
            ),
            96 => 
            array (
                'citymunCode' => 15520,
                'citymunDesc' => 'INFANTA',
                'provCode' => 155,
            ),
            97 => 
            array (
                'citymunCode' => 15521,
                'citymunDesc' => 'LABRADOR',
                'provCode' => 155,
            ),
            98 => 
            array (
                'citymunCode' => 15522,
            'citymunDesc' => 'LINGAYEN (Capital)',
                'provCode' => 155,
            ),
            99 => 
            array (
                'citymunCode' => 15523,
                'citymunDesc' => 'MABINI',
                'provCode' => 155,
            ),
            100 => 
            array (
                'citymunCode' => 15524,
                'citymunDesc' => 'MALASIQUI',
                'provCode' => 155,
            ),
            101 => 
            array (
                'citymunCode' => 15525,
                'citymunDesc' => 'MANAOAG',
                'provCode' => 155,
            ),
            102 => 
            array (
                'citymunCode' => 15526,
                'citymunDesc' => 'MANGALDAN',
                'provCode' => 155,
            ),
            103 => 
            array (
                'citymunCode' => 15527,
                'citymunDesc' => 'MANGATAREM',
                'provCode' => 155,
            ),
            104 => 
            array (
                'citymunCode' => 15528,
                'citymunDesc' => 'MAPANDAN',
                'provCode' => 155,
            ),
            105 => 
            array (
                'citymunCode' => 15529,
                'citymunDesc' => 'NATIVIDAD',
                'provCode' => 155,
            ),
            106 => 
            array (
                'citymunCode' => 15530,
                'citymunDesc' => 'POZORRUBIO',
                'provCode' => 155,
            ),
            107 => 
            array (
                'citymunCode' => 15531,
                'citymunDesc' => 'ROSALES',
                'provCode' => 155,
            ),
            108 => 
            array (
                'citymunCode' => 15532,
                'citymunDesc' => 'SAN CARLOS CITY',
                'provCode' => 155,
            ),
            109 => 
            array (
                'citymunCode' => 15533,
                'citymunDesc' => 'SAN FABIAN',
                'provCode' => 155,
            ),
            110 => 
            array (
                'citymunCode' => 15534,
                'citymunDesc' => 'SAN JACINTO',
                'provCode' => 155,
            ),
            111 => 
            array (
                'citymunCode' => 15535,
                'citymunDesc' => 'SAN MANUEL',
                'provCode' => 155,
            ),
            112 => 
            array (
                'citymunCode' => 15536,
                'citymunDesc' => 'SAN NICOLAS',
                'provCode' => 155,
            ),
            113 => 
            array (
                'citymunCode' => 15537,
                'citymunDesc' => 'SAN QUINTIN',
                'provCode' => 155,
            ),
            114 => 
            array (
                'citymunCode' => 15538,
                'citymunDesc' => 'SANTA BARBARA',
                'provCode' => 155,
            ),
            115 => 
            array (
                'citymunCode' => 15539,
                'citymunDesc' => 'SANTA MARIA',
                'provCode' => 155,
            ),
            116 => 
            array (
                'citymunCode' => 15540,
                'citymunDesc' => 'SANTO TOMAS',
                'provCode' => 155,
            ),
            117 => 
            array (
                'citymunCode' => 15541,
                'citymunDesc' => 'SISON',
                'provCode' => 155,
            ),
            118 => 
            array (
                'citymunCode' => 15542,
                'citymunDesc' => 'SUAL',
                'provCode' => 155,
            ),
            119 => 
            array (
                'citymunCode' => 15543,
                'citymunDesc' => 'TAYUG',
                'provCode' => 155,
            ),
            120 => 
            array (
                'citymunCode' => 15544,
                'citymunDesc' => 'UMINGAN',
                'provCode' => 155,
            ),
            121 => 
            array (
                'citymunCode' => 15545,
                'citymunDesc' => 'URBIZTONDO',
                'provCode' => 155,
            ),
            122 => 
            array (
                'citymunCode' => 15546,
                'citymunDesc' => 'CITY OF URDANETA',
                'provCode' => 155,
            ),
            123 => 
            array (
                'citymunCode' => 15547,
                'citymunDesc' => 'VILLASIS',
                'provCode' => 155,
            ),
            124 => 
            array (
                'citymunCode' => 15548,
                'citymunDesc' => 'LAOAC',
                'provCode' => 155,
            ),
            125 => 
            array (
                'citymunCode' => 20901,
            'citymunDesc' => 'BASCO (Capital)',
                'provCode' => 209,
            ),
            126 => 
            array (
                'citymunCode' => 20902,
                'citymunDesc' => 'ITBAYAT',
                'provCode' => 209,
            ),
            127 => 
            array (
                'citymunCode' => 20903,
                'citymunDesc' => 'IVANA',
                'provCode' => 209,
            ),
            128 => 
            array (
                'citymunCode' => 20904,
                'citymunDesc' => 'MAHATAO',
                'provCode' => 209,
            ),
            129 => 
            array (
                'citymunCode' => 20905,
                'citymunDesc' => 'SABTANG',
                'provCode' => 209,
            ),
            130 => 
            array (
                'citymunCode' => 20906,
                'citymunDesc' => 'UYUGAN',
                'provCode' => 209,
            ),
            131 => 
            array (
                'citymunCode' => 21501,
                'citymunDesc' => 'ABULUG',
                'provCode' => 215,
            ),
            132 => 
            array (
                'citymunCode' => 21502,
                'citymunDesc' => 'ALCALA',
                'provCode' => 215,
            ),
            133 => 
            array (
                'citymunCode' => 21503,
                'citymunDesc' => 'ALLACAPAN',
                'provCode' => 215,
            ),
            134 => 
            array (
                'citymunCode' => 21504,
                'citymunDesc' => 'AMULUNG',
                'provCode' => 215,
            ),
            135 => 
            array (
                'citymunCode' => 21505,
                'citymunDesc' => 'APARRI',
                'provCode' => 215,
            ),
            136 => 
            array (
                'citymunCode' => 21506,
                'citymunDesc' => 'BAGGAO',
                'provCode' => 215,
            ),
            137 => 
            array (
                'citymunCode' => 21507,
                'citymunDesc' => 'BALLESTEROS',
                'provCode' => 215,
            ),
            138 => 
            array (
                'citymunCode' => 21508,
                'citymunDesc' => 'BUGUEY',
                'provCode' => 215,
            ),
            139 => 
            array (
                'citymunCode' => 21509,
                'citymunDesc' => 'CALAYAN',
                'provCode' => 215,
            ),
            140 => 
            array (
                'citymunCode' => 21510,
                'citymunDesc' => 'CAMALANIUGAN',
                'provCode' => 215,
            ),
            141 => 
            array (
                'citymunCode' => 21511,
                'citymunDesc' => 'CLAVERIA',
                'provCode' => 215,
            ),
            142 => 
            array (
                'citymunCode' => 21512,
                'citymunDesc' => 'ENRILE',
                'provCode' => 215,
            ),
            143 => 
            array (
                'citymunCode' => 21513,
                'citymunDesc' => 'GATTARAN',
                'provCode' => 215,
            ),
            144 => 
            array (
                'citymunCode' => 21514,
                'citymunDesc' => 'GONZAGA',
                'provCode' => 215,
            ),
            145 => 
            array (
                'citymunCode' => 21515,
                'citymunDesc' => 'IGUIG',
                'provCode' => 215,
            ),
            146 => 
            array (
                'citymunCode' => 21516,
                'citymunDesc' => 'LAL-LO',
                'provCode' => 215,
            ),
            147 => 
            array (
                'citymunCode' => 21517,
                'citymunDesc' => 'LASAM',
                'provCode' => 215,
            ),
            148 => 
            array (
                'citymunCode' => 21518,
                'citymunDesc' => 'PAMPLONA',
                'provCode' => 215,
            ),
            149 => 
            array (
                'citymunCode' => 21519,
                'citymunDesc' => 'PEÑABLANCA',
                'provCode' => 215,
            ),
            150 => 
            array (
                'citymunCode' => 21520,
                'citymunDesc' => 'PIAT',
                'provCode' => 215,
            ),
            151 => 
            array (
                'citymunCode' => 21521,
                'citymunDesc' => 'RIZAL',
                'provCode' => 215,
            ),
            152 => 
            array (
                'citymunCode' => 21522,
                'citymunDesc' => 'SANCHEZ-MIRA',
                'provCode' => 215,
            ),
            153 => 
            array (
                'citymunCode' => 21523,
                'citymunDesc' => 'SANTA ANA',
                'provCode' => 215,
            ),
            154 => 
            array (
                'citymunCode' => 21524,
                'citymunDesc' => 'SANTA PRAXEDES',
                'provCode' => 215,
            ),
            155 => 
            array (
                'citymunCode' => 21525,
                'citymunDesc' => 'SANTA TERESITA',
                'provCode' => 215,
            ),
            156 => 
            array (
                'citymunCode' => 21526,
            'citymunDesc' => 'SANTO NIÑO (FAIRE)',
                'provCode' => 215,
            ),
            157 => 
            array (
                'citymunCode' => 21527,
                'citymunDesc' => 'SOLANA',
                'provCode' => 215,
            ),
            158 => 
            array (
                'citymunCode' => 21528,
                'citymunDesc' => 'TUAO',
                'provCode' => 215,
            ),
            159 => 
            array (
                'citymunCode' => 21529,
            'citymunDesc' => 'TUGUEGARAO CITY (Capital)',
                'provCode' => 215,
            ),
            160 => 
            array (
                'citymunCode' => 23101,
                'citymunDesc' => 'ALICIA',
                'provCode' => 231,
            ),
            161 => 
            array (
                'citymunCode' => 23102,
                'citymunDesc' => 'ANGADANAN',
                'provCode' => 231,
            ),
            162 => 
            array (
                'citymunCode' => 23103,
                'citymunDesc' => 'AURORA',
                'provCode' => 231,
            ),
            163 => 
            array (
                'citymunCode' => 23104,
                'citymunDesc' => 'BENITO SOLIVEN',
                'provCode' => 231,
            ),
            164 => 
            array (
                'citymunCode' => 23105,
                'citymunDesc' => 'BURGOS',
                'provCode' => 231,
            ),
            165 => 
            array (
                'citymunCode' => 23106,
                'citymunDesc' => 'CABAGAN',
                'provCode' => 231,
            ),
            166 => 
            array (
                'citymunCode' => 23107,
                'citymunDesc' => 'CABATUAN',
                'provCode' => 231,
            ),
            167 => 
            array (
                'citymunCode' => 23108,
                'citymunDesc' => 'CITY OF CAUAYAN',
                'provCode' => 231,
            ),
            168 => 
            array (
                'citymunCode' => 23109,
                'citymunDesc' => 'CORDON',
                'provCode' => 231,
            ),
            169 => 
            array (
                'citymunCode' => 23110,
                'citymunDesc' => 'DINAPIGUE',
                'provCode' => 231,
            ),
            170 => 
            array (
                'citymunCode' => 23111,
                'citymunDesc' => 'DIVILACAN',
                'provCode' => 231,
            ),
            171 => 
            array (
                'citymunCode' => 23112,
                'citymunDesc' => 'ECHAGUE',
                'provCode' => 231,
            ),
            172 => 
            array (
                'citymunCode' => 23113,
                'citymunDesc' => 'GAMU',
                'provCode' => 231,
            ),
            173 => 
            array (
                'citymunCode' => 23114,
            'citymunDesc' => 'ILAGAN CITY (Capital)',
                'provCode' => 231,
            ),
            174 => 
            array (
                'citymunCode' => 23115,
                'citymunDesc' => 'JONES',
                'provCode' => 231,
            ),
            175 => 
            array (
                'citymunCode' => 23116,
                'citymunDesc' => 'LUNA',
                'provCode' => 231,
            ),
            176 => 
            array (
                'citymunCode' => 23117,
                'citymunDesc' => 'MACONACON',
                'provCode' => 231,
            ),
            177 => 
            array (
                'citymunCode' => 23118,
            'citymunDesc' => 'DELFIN ALBANO (MAGSAYSAY)',
                'provCode' => 231,
            ),
            178 => 
            array (
                'citymunCode' => 23119,
                'citymunDesc' => 'MALLIG',
                'provCode' => 231,
            ),
            179 => 
            array (
                'citymunCode' => 23120,
                'citymunDesc' => 'NAGUILIAN',
                'provCode' => 231,
            ),
            180 => 
            array (
                'citymunCode' => 23121,
                'citymunDesc' => 'PALANAN',
                'provCode' => 231,
            ),
            181 => 
            array (
                'citymunCode' => 23122,
                'citymunDesc' => 'QUEZON',
                'provCode' => 231,
            ),
            182 => 
            array (
                'citymunCode' => 23123,
                'citymunDesc' => 'QUIRINO',
                'provCode' => 231,
            ),
            183 => 
            array (
                'citymunCode' => 23124,
                'citymunDesc' => 'RAMON',
                'provCode' => 231,
            ),
            184 => 
            array (
                'citymunCode' => 23125,
                'citymunDesc' => 'REINA MERCEDES',
                'provCode' => 231,
            ),
            185 => 
            array (
                'citymunCode' => 23126,
                'citymunDesc' => 'ROXAS',
                'provCode' => 231,
            ),
            186 => 
            array (
                'citymunCode' => 23127,
                'citymunDesc' => 'SAN AGUSTIN',
                'provCode' => 231,
            ),
            187 => 
            array (
                'citymunCode' => 23128,
                'citymunDesc' => 'SAN GUILLERMO',
                'provCode' => 231,
            ),
            188 => 
            array (
                'citymunCode' => 23129,
                'citymunDesc' => 'SAN ISIDRO',
                'provCode' => 231,
            ),
            189 => 
            array (
                'citymunCode' => 23130,
                'citymunDesc' => 'SAN MANUEL',
                'provCode' => 231,
            ),
            190 => 
            array (
                'citymunCode' => 23131,
                'citymunDesc' => 'SAN MARIANO',
                'provCode' => 231,
            ),
            191 => 
            array (
                'citymunCode' => 23132,
                'citymunDesc' => 'SAN MATEO',
                'provCode' => 231,
            ),
            192 => 
            array (
                'citymunCode' => 23133,
                'citymunDesc' => 'SAN PABLO',
                'provCode' => 231,
            ),
            193 => 
            array (
                'citymunCode' => 23134,
                'citymunDesc' => 'SANTA MARIA',
                'provCode' => 231,
            ),
            194 => 
            array (
                'citymunCode' => 23135,
                'citymunDesc' => 'CITY OF SANTIAGO',
                'provCode' => 231,
            ),
            195 => 
            array (
                'citymunCode' => 23136,
                'citymunDesc' => 'SANTO TOMAS',
                'provCode' => 231,
            ),
            196 => 
            array (
                'citymunCode' => 23137,
                'citymunDesc' => 'TUMAUINI',
                'provCode' => 231,
            ),
            197 => 
            array (
                'citymunCode' => 25001,
                'citymunDesc' => 'AMBAGUIO',
                'provCode' => 250,
            ),
            198 => 
            array (
                'citymunCode' => 25002,
                'citymunDesc' => 'ARITAO',
                'provCode' => 250,
            ),
            199 => 
            array (
                'citymunCode' => 25003,
                'citymunDesc' => 'BAGABAG',
                'provCode' => 250,
            ),
            200 => 
            array (
                'citymunCode' => 25004,
                'citymunDesc' => 'BAMBANG',
                'provCode' => 250,
            ),
            201 => 
            array (
                'citymunCode' => 25005,
            'citymunDesc' => 'BAYOMBONG (Capital)',
                'provCode' => 250,
            ),
            202 => 
            array (
                'citymunCode' => 25006,
                'citymunDesc' => 'DIADI',
                'provCode' => 250,
            ),
            203 => 
            array (
                'citymunCode' => 25007,
                'citymunDesc' => 'DUPAX DEL NORTE',
                'provCode' => 250,
            ),
            204 => 
            array (
                'citymunCode' => 25008,
                'citymunDesc' => 'DUPAX DEL SUR',
                'provCode' => 250,
            ),
            205 => 
            array (
                'citymunCode' => 25009,
                'citymunDesc' => 'KASIBU',
                'provCode' => 250,
            ),
            206 => 
            array (
                'citymunCode' => 25010,
                'citymunDesc' => 'KAYAPA',
                'provCode' => 250,
            ),
            207 => 
            array (
                'citymunCode' => 25011,
                'citymunDesc' => 'QUEZON',
                'provCode' => 250,
            ),
            208 => 
            array (
                'citymunCode' => 25012,
                'citymunDesc' => 'SANTA FE',
                'provCode' => 250,
            ),
            209 => 
            array (
                'citymunCode' => 25013,
                'citymunDesc' => 'SOLANO',
                'provCode' => 250,
            ),
            210 => 
            array (
                'citymunCode' => 25014,
                'citymunDesc' => 'VILLAVERDE',
                'provCode' => 250,
            ),
            211 => 
            array (
                'citymunCode' => 25015,
                'citymunDesc' => 'ALFONSO CASTANEDA',
                'provCode' => 250,
            ),
            212 => 
            array (
                'citymunCode' => 25701,
                'citymunDesc' => 'AGLIPAY',
                'provCode' => 257,
            ),
            213 => 
            array (
                'citymunCode' => 25702,
            'citymunDesc' => 'CABARROGUIS (Capital)',
                'provCode' => 257,
            ),
            214 => 
            array (
                'citymunCode' => 25703,
                'citymunDesc' => 'DIFFUN',
                'provCode' => 257,
            ),
            215 => 
            array (
                'citymunCode' => 25704,
                'citymunDesc' => 'MADDELA',
                'provCode' => 257,
            ),
            216 => 
            array (
                'citymunCode' => 25705,
                'citymunDesc' => 'SAGUDAY',
                'provCode' => 257,
            ),
            217 => 
            array (
                'citymunCode' => 25706,
                'citymunDesc' => 'NAGTIPUNAN',
                'provCode' => 257,
            ),
            218 => 
            array (
                'citymunCode' => 30801,
                'citymunDesc' => 'ABUCAY',
                'provCode' => 308,
            ),
            219 => 
            array (
                'citymunCode' => 30802,
                'citymunDesc' => 'BAGAC',
                'provCode' => 308,
            ),
            220 => 
            array (
                'citymunCode' => 30803,
            'citymunDesc' => 'CITY OF BALANGA (Capital)',
                'provCode' => 308,
            ),
            221 => 
            array (
                'citymunCode' => 30804,
                'citymunDesc' => 'DINALUPIHAN',
                'provCode' => 308,
            ),
            222 => 
            array (
                'citymunCode' => 30805,
                'citymunDesc' => 'HERMOSA',
                'provCode' => 308,
            ),
            223 => 
            array (
                'citymunCode' => 30806,
                'citymunDesc' => 'LIMAY',
                'provCode' => 308,
            ),
            224 => 
            array (
                'citymunCode' => 30807,
                'citymunDesc' => 'MARIVELES',
                'provCode' => 308,
            ),
            225 => 
            array (
                'citymunCode' => 30808,
                'citymunDesc' => 'MORONG',
                'provCode' => 308,
            ),
            226 => 
            array (
                'citymunCode' => 30809,
                'citymunDesc' => 'ORANI',
                'provCode' => 308,
            ),
            227 => 
            array (
                'citymunCode' => 30810,
                'citymunDesc' => 'ORION',
                'provCode' => 308,
            ),
            228 => 
            array (
                'citymunCode' => 30811,
                'citymunDesc' => 'PILAR',
                'provCode' => 308,
            ),
            229 => 
            array (
                'citymunCode' => 30812,
                'citymunDesc' => 'SAMAL',
                'provCode' => 308,
            ),
            230 => 
            array (
                'citymunCode' => 31401,
                'citymunDesc' => 'ANGAT',
                'provCode' => 314,
            ),
            231 => 
            array (
                'citymunCode' => 31402,
            'citymunDesc' => 'BALAGTAS (BIGAA)',
                'provCode' => 314,
            ),
            232 => 
            array (
                'citymunCode' => 31403,
                'citymunDesc' => 'BALIUAG',
                'provCode' => 314,
            ),
            233 => 
            array (
                'citymunCode' => 31404,
                'citymunDesc' => 'BOCAUE',
                'provCode' => 314,
            ),
            234 => 
            array (
                'citymunCode' => 31405,
                'citymunDesc' => 'BULACAN',
                'provCode' => 314,
            ),
            235 => 
            array (
                'citymunCode' => 31406,
                'citymunDesc' => 'BUSTOS',
                'provCode' => 314,
            ),
            236 => 
            array (
                'citymunCode' => 31407,
                'citymunDesc' => 'CALUMPIT',
                'provCode' => 314,
            ),
            237 => 
            array (
                'citymunCode' => 31408,
                'citymunDesc' => 'GUIGUINTO',
                'provCode' => 314,
            ),
            238 => 
            array (
                'citymunCode' => 31409,
                'citymunDesc' => 'HAGONOY',
                'provCode' => 314,
            ),
            239 => 
            array (
                'citymunCode' => 31410,
            'citymunDesc' => 'CITY OF MALOLOS (Capital)',
                'provCode' => 314,
            ),
            240 => 
            array (
                'citymunCode' => 31411,
                'citymunDesc' => 'MARILAO',
                'provCode' => 314,
            ),
            241 => 
            array (
                'citymunCode' => 31412,
                'citymunDesc' => 'CITY OF MEYCAUAYAN',
                'provCode' => 314,
            ),
            242 => 
            array (
                'citymunCode' => 31413,
                'citymunDesc' => 'NORZAGARAY',
                'provCode' => 314,
            ),
            243 => 
            array (
                'citymunCode' => 31414,
                'citymunDesc' => 'OBANDO',
                'provCode' => 314,
            ),
            244 => 
            array (
                'citymunCode' => 31415,
                'citymunDesc' => 'PANDI',
                'provCode' => 314,
            ),
            245 => 
            array (
                'citymunCode' => 31416,
                'citymunDesc' => 'PAOMBONG',
                'provCode' => 314,
            ),
            246 => 
            array (
                'citymunCode' => 31417,
                'citymunDesc' => 'PLARIDEL',
                'provCode' => 314,
            ),
            247 => 
            array (
                'citymunCode' => 31418,
                'citymunDesc' => 'PULILAN',
                'provCode' => 314,
            ),
            248 => 
            array (
                'citymunCode' => 31419,
                'citymunDesc' => 'SAN ILDEFONSO',
                'provCode' => 314,
            ),
            249 => 
            array (
                'citymunCode' => 31420,
                'citymunDesc' => 'CITY OF SAN JOSE DEL MONTE',
                'provCode' => 314,
            ),
            250 => 
            array (
                'citymunCode' => 31421,
                'citymunDesc' => 'SAN MIGUEL',
                'provCode' => 314,
            ),
            251 => 
            array (
                'citymunCode' => 31422,
                'citymunDesc' => 'SAN RAFAEL',
                'provCode' => 314,
            ),
            252 => 
            array (
                'citymunCode' => 31423,
                'citymunDesc' => 'SANTA MARIA',
                'provCode' => 314,
            ),
            253 => 
            array (
                'citymunCode' => 31424,
                'citymunDesc' => 'DOÑA REMEDIOS TRINIDAD',
                'provCode' => 314,
            ),
            254 => 
            array (
                'citymunCode' => 34901,
                'citymunDesc' => 'ALIAGA',
                'provCode' => 349,
            ),
            255 => 
            array (
                'citymunCode' => 34902,
                'citymunDesc' => 'BONGABON',
                'provCode' => 349,
            ),
            256 => 
            array (
                'citymunCode' => 34903,
                'citymunDesc' => 'CABANATUAN CITY',
                'provCode' => 349,
            ),
            257 => 
            array (
                'citymunCode' => 34904,
                'citymunDesc' => 'CABIAO',
                'provCode' => 349,
            ),
            258 => 
            array (
                'citymunCode' => 34905,
                'citymunDesc' => 'CARRANGLAN',
                'provCode' => 349,
            ),
            259 => 
            array (
                'citymunCode' => 34906,
                'citymunDesc' => 'CUYAPO',
                'provCode' => 349,
            ),
            260 => 
            array (
                'citymunCode' => 34907,
            'citymunDesc' => 'GABALDON (BITULOK & SABANI)',
                'provCode' => 349,
            ),
            261 => 
            array (
                'citymunCode' => 34908,
                'citymunDesc' => 'CITY OF GAPAN',
                'provCode' => 349,
            ),
            262 => 
            array (
                'citymunCode' => 34909,
                'citymunDesc' => 'GENERAL MAMERTO NATIVIDAD',
                'provCode' => 349,
            ),
            263 => 
            array (
                'citymunCode' => 34910,
            'citymunDesc' => 'GENERAL TINIO (PAPAYA)',
                'provCode' => 349,
            ),
            264 => 
            array (
                'citymunCode' => 34911,
                'citymunDesc' => 'GUIMBA',
                'provCode' => 349,
            ),
            265 => 
            array (
                'citymunCode' => 34912,
                'citymunDesc' => 'JAEN',
                'provCode' => 349,
            ),
            266 => 
            array (
                'citymunCode' => 34913,
                'citymunDesc' => 'LAUR',
                'provCode' => 349,
            ),
            267 => 
            array (
                'citymunCode' => 34914,
                'citymunDesc' => 'LICAB',
                'provCode' => 349,
            ),
            268 => 
            array (
                'citymunCode' => 34915,
                'citymunDesc' => 'LLANERA',
                'provCode' => 349,
            ),
            269 => 
            array (
                'citymunCode' => 34916,
                'citymunDesc' => 'LUPAO',
                'provCode' => 349,
            ),
            270 => 
            array (
                'citymunCode' => 34917,
                'citymunDesc' => 'SCIENCE CITY OF MUÑOZ',
                'provCode' => 349,
            ),
            271 => 
            array (
                'citymunCode' => 34918,
                'citymunDesc' => 'NAMPICUAN',
                'provCode' => 349,
            ),
            272 => 
            array (
                'citymunCode' => 34919,
            'citymunDesc' => 'PALAYAN CITY (Capital)',
                'provCode' => 349,
            ),
            273 => 
            array (
                'citymunCode' => 34920,
                'citymunDesc' => 'PANTABANGAN',
                'provCode' => 349,
            ),
            274 => 
            array (
                'citymunCode' => 34921,
                'citymunDesc' => 'PEÑARANDA',
                'provCode' => 349,
            ),
            275 => 
            array (
                'citymunCode' => 34922,
                'citymunDesc' => 'QUEZON',
                'provCode' => 349,
            ),
            276 => 
            array (
                'citymunCode' => 34923,
                'citymunDesc' => 'RIZAL',
                'provCode' => 349,
            ),
            277 => 
            array (
                'citymunCode' => 34924,
                'citymunDesc' => 'SAN ANTONIO',
                'provCode' => 349,
            ),
            278 => 
            array (
                'citymunCode' => 34925,
                'citymunDesc' => 'SAN ISIDRO',
                'provCode' => 349,
            ),
            279 => 
            array (
                'citymunCode' => 34926,
                'citymunDesc' => 'SAN JOSE CITY',
                'provCode' => 349,
            ),
            280 => 
            array (
                'citymunCode' => 34927,
                'citymunDesc' => 'SAN LEONARDO',
                'provCode' => 349,
            ),
            281 => 
            array (
                'citymunCode' => 34928,
                'citymunDesc' => 'SANTA ROSA',
                'provCode' => 349,
            ),
            282 => 
            array (
                'citymunCode' => 34929,
                'citymunDesc' => 'SANTO DOMINGO',
                'provCode' => 349,
            ),
            283 => 
            array (
                'citymunCode' => 34930,
                'citymunDesc' => 'TALAVERA',
                'provCode' => 349,
            ),
            284 => 
            array (
                'citymunCode' => 34931,
                'citymunDesc' => 'TALUGTUG',
                'provCode' => 349,
            ),
            285 => 
            array (
                'citymunCode' => 34932,
                'citymunDesc' => 'ZARAGOZA',
                'provCode' => 349,
            ),
            286 => 
            array (
                'citymunCode' => 35401,
                'citymunDesc' => 'ANGELES CITY',
                'provCode' => 354,
            ),
            287 => 
            array (
                'citymunCode' => 35402,
                'citymunDesc' => 'APALIT',
                'provCode' => 354,
            ),
            288 => 
            array (
                'citymunCode' => 35403,
                'citymunDesc' => 'ARAYAT',
                'provCode' => 354,
            ),
            289 => 
            array (
                'citymunCode' => 35404,
                'citymunDesc' => 'BACOLOR',
                'provCode' => 354,
            ),
            290 => 
            array (
                'citymunCode' => 35405,
                'citymunDesc' => 'CANDABA',
                'provCode' => 354,
            ),
            291 => 
            array (
                'citymunCode' => 35406,
                'citymunDesc' => 'FLORIDABLANCA',
                'provCode' => 354,
            ),
            292 => 
            array (
                'citymunCode' => 35407,
                'citymunDesc' => 'GUAGUA',
                'provCode' => 354,
            ),
            293 => 
            array (
                'citymunCode' => 35408,
                'citymunDesc' => 'LUBAO',
                'provCode' => 354,
            ),
            294 => 
            array (
                'citymunCode' => 35409,
                'citymunDesc' => 'MABALACAT CITY',
                'provCode' => 354,
            ),
            295 => 
            array (
                'citymunCode' => 35410,
                'citymunDesc' => 'MACABEBE',
                'provCode' => 354,
            ),
            296 => 
            array (
                'citymunCode' => 35411,
                'citymunDesc' => 'MAGALANG',
                'provCode' => 354,
            ),
            297 => 
            array (
                'citymunCode' => 35412,
                'citymunDesc' => 'MASANTOL',
                'provCode' => 354,
            ),
            298 => 
            array (
                'citymunCode' => 35413,
                'citymunDesc' => 'MEXICO',
                'provCode' => 354,
            ),
            299 => 
            array (
                'citymunCode' => 35414,
                'citymunDesc' => 'MINALIN',
                'provCode' => 354,
            ),
            300 => 
            array (
                'citymunCode' => 35415,
                'citymunDesc' => 'PORAC',
                'provCode' => 354,
            ),
            301 => 
            array (
                'citymunCode' => 35416,
            'citymunDesc' => 'CITY OF SAN FERNANDO (Capital)',
                'provCode' => 354,
            ),
            302 => 
            array (
                'citymunCode' => 35417,
                'citymunDesc' => 'SAN LUIS',
                'provCode' => 354,
            ),
            303 => 
            array (
                'citymunCode' => 35418,
                'citymunDesc' => 'SAN SIMON',
                'provCode' => 354,
            ),
            304 => 
            array (
                'citymunCode' => 35419,
                'citymunDesc' => 'SANTA ANA',
                'provCode' => 354,
            ),
            305 => 
            array (
                'citymunCode' => 35420,
                'citymunDesc' => 'SANTA RITA',
                'provCode' => 354,
            ),
            306 => 
            array (
                'citymunCode' => 35421,
                'citymunDesc' => 'SANTO TOMAS',
                'provCode' => 354,
            ),
            307 => 
            array (
                'citymunCode' => 35422,
            'citymunDesc' => 'SASMUAN (Sexmoan)',
                'provCode' => 354,
            ),
            308 => 
            array (
                'citymunCode' => 36901,
                'citymunDesc' => 'ANAO',
                'provCode' => 369,
            ),
            309 => 
            array (
                'citymunCode' => 36902,
                'citymunDesc' => 'BAMBAN',
                'provCode' => 369,
            ),
            310 => 
            array (
                'citymunCode' => 36903,
                'citymunDesc' => 'CAMILING',
                'provCode' => 369,
            ),
            311 => 
            array (
                'citymunCode' => 36904,
                'citymunDesc' => 'CAPAS',
                'provCode' => 369,
            ),
            312 => 
            array (
                'citymunCode' => 36905,
                'citymunDesc' => 'CONCEPCION',
                'provCode' => 369,
            ),
            313 => 
            array (
                'citymunCode' => 36906,
                'citymunDesc' => 'GERONA',
                'provCode' => 369,
            ),
            314 => 
            array (
                'citymunCode' => 36907,
                'citymunDesc' => 'LA PAZ',
                'provCode' => 369,
            ),
            315 => 
            array (
                'citymunCode' => 36908,
                'citymunDesc' => 'MAYANTOC',
                'provCode' => 369,
            ),
            316 => 
            array (
                'citymunCode' => 36909,
                'citymunDesc' => 'MONCADA',
                'provCode' => 369,
            ),
            317 => 
            array (
                'citymunCode' => 36910,
                'citymunDesc' => 'PANIQUI',
                'provCode' => 369,
            ),
            318 => 
            array (
                'citymunCode' => 36911,
                'citymunDesc' => 'PURA',
                'provCode' => 369,
            ),
            319 => 
            array (
                'citymunCode' => 36912,
                'citymunDesc' => 'RAMOS',
                'provCode' => 369,
            ),
            320 => 
            array (
                'citymunCode' => 36913,
                'citymunDesc' => 'SAN CLEMENTE',
                'provCode' => 369,
            ),
            321 => 
            array (
                'citymunCode' => 36914,
                'citymunDesc' => 'SAN MANUEL',
                'provCode' => 369,
            ),
            322 => 
            array (
                'citymunCode' => 36915,
                'citymunDesc' => 'SANTA IGNACIA',
                'provCode' => 369,
            ),
            323 => 
            array (
                'citymunCode' => 36916,
            'citymunDesc' => 'CITY OF TARLAC (Capital)',
                'provCode' => 369,
            ),
            324 => 
            array (
                'citymunCode' => 36917,
                'citymunDesc' => 'VICTORIA',
                'provCode' => 369,
            ),
            325 => 
            array (
                'citymunCode' => 36918,
                'citymunDesc' => 'SAN JOSE',
                'provCode' => 369,
            ),
            326 => 
            array (
                'citymunCode' => 37101,
                'citymunDesc' => 'BOTOLAN',
                'provCode' => 371,
            ),
            327 => 
            array (
                'citymunCode' => 37102,
                'citymunDesc' => 'CABANGAN',
                'provCode' => 371,
            ),
            328 => 
            array (
                'citymunCode' => 37103,
                'citymunDesc' => 'CANDELARIA',
                'provCode' => 371,
            ),
            329 => 
            array (
                'citymunCode' => 37104,
                'citymunDesc' => 'CASTILLEJOS',
                'provCode' => 371,
            ),
            330 => 
            array (
                'citymunCode' => 37105,
            'citymunDesc' => 'IBA (Capital)',
                'provCode' => 371,
            ),
            331 => 
            array (
                'citymunCode' => 37106,
                'citymunDesc' => 'MASINLOC',
                'provCode' => 371,
            ),
            332 => 
            array (
                'citymunCode' => 37107,
                'citymunDesc' => 'OLONGAPO CITY',
                'provCode' => 371,
            ),
            333 => 
            array (
                'citymunCode' => 37108,
                'citymunDesc' => 'PALAUIG',
                'provCode' => 371,
            ),
            334 => 
            array (
                'citymunCode' => 37109,
                'citymunDesc' => 'SAN ANTONIO',
                'provCode' => 371,
            ),
            335 => 
            array (
                'citymunCode' => 37110,
                'citymunDesc' => 'SAN FELIPE',
                'provCode' => 371,
            ),
            336 => 
            array (
                'citymunCode' => 37111,
                'citymunDesc' => 'SAN MARCELINO',
                'provCode' => 371,
            ),
            337 => 
            array (
                'citymunCode' => 37112,
                'citymunDesc' => 'SAN NARCISO',
                'provCode' => 371,
            ),
            338 => 
            array (
                'citymunCode' => 37113,
                'citymunDesc' => 'SANTA CRUZ',
                'provCode' => 371,
            ),
            339 => 
            array (
                'citymunCode' => 37114,
                'citymunDesc' => 'SUBIC',
                'provCode' => 371,
            ),
            340 => 
            array (
                'citymunCode' => 37701,
            'citymunDesc' => 'BALER (Capital)',
                'provCode' => 377,
            ),
            341 => 
            array (
                'citymunCode' => 37702,
                'citymunDesc' => 'CASIGURAN',
                'provCode' => 377,
            ),
            342 => 
            array (
                'citymunCode' => 37703,
                'citymunDesc' => 'DILASAG',
                'provCode' => 377,
            ),
            343 => 
            array (
                'citymunCode' => 37704,
                'citymunDesc' => 'DINALUNGAN',
                'provCode' => 377,
            ),
            344 => 
            array (
                'citymunCode' => 37705,
                'citymunDesc' => 'DINGALAN',
                'provCode' => 377,
            ),
            345 => 
            array (
                'citymunCode' => 37706,
                'citymunDesc' => 'DIPACULAO',
                'provCode' => 377,
            ),
            346 => 
            array (
                'citymunCode' => 37707,
                'citymunDesc' => 'MARIA AURORA',
                'provCode' => 377,
            ),
            347 => 
            array (
                'citymunCode' => 37708,
                'citymunDesc' => 'SAN LUIS',
                'provCode' => 377,
            ),
            348 => 
            array (
                'citymunCode' => 41001,
                'citymunDesc' => 'AGONCILLO',
                'provCode' => 410,
            ),
            349 => 
            array (
                'citymunCode' => 41002,
                'citymunDesc' => 'ALITAGTAG',
                'provCode' => 410,
            ),
            350 => 
            array (
                'citymunCode' => 41003,
                'citymunDesc' => 'BALAYAN',
                'provCode' => 410,
            ),
            351 => 
            array (
                'citymunCode' => 41004,
                'citymunDesc' => 'BALETE',
                'provCode' => 410,
            ),
            352 => 
            array (
                'citymunCode' => 41005,
            'citymunDesc' => 'BATANGAS CITY (Capital)',
                'provCode' => 410,
            ),
            353 => 
            array (
                'citymunCode' => 41006,
                'citymunDesc' => 'BAUAN',
                'provCode' => 410,
            ),
            354 => 
            array (
                'citymunCode' => 41007,
                'citymunDesc' => 'CALACA',
                'provCode' => 410,
            ),
            355 => 
            array (
                'citymunCode' => 41008,
                'citymunDesc' => 'CALATAGAN',
                'provCode' => 410,
            ),
            356 => 
            array (
                'citymunCode' => 41009,
                'citymunDesc' => 'CUENCA',
                'provCode' => 410,
            ),
            357 => 
            array (
                'citymunCode' => 41010,
                'citymunDesc' => 'IBAAN',
                'provCode' => 410,
            ),
            358 => 
            array (
                'citymunCode' => 41011,
                'citymunDesc' => 'LAUREL',
                'provCode' => 410,
            ),
            359 => 
            array (
                'citymunCode' => 41012,
                'citymunDesc' => 'LEMERY',
                'provCode' => 410,
            ),
            360 => 
            array (
                'citymunCode' => 41013,
                'citymunDesc' => 'LIAN',
                'provCode' => 410,
            ),
            361 => 
            array (
                'citymunCode' => 41014,
                'citymunDesc' => 'LIPA CITY',
                'provCode' => 410,
            ),
            362 => 
            array (
                'citymunCode' => 41015,
                'citymunDesc' => 'LOBO',
                'provCode' => 410,
            ),
            363 => 
            array (
                'citymunCode' => 41016,
                'citymunDesc' => 'MABINI',
                'provCode' => 410,
            ),
            364 => 
            array (
                'citymunCode' => 41017,
                'citymunDesc' => 'MALVAR',
                'provCode' => 410,
            ),
            365 => 
            array (
                'citymunCode' => 41018,
                'citymunDesc' => 'MATAASNAKAHOY',
                'provCode' => 410,
            ),
            366 => 
            array (
                'citymunCode' => 41019,
                'citymunDesc' => 'NASUGBU',
                'provCode' => 410,
            ),
            367 => 
            array (
                'citymunCode' => 41020,
                'citymunDesc' => 'PADRE GARCIA',
                'provCode' => 410,
            ),
            368 => 
            array (
                'citymunCode' => 41021,
                'citymunDesc' => 'ROSARIO',
                'provCode' => 410,
            ),
            369 => 
            array (
                'citymunCode' => 41022,
                'citymunDesc' => 'SAN JOSE',
                'provCode' => 410,
            ),
            370 => 
            array (
                'citymunCode' => 41023,
                'citymunDesc' => 'SAN JUAN',
                'provCode' => 410,
            ),
            371 => 
            array (
                'citymunCode' => 41024,
                'citymunDesc' => 'SAN LUIS',
                'provCode' => 410,
            ),
            372 => 
            array (
                'citymunCode' => 41025,
                'citymunDesc' => 'SAN NICOLAS',
                'provCode' => 410,
            ),
            373 => 
            array (
                'citymunCode' => 41026,
                'citymunDesc' => 'SAN PASCUAL',
                'provCode' => 410,
            ),
            374 => 
            array (
                'citymunCode' => 41027,
                'citymunDesc' => 'SANTA TERESITA',
                'provCode' => 410,
            ),
            375 => 
            array (
                'citymunCode' => 41028,
                'citymunDesc' => 'SANTO TOMAS',
                'provCode' => 410,
            ),
            376 => 
            array (
                'citymunCode' => 41029,
                'citymunDesc' => 'TAAL',
                'provCode' => 410,
            ),
            377 => 
            array (
                'citymunCode' => 41030,
                'citymunDesc' => 'TALISAY',
                'provCode' => 410,
            ),
            378 => 
            array (
                'citymunCode' => 41031,
                'citymunDesc' => 'CITY OF TANAUAN',
                'provCode' => 410,
            ),
            379 => 
            array (
                'citymunCode' => 41032,
                'citymunDesc' => 'TAYSAN',
                'provCode' => 410,
            ),
            380 => 
            array (
                'citymunCode' => 41033,
                'citymunDesc' => 'TINGLOY',
                'provCode' => 410,
            ),
            381 => 
            array (
                'citymunCode' => 41034,
                'citymunDesc' => 'TUY',
                'provCode' => 410,
            ),
            382 => 
            array (
                'citymunCode' => 42101,
                'citymunDesc' => 'ALFONSO',
                'provCode' => 421,
            ),
            383 => 
            array (
                'citymunCode' => 42102,
                'citymunDesc' => 'AMADEO',
                'provCode' => 421,
            ),
            384 => 
            array (
                'citymunCode' => 42103,
                'citymunDesc' => 'BACOOR CITY',
                'provCode' => 421,
            ),
            385 => 
            array (
                'citymunCode' => 42104,
                'citymunDesc' => 'CARMONA',
                'provCode' => 421,
            ),
            386 => 
            array (
                'citymunCode' => 42105,
                'citymunDesc' => 'CAVITE CITY',
                'provCode' => 421,
            ),
            387 => 
            array (
                'citymunCode' => 42106,
                'citymunDesc' => 'CITY OF DASMARIÑAS',
                'provCode' => 421,
            ),
            388 => 
            array (
                'citymunCode' => 42107,
                'citymunDesc' => 'GENERAL EMILIO AGUINALDO',
                'provCode' => 421,
            ),
            389 => 
            array (
                'citymunCode' => 42108,
                'citymunDesc' => 'GENERAL TRIAS',
                'provCode' => 421,
            ),
            390 => 
            array (
                'citymunCode' => 42109,
                'citymunDesc' => 'IMUS CITY',
                'provCode' => 421,
            ),
            391 => 
            array (
                'citymunCode' => 42110,
                'citymunDesc' => 'INDANG',
                'provCode' => 421,
            ),
            392 => 
            array (
                'citymunCode' => 42111,
                'citymunDesc' => 'KAWIT',
                'provCode' => 421,
            ),
            393 => 
            array (
                'citymunCode' => 42112,
                'citymunDesc' => 'MAGALLANES',
                'provCode' => 421,
            ),
            394 => 
            array (
                'citymunCode' => 42113,
                'citymunDesc' => 'MARAGONDON',
                'provCode' => 421,
            ),
            395 => 
            array (
                'citymunCode' => 42114,
            'citymunDesc' => 'MENDEZ (MENDEZ-NUÑEZ)',
                'provCode' => 421,
            ),
            396 => 
            array (
                'citymunCode' => 42115,
                'citymunDesc' => 'NAIC',
                'provCode' => 421,
            ),
            397 => 
            array (
                'citymunCode' => 42116,
                'citymunDesc' => 'NOVELETA',
                'provCode' => 421,
            ),
            398 => 
            array (
                'citymunCode' => 42117,
                'citymunDesc' => 'ROSARIO',
                'provCode' => 421,
            ),
            399 => 
            array (
                'citymunCode' => 42118,
                'citymunDesc' => 'SILANG',
                'provCode' => 421,
            ),
            400 => 
            array (
                'citymunCode' => 42119,
                'citymunDesc' => 'TAGAYTAY CITY',
                'provCode' => 421,
            ),
            401 => 
            array (
                'citymunCode' => 42120,
                'citymunDesc' => 'TANZA',
                'provCode' => 421,
            ),
            402 => 
            array (
                'citymunCode' => 42121,
                'citymunDesc' => 'TERNATE',
                'provCode' => 421,
            ),
            403 => 
            array (
                'citymunCode' => 42122,
            'citymunDesc' => 'TRECE MARTIRES CITY (Capital)',
                'provCode' => 421,
            ),
            404 => 
            array (
                'citymunCode' => 42123,
                'citymunDesc' => 'GEN. MARIANO ALVAREZ',
                'provCode' => 421,
            ),
            405 => 
            array (
                'citymunCode' => 43401,
                'citymunDesc' => 'ALAMINOS',
                'provCode' => 434,
            ),
            406 => 
            array (
                'citymunCode' => 43402,
                'citymunDesc' => 'BAY',
                'provCode' => 434,
            ),
            407 => 
            array (
                'citymunCode' => 43403,
                'citymunDesc' => 'CITY OF BIÑAN',
                'provCode' => 434,
            ),
            408 => 
            array (
                'citymunCode' => 43404,
                'citymunDesc' => 'CABUYAO CITY',
                'provCode' => 434,
            ),
            409 => 
            array (
                'citymunCode' => 43405,
                'citymunDesc' => 'CITY OF CALAMBA',
                'provCode' => 434,
            ),
            410 => 
            array (
                'citymunCode' => 43406,
                'citymunDesc' => 'CALAUAN',
                'provCode' => 434,
            ),
            411 => 
            array (
                'citymunCode' => 43407,
                'citymunDesc' => 'CAVINTI',
                'provCode' => 434,
            ),
            412 => 
            array (
                'citymunCode' => 43408,
                'citymunDesc' => 'FAMY',
                'provCode' => 434,
            ),
            413 => 
            array (
                'citymunCode' => 43409,
                'citymunDesc' => 'KALAYAAN',
                'provCode' => 434,
            ),
            414 => 
            array (
                'citymunCode' => 43410,
                'citymunDesc' => 'LILIW',
                'provCode' => 434,
            ),
            415 => 
            array (
                'citymunCode' => 43411,
                'citymunDesc' => 'LOS BAÑOS',
                'provCode' => 434,
            ),
            416 => 
            array (
                'citymunCode' => 43412,
                'citymunDesc' => 'LUISIANA',
                'provCode' => 434,
            ),
            417 => 
            array (
                'citymunCode' => 43413,
                'citymunDesc' => 'LUMBAN',
                'provCode' => 434,
            ),
            418 => 
            array (
                'citymunCode' => 43414,
                'citymunDesc' => 'MABITAC',
                'provCode' => 434,
            ),
            419 => 
            array (
                'citymunCode' => 43415,
                'citymunDesc' => 'MAGDALENA',
                'provCode' => 434,
            ),
            420 => 
            array (
                'citymunCode' => 43416,
                'citymunDesc' => 'MAJAYJAY',
                'provCode' => 434,
            ),
            421 => 
            array (
                'citymunCode' => 43417,
                'citymunDesc' => 'NAGCARLAN',
                'provCode' => 434,
            ),
            422 => 
            array (
                'citymunCode' => 43418,
                'citymunDesc' => 'PAETE',
                'provCode' => 434,
            ),
            423 => 
            array (
                'citymunCode' => 43419,
                'citymunDesc' => 'PAGSANJAN',
                'provCode' => 434,
            ),
            424 => 
            array (
                'citymunCode' => 43420,
                'citymunDesc' => 'PAKIL',
                'provCode' => 434,
            ),
            425 => 
            array (
                'citymunCode' => 43421,
                'citymunDesc' => 'PANGIL',
                'provCode' => 434,
            ),
            426 => 
            array (
                'citymunCode' => 43422,
                'citymunDesc' => 'PILA',
                'provCode' => 434,
            ),
            427 => 
            array (
                'citymunCode' => 43423,
                'citymunDesc' => 'RIZAL',
                'provCode' => 434,
            ),
            428 => 
            array (
                'citymunCode' => 43424,
                'citymunDesc' => 'SAN PABLO CITY',
                'provCode' => 434,
            ),
            429 => 
            array (
                'citymunCode' => 43425,
                'citymunDesc' => 'CITY OF SAN PEDRO',
                'provCode' => 434,
            ),
            430 => 
            array (
                'citymunCode' => 43426,
            'citymunDesc' => 'SANTA CRUZ (Capital)',
                'provCode' => 434,
            ),
            431 => 
            array (
                'citymunCode' => 43427,
                'citymunDesc' => 'SANTA MARIA',
                'provCode' => 434,
            ),
            432 => 
            array (
                'citymunCode' => 43428,
                'citymunDesc' => 'CITY OF SANTA ROSA',
                'provCode' => 434,
            ),
            433 => 
            array (
                'citymunCode' => 43429,
                'citymunDesc' => 'SINILOAN',
                'provCode' => 434,
            ),
            434 => 
            array (
                'citymunCode' => 43430,
                'citymunDesc' => 'VICTORIA',
                'provCode' => 434,
            ),
            435 => 
            array (
                'citymunCode' => 45601,
                'citymunDesc' => 'AGDANGAN',
                'provCode' => 456,
            ),
            436 => 
            array (
                'citymunCode' => 45602,
                'citymunDesc' => 'ALABAT',
                'provCode' => 456,
            ),
            437 => 
            array (
                'citymunCode' => 45603,
                'citymunDesc' => 'ATIMONAN',
                'provCode' => 456,
            ),
            438 => 
            array (
                'citymunCode' => 45605,
                'citymunDesc' => 'BUENAVISTA',
                'provCode' => 456,
            ),
            439 => 
            array (
                'citymunCode' => 45606,
                'citymunDesc' => 'BURDEOS',
                'provCode' => 456,
            ),
            440 => 
            array (
                'citymunCode' => 45607,
                'citymunDesc' => 'CALAUAG',
                'provCode' => 456,
            ),
            441 => 
            array (
                'citymunCode' => 45608,
                'citymunDesc' => 'CANDELARIA',
                'provCode' => 456,
            ),
            442 => 
            array (
                'citymunCode' => 45610,
                'citymunDesc' => 'CATANAUAN',
                'provCode' => 456,
            ),
            443 => 
            array (
                'citymunCode' => 45615,
                'citymunDesc' => 'DOLORES',
                'provCode' => 456,
            ),
            444 => 
            array (
                'citymunCode' => 45616,
                'citymunDesc' => 'GENERAL LUNA',
                'provCode' => 456,
            ),
            445 => 
            array (
                'citymunCode' => 45617,
                'citymunDesc' => 'GENERAL NAKAR',
                'provCode' => 456,
            ),
            446 => 
            array (
                'citymunCode' => 45618,
                'citymunDesc' => 'GUINAYANGAN',
                'provCode' => 456,
            ),
            447 => 
            array (
                'citymunCode' => 45619,
                'citymunDesc' => 'GUMACA',
                'provCode' => 456,
            ),
            448 => 
            array (
                'citymunCode' => 45620,
                'citymunDesc' => 'INFANTA',
                'provCode' => 456,
            ),
            449 => 
            array (
                'citymunCode' => 45621,
                'citymunDesc' => 'JOMALIG',
                'provCode' => 456,
            ),
            450 => 
            array (
                'citymunCode' => 45622,
                'citymunDesc' => 'LOPEZ',
                'provCode' => 456,
            ),
            451 => 
            array (
                'citymunCode' => 45623,
                'citymunDesc' => 'LUCBAN',
                'provCode' => 456,
            ),
            452 => 
            array (
                'citymunCode' => 45624,
            'citymunDesc' => 'LUCENA CITY (Capital)',
                'provCode' => 456,
            ),
            453 => 
            array (
                'citymunCode' => 45625,
                'citymunDesc' => 'MACALELON',
                'provCode' => 456,
            ),
            454 => 
            array (
                'citymunCode' => 45627,
                'citymunDesc' => 'MAUBAN',
                'provCode' => 456,
            ),
            455 => 
            array (
                'citymunCode' => 45628,
                'citymunDesc' => 'MULANAY',
                'provCode' => 456,
            ),
            456 => 
            array (
                'citymunCode' => 45629,
                'citymunDesc' => 'PADRE BURGOS',
                'provCode' => 456,
            ),
            457 => 
            array (
                'citymunCode' => 45630,
                'citymunDesc' => 'PAGBILAO',
                'provCode' => 456,
            ),
            458 => 
            array (
                'citymunCode' => 45631,
                'citymunDesc' => 'PANUKULAN',
                'provCode' => 456,
            ),
            459 => 
            array (
                'citymunCode' => 45632,
                'citymunDesc' => 'PATNANUNGAN',
                'provCode' => 456,
            ),
            460 => 
            array (
                'citymunCode' => 45633,
                'citymunDesc' => 'PEREZ',
                'provCode' => 456,
            ),
            461 => 
            array (
                'citymunCode' => 45634,
                'citymunDesc' => 'PITOGO',
                'provCode' => 456,
            ),
            462 => 
            array (
                'citymunCode' => 45635,
                'citymunDesc' => 'PLARIDEL',
                'provCode' => 456,
            ),
            463 => 
            array (
                'citymunCode' => 45636,
                'citymunDesc' => 'POLILLO',
                'provCode' => 456,
            ),
            464 => 
            array (
                'citymunCode' => 45637,
                'citymunDesc' => 'QUEZON',
                'provCode' => 456,
            ),
            465 => 
            array (
                'citymunCode' => 45638,
                'citymunDesc' => 'REAL',
                'provCode' => 456,
            ),
            466 => 
            array (
                'citymunCode' => 45639,
                'citymunDesc' => 'SAMPALOC',
                'provCode' => 456,
            ),
            467 => 
            array (
                'citymunCode' => 45640,
                'citymunDesc' => 'SAN ANDRES',
                'provCode' => 456,
            ),
            468 => 
            array (
                'citymunCode' => 45641,
                'citymunDesc' => 'SAN ANTONIO',
                'provCode' => 456,
            ),
            469 => 
            array (
                'citymunCode' => 45642,
            'citymunDesc' => 'SAN FRANCISCO (AURORA)',
                'provCode' => 456,
            ),
            470 => 
            array (
                'citymunCode' => 45644,
                'citymunDesc' => 'SAN NARCISO',
                'provCode' => 456,
            ),
            471 => 
            array (
                'citymunCode' => 45645,
                'citymunDesc' => 'SARIAYA',
                'provCode' => 456,
            ),
            472 => 
            array (
                'citymunCode' => 45646,
                'citymunDesc' => 'TAGKAWAYAN',
                'provCode' => 456,
            ),
            473 => 
            array (
                'citymunCode' => 45647,
                'citymunDesc' => 'CITY OF TAYABAS',
                'provCode' => 456,
            ),
            474 => 
            array (
                'citymunCode' => 45648,
                'citymunDesc' => 'TIAONG',
                'provCode' => 456,
            ),
            475 => 
            array (
                'citymunCode' => 45649,
                'citymunDesc' => 'UNISAN',
                'provCode' => 456,
            ),
            476 => 
            array (
                'citymunCode' => 45801,
                'citymunDesc' => 'ANGONO',
                'provCode' => 458,
            ),
            477 => 
            array (
                'citymunCode' => 45802,
                'citymunDesc' => 'CITY OF ANTIPOLO',
                'provCode' => 458,
            ),
            478 => 
            array (
                'citymunCode' => 45803,
                'citymunDesc' => 'BARAS',
                'provCode' => 458,
            ),
            479 => 
            array (
                'citymunCode' => 45804,
                'citymunDesc' => 'BINANGONAN',
                'provCode' => 458,
            ),
            480 => 
            array (
                'citymunCode' => 45805,
                'citymunDesc' => 'CAINTA',
                'provCode' => 458,
            ),
            481 => 
            array (
                'citymunCode' => 45806,
                'citymunDesc' => 'CARDONA',
                'provCode' => 458,
            ),
            482 => 
            array (
                'citymunCode' => 45807,
                'citymunDesc' => 'JALA-JALA',
                'provCode' => 458,
            ),
            483 => 
            array (
                'citymunCode' => 45808,
            'citymunDesc' => 'RODRIGUEZ (MONTALBAN)',
                'provCode' => 458,
            ),
            484 => 
            array (
                'citymunCode' => 45809,
                'citymunDesc' => 'MORONG',
                'provCode' => 458,
            ),
            485 => 
            array (
                'citymunCode' => 45810,
                'citymunDesc' => 'PILILLA',
                'provCode' => 458,
            ),
            486 => 
            array (
                'citymunCode' => 45811,
                'citymunDesc' => 'SAN MATEO',
                'provCode' => 458,
            ),
            487 => 
            array (
                'citymunCode' => 45812,
                'citymunDesc' => 'TANAY',
                'provCode' => 458,
            ),
            488 => 
            array (
                'citymunCode' => 45813,
                'citymunDesc' => 'TAYTAY',
                'provCode' => 458,
            ),
            489 => 
            array (
                'citymunCode' => 45814,
                'citymunDesc' => 'TERESA',
                'provCode' => 458,
            ),
            490 => 
            array (
                'citymunCode' => 50501,
                'citymunDesc' => 'BACACAY',
                'provCode' => 505,
            ),
            491 => 
            array (
                'citymunCode' => 50502,
                'citymunDesc' => 'CAMALIG',
                'provCode' => 505,
            ),
            492 => 
            array (
                'citymunCode' => 50503,
            'citymunDesc' => 'DARAGA (LOCSIN)',
                'provCode' => 505,
            ),
            493 => 
            array (
                'citymunCode' => 50504,
                'citymunDesc' => 'GUINOBATAN',
                'provCode' => 505,
            ),
            494 => 
            array (
                'citymunCode' => 50505,
                'citymunDesc' => 'JOVELLAR',
                'provCode' => 505,
            ),
            495 => 
            array (
                'citymunCode' => 50506,
            'citymunDesc' => 'LEGAZPI CITY (Capital)',
                'provCode' => 505,
            ),
            496 => 
            array (
                'citymunCode' => 50507,
                'citymunDesc' => 'LIBON',
                'provCode' => 505,
            ),
            497 => 
            array (
                'citymunCode' => 50508,
                'citymunDesc' => 'CITY OF LIGAO',
                'provCode' => 505,
            ),
            498 => 
            array (
                'citymunCode' => 50509,
                'citymunDesc' => 'MALILIPOT',
                'provCode' => 505,
            ),
            499 => 
            array (
                'citymunCode' => 50510,
                'citymunDesc' => 'MALINAO',
                'provCode' => 505,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'citymunCode' => 50511,
                'citymunDesc' => 'MANITO',
                'provCode' => 505,
            ),
            1 => 
            array (
                'citymunCode' => 50512,
                'citymunDesc' => 'OAS',
                'provCode' => 505,
            ),
            2 => 
            array (
                'citymunCode' => 50513,
                'citymunDesc' => 'PIO DURAN',
                'provCode' => 505,
            ),
            3 => 
            array (
                'citymunCode' => 50514,
                'citymunDesc' => 'POLANGUI',
                'provCode' => 505,
            ),
            4 => 
            array (
                'citymunCode' => 50515,
                'citymunDesc' => 'RAPU-RAPU',
                'provCode' => 505,
            ),
            5 => 
            array (
                'citymunCode' => 50516,
            'citymunDesc' => 'SANTO DOMINGO (LIBOG)',
                'provCode' => 505,
            ),
            6 => 
            array (
                'citymunCode' => 50517,
                'citymunDesc' => 'CITY OF TABACO',
                'provCode' => 505,
            ),
            7 => 
            array (
                'citymunCode' => 50518,
                'citymunDesc' => 'TIWI',
                'provCode' => 505,
            ),
            8 => 
            array (
                'citymunCode' => 51601,
                'citymunDesc' => 'BASUD',
                'provCode' => 516,
            ),
            9 => 
            array (
                'citymunCode' => 51602,
                'citymunDesc' => 'CAPALONGA',
                'provCode' => 516,
            ),
            10 => 
            array (
                'citymunCode' => 51603,
            'citymunDesc' => 'DAET (Capital)',
                'provCode' => 516,
            ),
            11 => 
            array (
                'citymunCode' => 51604,
            'citymunDesc' => 'SAN LORENZO RUIZ (IMELDA)',
                'provCode' => 516,
            ),
            12 => 
            array (
                'citymunCode' => 51605,
                'citymunDesc' => 'JOSE PANGANIBAN',
                'provCode' => 516,
            ),
            13 => 
            array (
                'citymunCode' => 51606,
                'citymunDesc' => 'LABO',
                'provCode' => 516,
            ),
            14 => 
            array (
                'citymunCode' => 51607,
                'citymunDesc' => 'MERCEDES',
                'provCode' => 516,
            ),
            15 => 
            array (
                'citymunCode' => 51608,
                'citymunDesc' => 'PARACALE',
                'provCode' => 516,
            ),
            16 => 
            array (
                'citymunCode' => 51609,
                'citymunDesc' => 'SAN VICENTE',
                'provCode' => 516,
            ),
            17 => 
            array (
                'citymunCode' => 51610,
                'citymunDesc' => 'SANTA ELENA',
                'provCode' => 516,
            ),
            18 => 
            array (
                'citymunCode' => 51611,
                'citymunDesc' => 'TALISAY',
                'provCode' => 516,
            ),
            19 => 
            array (
                'citymunCode' => 51612,
                'citymunDesc' => 'VINZONS',
                'provCode' => 516,
            ),
            20 => 
            array (
                'citymunCode' => 51701,
                'citymunDesc' => 'BAAO',
                'provCode' => 517,
            ),
            21 => 
            array (
                'citymunCode' => 51702,
                'citymunDesc' => 'BALATAN',
                'provCode' => 517,
            ),
            22 => 
            array (
                'citymunCode' => 51703,
                'citymunDesc' => 'BATO',
                'provCode' => 517,
            ),
            23 => 
            array (
                'citymunCode' => 51704,
                'citymunDesc' => 'BOMBON',
                'provCode' => 517,
            ),
            24 => 
            array (
                'citymunCode' => 51705,
                'citymunDesc' => 'BUHI',
                'provCode' => 517,
            ),
            25 => 
            array (
                'citymunCode' => 51706,
                'citymunDesc' => 'BULA',
                'provCode' => 517,
            ),
            26 => 
            array (
                'citymunCode' => 51707,
                'citymunDesc' => 'CABUSAO',
                'provCode' => 517,
            ),
            27 => 
            array (
                'citymunCode' => 51708,
                'citymunDesc' => 'CALABANGA',
                'provCode' => 517,
            ),
            28 => 
            array (
                'citymunCode' => 51709,
                'citymunDesc' => 'CAMALIGAN',
                'provCode' => 517,
            ),
            29 => 
            array (
                'citymunCode' => 51710,
                'citymunDesc' => 'CANAMAN',
                'provCode' => 517,
            ),
            30 => 
            array (
                'citymunCode' => 51711,
                'citymunDesc' => 'CARAMOAN',
                'provCode' => 517,
            ),
            31 => 
            array (
                'citymunCode' => 51712,
                'citymunDesc' => 'DEL GALLEGO',
                'provCode' => 517,
            ),
            32 => 
            array (
                'citymunCode' => 51713,
                'citymunDesc' => 'GAINZA',
                'provCode' => 517,
            ),
            33 => 
            array (
                'citymunCode' => 51714,
                'citymunDesc' => 'GARCHITORENA',
                'provCode' => 517,
            ),
            34 => 
            array (
                'citymunCode' => 51715,
                'citymunDesc' => 'GOA',
                'provCode' => 517,
            ),
            35 => 
            array (
                'citymunCode' => 51716,
                'citymunDesc' => 'IRIGA CITY',
                'provCode' => 517,
            ),
            36 => 
            array (
                'citymunCode' => 51717,
                'citymunDesc' => 'LAGONOY',
                'provCode' => 517,
            ),
            37 => 
            array (
                'citymunCode' => 51718,
                'citymunDesc' => 'LIBMANAN',
                'provCode' => 517,
            ),
            38 => 
            array (
                'citymunCode' => 51719,
                'citymunDesc' => 'LUPI',
                'provCode' => 517,
            ),
            39 => 
            array (
                'citymunCode' => 51720,
                'citymunDesc' => 'MAGARAO',
                'provCode' => 517,
            ),
            40 => 
            array (
                'citymunCode' => 51721,
                'citymunDesc' => 'MILAOR',
                'provCode' => 517,
            ),
            41 => 
            array (
                'citymunCode' => 51722,
                'citymunDesc' => 'MINALABAC',
                'provCode' => 517,
            ),
            42 => 
            array (
                'citymunCode' => 51723,
                'citymunDesc' => 'NABUA',
                'provCode' => 517,
            ),
            43 => 
            array (
                'citymunCode' => 51724,
                'citymunDesc' => 'NAGA CITY',
                'provCode' => 517,
            ),
            44 => 
            array (
                'citymunCode' => 51725,
                'citymunDesc' => 'OCAMPO',
                'provCode' => 517,
            ),
            45 => 
            array (
                'citymunCode' => 51726,
                'citymunDesc' => 'PAMPLONA',
                'provCode' => 517,
            ),
            46 => 
            array (
                'citymunCode' => 51727,
                'citymunDesc' => 'PASACAO',
                'provCode' => 517,
            ),
            47 => 
            array (
                'citymunCode' => 51728,
            'citymunDesc' => 'PILI (Capital)',
                'provCode' => 517,
            ),
            48 => 
            array (
                'citymunCode' => 51729,
            'citymunDesc' => 'PRESENTACION (PARUBCAN)',
                'provCode' => 517,
            ),
            49 => 
            array (
                'citymunCode' => 51730,
                'citymunDesc' => 'RAGAY',
                'provCode' => 517,
            ),
            50 => 
            array (
                'citymunCode' => 51731,
                'citymunDesc' => 'SAGÑAY',
                'provCode' => 517,
            ),
            51 => 
            array (
                'citymunCode' => 51732,
                'citymunDesc' => 'SAN FERNANDO',
                'provCode' => 517,
            ),
            52 => 
            array (
                'citymunCode' => 51733,
                'citymunDesc' => 'SAN JOSE',
                'provCode' => 517,
            ),
            53 => 
            array (
                'citymunCode' => 51734,
                'citymunDesc' => 'SIPOCOT',
                'provCode' => 517,
            ),
            54 => 
            array (
                'citymunCode' => 51735,
                'citymunDesc' => 'SIRUMA',
                'provCode' => 517,
            ),
            55 => 
            array (
                'citymunCode' => 51736,
                'citymunDesc' => 'TIGAON',
                'provCode' => 517,
            ),
            56 => 
            array (
                'citymunCode' => 51737,
                'citymunDesc' => 'TINAMBAC',
                'provCode' => 517,
            ),
            57 => 
            array (
                'citymunCode' => 52001,
                'citymunDesc' => 'BAGAMANOC',
                'provCode' => 520,
            ),
            58 => 
            array (
                'citymunCode' => 52002,
                'citymunDesc' => 'BARAS',
                'provCode' => 520,
            ),
            59 => 
            array (
                'citymunCode' => 52003,
                'citymunDesc' => 'BATO',
                'provCode' => 520,
            ),
            60 => 
            array (
                'citymunCode' => 52004,
                'citymunDesc' => 'CARAMORAN',
                'provCode' => 520,
            ),
            61 => 
            array (
                'citymunCode' => 52005,
                'citymunDesc' => 'GIGMOTO',
                'provCode' => 520,
            ),
            62 => 
            array (
                'citymunCode' => 52006,
                'citymunDesc' => 'PANDAN',
                'provCode' => 520,
            ),
            63 => 
            array (
                'citymunCode' => 52007,
            'citymunDesc' => 'PANGANIBAN (PAYO)',
                'provCode' => 520,
            ),
            64 => 
            array (
                'citymunCode' => 52008,
            'citymunDesc' => 'SAN ANDRES (CALOLBON)',
                'provCode' => 520,
            ),
            65 => 
            array (
                'citymunCode' => 52009,
                'citymunDesc' => 'SAN MIGUEL',
                'provCode' => 520,
            ),
            66 => 
            array (
                'citymunCode' => 52010,
                'citymunDesc' => 'VIGA',
                'provCode' => 520,
            ),
            67 => 
            array (
                'citymunCode' => 52011,
            'citymunDesc' => 'VIRAC (Capital)',
                'provCode' => 520,
            ),
            68 => 
            array (
                'citymunCode' => 54101,
                'citymunDesc' => 'AROROY',
                'provCode' => 541,
            ),
            69 => 
            array (
                'citymunCode' => 54102,
                'citymunDesc' => 'BALENO',
                'provCode' => 541,
            ),
            70 => 
            array (
                'citymunCode' => 54103,
                'citymunDesc' => 'BALUD',
                'provCode' => 541,
            ),
            71 => 
            array (
                'citymunCode' => 54104,
                'citymunDesc' => 'BATUAN',
                'provCode' => 541,
            ),
            72 => 
            array (
                'citymunCode' => 54105,
                'citymunDesc' => 'CATAINGAN',
                'provCode' => 541,
            ),
            73 => 
            array (
                'citymunCode' => 54106,
                'citymunDesc' => 'CAWAYAN',
                'provCode' => 541,
            ),
            74 => 
            array (
                'citymunCode' => 54107,
                'citymunDesc' => 'CLAVERIA',
                'provCode' => 541,
            ),
            75 => 
            array (
                'citymunCode' => 54108,
                'citymunDesc' => 'DIMASALANG',
                'provCode' => 541,
            ),
            76 => 
            array (
                'citymunCode' => 54109,
                'citymunDesc' => 'ESPERANZA',
                'provCode' => 541,
            ),
            77 => 
            array (
                'citymunCode' => 54110,
                'citymunDesc' => 'MANDAON',
                'provCode' => 541,
            ),
            78 => 
            array (
                'citymunCode' => 54111,
            'citymunDesc' => 'CITY OF MASBATE (Capital)',
                'provCode' => 541,
            ),
            79 => 
            array (
                'citymunCode' => 54112,
                'citymunDesc' => 'MILAGROS',
                'provCode' => 541,
            ),
            80 => 
            array (
                'citymunCode' => 54113,
                'citymunDesc' => 'MOBO',
                'provCode' => 541,
            ),
            81 => 
            array (
                'citymunCode' => 54114,
                'citymunDesc' => 'MONREAL',
                'provCode' => 541,
            ),
            82 => 
            array (
                'citymunCode' => 54115,
                'citymunDesc' => 'PALANAS',
                'provCode' => 541,
            ),
            83 => 
            array (
                'citymunCode' => 54116,
            'citymunDesc' => 'PIO V. CORPUZ (LIMBUHAN)',
                'provCode' => 541,
            ),
            84 => 
            array (
                'citymunCode' => 54117,
                'citymunDesc' => 'PLACER',
                'provCode' => 541,
            ),
            85 => 
            array (
                'citymunCode' => 54118,
                'citymunDesc' => 'SAN FERNANDO',
                'provCode' => 541,
            ),
            86 => 
            array (
                'citymunCode' => 54119,
                'citymunDesc' => 'SAN JACINTO',
                'provCode' => 541,
            ),
            87 => 
            array (
                'citymunCode' => 54120,
                'citymunDesc' => 'SAN PASCUAL',
                'provCode' => 541,
            ),
            88 => 
            array (
                'citymunCode' => 54121,
                'citymunDesc' => 'USON',
                'provCode' => 541,
            ),
            89 => 
            array (
                'citymunCode' => 56202,
                'citymunDesc' => 'BARCELONA',
                'provCode' => 562,
            ),
            90 => 
            array (
                'citymunCode' => 56203,
                'citymunDesc' => 'BULAN',
                'provCode' => 562,
            ),
            91 => 
            array (
                'citymunCode' => 56204,
                'citymunDesc' => 'BULUSAN',
                'provCode' => 562,
            ),
            92 => 
            array (
                'citymunCode' => 56205,
                'citymunDesc' => 'CASIGURAN',
                'provCode' => 562,
            ),
            93 => 
            array (
                'citymunCode' => 56206,
                'citymunDesc' => 'CASTILLA',
                'provCode' => 562,
            ),
            94 => 
            array (
                'citymunCode' => 56207,
                'citymunDesc' => 'DONSOL',
                'provCode' => 562,
            ),
            95 => 
            array (
                'citymunCode' => 56208,
                'citymunDesc' => 'GUBAT',
                'provCode' => 562,
            ),
            96 => 
            array (
                'citymunCode' => 56209,
                'citymunDesc' => 'IROSIN',
                'provCode' => 562,
            ),
            97 => 
            array (
                'citymunCode' => 56210,
                'citymunDesc' => 'JUBAN',
                'provCode' => 562,
            ),
            98 => 
            array (
                'citymunCode' => 56211,
                'citymunDesc' => 'MAGALLANES',
                'provCode' => 562,
            ),
            99 => 
            array (
                'citymunCode' => 56212,
                'citymunDesc' => 'MATNOG',
                'provCode' => 562,
            ),
            100 => 
            array (
                'citymunCode' => 56213,
                'citymunDesc' => 'PILAR',
                'provCode' => 562,
            ),
            101 => 
            array (
                'citymunCode' => 56214,
                'citymunDesc' => 'PRIETO DIAZ',
                'provCode' => 562,
            ),
            102 => 
            array (
                'citymunCode' => 56215,
                'citymunDesc' => 'SANTA MAGDALENA',
                'provCode' => 562,
            ),
            103 => 
            array (
                'citymunCode' => 56216,
            'citymunDesc' => 'CITY OF SORSOGON (Capital)',
                'provCode' => 562,
            ),
            104 => 
            array (
                'citymunCode' => 60401,
                'citymunDesc' => 'ALTAVAS',
                'provCode' => 604,
            ),
            105 => 
            array (
                'citymunCode' => 60402,
                'citymunDesc' => 'BALETE',
                'provCode' => 604,
            ),
            106 => 
            array (
                'citymunCode' => 60403,
                'citymunDesc' => 'BANGA',
                'provCode' => 604,
            ),
            107 => 
            array (
                'citymunCode' => 60404,
                'citymunDesc' => 'BATAN',
                'provCode' => 604,
            ),
            108 => 
            array (
                'citymunCode' => 60405,
                'citymunDesc' => 'BURUANGA',
                'provCode' => 604,
            ),
            109 => 
            array (
                'citymunCode' => 60406,
                'citymunDesc' => 'IBAJAY',
                'provCode' => 604,
            ),
            110 => 
            array (
                'citymunCode' => 60407,
            'citymunDesc' => 'KALIBO (Capital)',
                'provCode' => 604,
            ),
            111 => 
            array (
                'citymunCode' => 60408,
                'citymunDesc' => 'LEZO',
                'provCode' => 604,
            ),
            112 => 
            array (
                'citymunCode' => 60409,
                'citymunDesc' => 'LIBACAO',
                'provCode' => 604,
            ),
            113 => 
            array (
                'citymunCode' => 60410,
                'citymunDesc' => 'MADALAG',
                'provCode' => 604,
            ),
            114 => 
            array (
                'citymunCode' => 60411,
                'citymunDesc' => 'MAKATO',
                'provCode' => 604,
            ),
            115 => 
            array (
                'citymunCode' => 60412,
                'citymunDesc' => 'MALAY',
                'provCode' => 604,
            ),
            116 => 
            array (
                'citymunCode' => 60413,
                'citymunDesc' => 'MALINAO',
                'provCode' => 604,
            ),
            117 => 
            array (
                'citymunCode' => 60414,
                'citymunDesc' => 'NABAS',
                'provCode' => 604,
            ),
            118 => 
            array (
                'citymunCode' => 60415,
                'citymunDesc' => 'NEW WASHINGTON',
                'provCode' => 604,
            ),
            119 => 
            array (
                'citymunCode' => 60416,
                'citymunDesc' => 'NUMANCIA',
                'provCode' => 604,
            ),
            120 => 
            array (
                'citymunCode' => 60417,
                'citymunDesc' => 'TANGALAN',
                'provCode' => 604,
            ),
            121 => 
            array (
                'citymunCode' => 60601,
                'citymunDesc' => 'ANINI-Y',
                'provCode' => 606,
            ),
            122 => 
            array (
                'citymunCode' => 60602,
                'citymunDesc' => 'BARBAZA',
                'provCode' => 606,
            ),
            123 => 
            array (
                'citymunCode' => 60603,
                'citymunDesc' => 'BELISON',
                'provCode' => 606,
            ),
            124 => 
            array (
                'citymunCode' => 60604,
                'citymunDesc' => 'BUGASONG',
                'provCode' => 606,
            ),
            125 => 
            array (
                'citymunCode' => 60605,
                'citymunDesc' => 'CALUYA',
                'provCode' => 606,
            ),
            126 => 
            array (
                'citymunCode' => 60606,
                'citymunDesc' => 'CULASI',
                'provCode' => 606,
            ),
            127 => 
            array (
                'citymunCode' => 60607,
            'citymunDesc' => 'TOBIAS FORNIER (DAO)',
                'provCode' => 606,
            ),
            128 => 
            array (
                'citymunCode' => 60608,
                'citymunDesc' => 'HAMTIC',
                'provCode' => 606,
            ),
            129 => 
            array (
                'citymunCode' => 60609,
                'citymunDesc' => 'LAUA-AN',
                'provCode' => 606,
            ),
            130 => 
            array (
                'citymunCode' => 60610,
                'citymunDesc' => 'LIBERTAD',
                'provCode' => 606,
            ),
            131 => 
            array (
                'citymunCode' => 60611,
                'citymunDesc' => 'PANDAN',
                'provCode' => 606,
            ),
            132 => 
            array (
                'citymunCode' => 60612,
                'citymunDesc' => 'PATNONGON',
                'provCode' => 606,
            ),
            133 => 
            array (
                'citymunCode' => 60613,
            'citymunDesc' => 'SAN JOSE (Capital)',
                'provCode' => 606,
            ),
            134 => 
            array (
                'citymunCode' => 60614,
                'citymunDesc' => 'SAN REMIGIO',
                'provCode' => 606,
            ),
            135 => 
            array (
                'citymunCode' => 60615,
                'citymunDesc' => 'SEBASTE',
                'provCode' => 606,
            ),
            136 => 
            array (
                'citymunCode' => 60616,
                'citymunDesc' => 'SIBALOM',
                'provCode' => 606,
            ),
            137 => 
            array (
                'citymunCode' => 60617,
                'citymunDesc' => 'TIBIAO',
                'provCode' => 606,
            ),
            138 => 
            array (
                'citymunCode' => 60618,
                'citymunDesc' => 'VALDERRAMA',
                'provCode' => 606,
            ),
            139 => 
            array (
                'citymunCode' => 61901,
                'citymunDesc' => 'CUARTERO',
                'provCode' => 619,
            ),
            140 => 
            array (
                'citymunCode' => 61902,
                'citymunDesc' => 'DAO',
                'provCode' => 619,
            ),
            141 => 
            array (
                'citymunCode' => 61903,
                'citymunDesc' => 'DUMALAG',
                'provCode' => 619,
            ),
            142 => 
            array (
                'citymunCode' => 61904,
                'citymunDesc' => 'DUMARAO',
                'provCode' => 619,
            ),
            143 => 
            array (
                'citymunCode' => 61905,
                'citymunDesc' => 'IVISAN',
                'provCode' => 619,
            ),
            144 => 
            array (
                'citymunCode' => 61906,
                'citymunDesc' => 'JAMINDAN',
                'provCode' => 619,
            ),
            145 => 
            array (
                'citymunCode' => 61907,
                'citymunDesc' => 'MA-AYON',
                'provCode' => 619,
            ),
            146 => 
            array (
                'citymunCode' => 61908,
                'citymunDesc' => 'MAMBUSAO',
                'provCode' => 619,
            ),
            147 => 
            array (
                'citymunCode' => 61909,
                'citymunDesc' => 'PANAY',
                'provCode' => 619,
            ),
            148 => 
            array (
                'citymunCode' => 61910,
                'citymunDesc' => 'PANITAN',
                'provCode' => 619,
            ),
            149 => 
            array (
                'citymunCode' => 61911,
                'citymunDesc' => 'PILAR',
                'provCode' => 619,
            ),
            150 => 
            array (
                'citymunCode' => 61912,
                'citymunDesc' => 'PONTEVEDRA',
                'provCode' => 619,
            ),
            151 => 
            array (
                'citymunCode' => 61913,
                'citymunDesc' => 'PRESIDENT ROXAS',
                'provCode' => 619,
            ),
            152 => 
            array (
                'citymunCode' => 61914,
            'citymunDesc' => 'ROXAS CITY (Capital)',
                'provCode' => 619,
            ),
            153 => 
            array (
                'citymunCode' => 61915,
                'citymunDesc' => 'SAPI-AN',
                'provCode' => 619,
            ),
            154 => 
            array (
                'citymunCode' => 61916,
                'citymunDesc' => 'SIGMA',
                'provCode' => 619,
            ),
            155 => 
            array (
                'citymunCode' => 61917,
                'citymunDesc' => 'TAPAZ',
                'provCode' => 619,
            ),
            156 => 
            array (
                'citymunCode' => 63001,
                'citymunDesc' => 'AJUY',
                'provCode' => 630,
            ),
            157 => 
            array (
                'citymunCode' => 63002,
                'citymunDesc' => 'ALIMODIAN',
                'provCode' => 630,
            ),
            158 => 
            array (
                'citymunCode' => 63003,
                'citymunDesc' => 'ANILAO',
                'provCode' => 630,
            ),
            159 => 
            array (
                'citymunCode' => 63004,
                'citymunDesc' => 'BADIANGAN',
                'provCode' => 630,
            ),
            160 => 
            array (
                'citymunCode' => 63005,
                'citymunDesc' => 'BALASAN',
                'provCode' => 630,
            ),
            161 => 
            array (
                'citymunCode' => 63006,
                'citymunDesc' => 'BANATE',
                'provCode' => 630,
            ),
            162 => 
            array (
                'citymunCode' => 63007,
                'citymunDesc' => 'BAROTAC NUEVO',
                'provCode' => 630,
            ),
            163 => 
            array (
                'citymunCode' => 63008,
                'citymunDesc' => 'BAROTAC VIEJO',
                'provCode' => 630,
            ),
            164 => 
            array (
                'citymunCode' => 63009,
                'citymunDesc' => 'BATAD',
                'provCode' => 630,
            ),
            165 => 
            array (
                'citymunCode' => 63010,
                'citymunDesc' => 'BINGAWAN',
                'provCode' => 630,
            ),
            166 => 
            array (
                'citymunCode' => 63012,
                'citymunDesc' => 'CABATUAN',
                'provCode' => 630,
            ),
            167 => 
            array (
                'citymunCode' => 63013,
                'citymunDesc' => 'CALINOG',
                'provCode' => 630,
            ),
            168 => 
            array (
                'citymunCode' => 63014,
                'citymunDesc' => 'CARLES',
                'provCode' => 630,
            ),
            169 => 
            array (
                'citymunCode' => 63015,
                'citymunDesc' => 'CONCEPCION',
                'provCode' => 630,
            ),
            170 => 
            array (
                'citymunCode' => 63016,
                'citymunDesc' => 'DINGLE',
                'provCode' => 630,
            ),
            171 => 
            array (
                'citymunCode' => 63017,
                'citymunDesc' => 'DUEÑAS',
                'provCode' => 630,
            ),
            172 => 
            array (
                'citymunCode' => 63018,
                'citymunDesc' => 'DUMANGAS',
                'provCode' => 630,
            ),
            173 => 
            array (
                'citymunCode' => 63019,
                'citymunDesc' => 'ESTANCIA',
                'provCode' => 630,
            ),
            174 => 
            array (
                'citymunCode' => 63020,
                'citymunDesc' => 'GUIMBAL',
                'provCode' => 630,
            ),
            175 => 
            array (
                'citymunCode' => 63021,
                'citymunDesc' => 'IGBARAS',
                'provCode' => 630,
            ),
            176 => 
            array (
                'citymunCode' => 63022,
            'citymunDesc' => 'ILOILO CITY (Capital)',
                'provCode' => 630,
            ),
            177 => 
            array (
                'citymunCode' => 63023,
                'citymunDesc' => 'JANIUAY',
                'provCode' => 630,
            ),
            178 => 
            array (
                'citymunCode' => 63025,
                'citymunDesc' => 'LAMBUNAO',
                'provCode' => 630,
            ),
            179 => 
            array (
                'citymunCode' => 63026,
                'citymunDesc' => 'LEGANES',
                'provCode' => 630,
            ),
            180 => 
            array (
                'citymunCode' => 63027,
                'citymunDesc' => 'LEMERY',
                'provCode' => 630,
            ),
            181 => 
            array (
                'citymunCode' => 63028,
                'citymunDesc' => 'LEON',
                'provCode' => 630,
            ),
            182 => 
            array (
                'citymunCode' => 63029,
                'citymunDesc' => 'MAASIN',
                'provCode' => 630,
            ),
            183 => 
            array (
                'citymunCode' => 63030,
                'citymunDesc' => 'MIAGAO',
                'provCode' => 630,
            ),
            184 => 
            array (
                'citymunCode' => 63031,
                'citymunDesc' => 'MINA',
                'provCode' => 630,
            ),
            185 => 
            array (
                'citymunCode' => 63032,
                'citymunDesc' => 'NEW LUCENA',
                'provCode' => 630,
            ),
            186 => 
            array (
                'citymunCode' => 63034,
                'citymunDesc' => 'OTON',
                'provCode' => 630,
            ),
            187 => 
            array (
                'citymunCode' => 63035,
                'citymunDesc' => 'CITY OF PASSI',
                'provCode' => 630,
            ),
            188 => 
            array (
                'citymunCode' => 63036,
                'citymunDesc' => 'PAVIA',
                'provCode' => 630,
            ),
            189 => 
            array (
                'citymunCode' => 63037,
                'citymunDesc' => 'POTOTAN',
                'provCode' => 630,
            ),
            190 => 
            array (
                'citymunCode' => 63038,
                'citymunDesc' => 'SAN DIONISIO',
                'provCode' => 630,
            ),
            191 => 
            array (
                'citymunCode' => 63039,
                'citymunDesc' => 'SAN ENRIQUE',
                'provCode' => 630,
            ),
            192 => 
            array (
                'citymunCode' => 63040,
                'citymunDesc' => 'SAN JOAQUIN',
                'provCode' => 630,
            ),
            193 => 
            array (
                'citymunCode' => 63041,
                'citymunDesc' => 'SAN MIGUEL',
                'provCode' => 630,
            ),
            194 => 
            array (
                'citymunCode' => 63042,
                'citymunDesc' => 'SAN RAFAEL',
                'provCode' => 630,
            ),
            195 => 
            array (
                'citymunCode' => 63043,
                'citymunDesc' => 'SANTA BARBARA',
                'provCode' => 630,
            ),
            196 => 
            array (
                'citymunCode' => 63044,
                'citymunDesc' => 'SARA',
                'provCode' => 630,
            ),
            197 => 
            array (
                'citymunCode' => 63045,
                'citymunDesc' => 'TIGBAUAN',
                'provCode' => 630,
            ),
            198 => 
            array (
                'citymunCode' => 63046,
                'citymunDesc' => 'TUBUNGAN',
                'provCode' => 630,
            ),
            199 => 
            array (
                'citymunCode' => 63047,
                'citymunDesc' => 'ZARRAGA',
                'provCode' => 630,
            ),
            200 => 
            array (
                'citymunCode' => 64501,
            'citymunDesc' => 'BACOLOD CITY (Capital)',
                'provCode' => 645,
            ),
            201 => 
            array (
                'citymunCode' => 64502,
                'citymunDesc' => 'BAGO CITY',
                'provCode' => 645,
            ),
            202 => 
            array (
                'citymunCode' => 64503,
                'citymunDesc' => 'BINALBAGAN',
                'provCode' => 645,
            ),
            203 => 
            array (
                'citymunCode' => 64504,
                'citymunDesc' => 'CADIZ CITY',
                'provCode' => 645,
            ),
            204 => 
            array (
                'citymunCode' => 64505,
                'citymunDesc' => 'CALATRAVA',
                'provCode' => 645,
            ),
            205 => 
            array (
                'citymunCode' => 64506,
                'citymunDesc' => 'CANDONI',
                'provCode' => 645,
            ),
            206 => 
            array (
                'citymunCode' => 64507,
                'citymunDesc' => 'CAUAYAN',
                'provCode' => 645,
            ),
            207 => 
            array (
                'citymunCode' => 64508,
            'citymunDesc' => 'ENRIQUE B. MAGALONA (SARAVIA)',
                'provCode' => 645,
            ),
            208 => 
            array (
                'citymunCode' => 64509,
                'citymunDesc' => 'CITY OF ESCALANTE',
                'provCode' => 645,
            ),
            209 => 
            array (
                'citymunCode' => 64510,
                'citymunDesc' => 'CITY OF HIMAMAYLAN',
                'provCode' => 645,
            ),
            210 => 
            array (
                'citymunCode' => 64511,
                'citymunDesc' => 'HINIGARAN',
                'provCode' => 645,
            ),
            211 => 
            array (
                'citymunCode' => 64512,
            'citymunDesc' => 'HINOBA-AN (ASIA)',
                'provCode' => 645,
            ),
            212 => 
            array (
                'citymunCode' => 64513,
                'citymunDesc' => 'ILOG',
                'provCode' => 645,
            ),
            213 => 
            array (
                'citymunCode' => 64514,
                'citymunDesc' => 'ISABELA',
                'provCode' => 645,
            ),
            214 => 
            array (
                'citymunCode' => 64515,
                'citymunDesc' => 'CITY OF KABANKALAN',
                'provCode' => 645,
            ),
            215 => 
            array (
                'citymunCode' => 64516,
                'citymunDesc' => 'LA CARLOTA CITY',
                'provCode' => 645,
            ),
            216 => 
            array (
                'citymunCode' => 64517,
                'citymunDesc' => 'LA CASTELLANA',
                'provCode' => 645,
            ),
            217 => 
            array (
                'citymunCode' => 64518,
                'citymunDesc' => 'MANAPLA',
                'provCode' => 645,
            ),
            218 => 
            array (
                'citymunCode' => 64519,
            'citymunDesc' => 'MOISES PADILLA (MAGALLON)',
                'provCode' => 645,
            ),
            219 => 
            array (
                'citymunCode' => 64520,
                'citymunDesc' => 'MURCIA',
                'provCode' => 645,
            ),
            220 => 
            array (
                'citymunCode' => 64521,
                'citymunDesc' => 'PONTEVEDRA',
                'provCode' => 645,
            ),
            221 => 
            array (
                'citymunCode' => 64522,
                'citymunDesc' => 'PULUPANDAN',
                'provCode' => 645,
            ),
            222 => 
            array (
                'citymunCode' => 64523,
                'citymunDesc' => 'SAGAY CITY',
                'provCode' => 645,
            ),
            223 => 
            array (
                'citymunCode' => 64524,
                'citymunDesc' => 'SAN CARLOS CITY',
                'provCode' => 645,
            ),
            224 => 
            array (
                'citymunCode' => 64525,
                'citymunDesc' => 'SAN ENRIQUE',
                'provCode' => 645,
            ),
            225 => 
            array (
                'citymunCode' => 64526,
                'citymunDesc' => 'SILAY CITY',
                'provCode' => 645,
            ),
            226 => 
            array (
                'citymunCode' => 64527,
                'citymunDesc' => 'CITY OF SIPALAY',
                'provCode' => 645,
            ),
            227 => 
            array (
                'citymunCode' => 64528,
                'citymunDesc' => 'CITY OF TALISAY',
                'provCode' => 645,
            ),
            228 => 
            array (
                'citymunCode' => 64529,
                'citymunDesc' => 'TOBOSO',
                'provCode' => 645,
            ),
            229 => 
            array (
                'citymunCode' => 64530,
                'citymunDesc' => 'VALLADOLID',
                'provCode' => 645,
            ),
            230 => 
            array (
                'citymunCode' => 64531,
                'citymunDesc' => 'CITY OF VICTORIAS',
                'provCode' => 645,
            ),
            231 => 
            array (
                'citymunCode' => 64532,
                'citymunDesc' => 'SALVADOR BENEDICTO',
                'provCode' => 645,
            ),
            232 => 
            array (
                'citymunCode' => 67901,
                'citymunDesc' => 'BUENAVISTA',
                'provCode' => 679,
            ),
            233 => 
            array (
                'citymunCode' => 67902,
            'citymunDesc' => 'JORDAN (Capital)',
                'provCode' => 679,
            ),
            234 => 
            array (
                'citymunCode' => 67903,
                'citymunDesc' => 'NUEVA VALENCIA',
                'provCode' => 679,
            ),
            235 => 
            array (
                'citymunCode' => 67904,
                'citymunDesc' => 'SAN LORENZO',
                'provCode' => 679,
            ),
            236 => 
            array (
                'citymunCode' => 67905,
                'citymunDesc' => 'SIBUNAG',
                'provCode' => 679,
            ),
            237 => 
            array (
                'citymunCode' => 71201,
                'citymunDesc' => 'ALBURQUERQUE',
                'provCode' => 712,
            ),
            238 => 
            array (
                'citymunCode' => 71202,
                'citymunDesc' => 'ALICIA',
                'provCode' => 712,
            ),
            239 => 
            array (
                'citymunCode' => 71203,
                'citymunDesc' => 'ANDA',
                'provCode' => 712,
            ),
            240 => 
            array (
                'citymunCode' => 71204,
                'citymunDesc' => 'ANTEQUERA',
                'provCode' => 712,
            ),
            241 => 
            array (
                'citymunCode' => 71205,
                'citymunDesc' => 'BACLAYON',
                'provCode' => 712,
            ),
            242 => 
            array (
                'citymunCode' => 71206,
                'citymunDesc' => 'BALILIHAN',
                'provCode' => 712,
            ),
            243 => 
            array (
                'citymunCode' => 71207,
                'citymunDesc' => 'BATUAN',
                'provCode' => 712,
            ),
            244 => 
            array (
                'citymunCode' => 71208,
                'citymunDesc' => 'BILAR',
                'provCode' => 712,
            ),
            245 => 
            array (
                'citymunCode' => 71209,
                'citymunDesc' => 'BUENAVISTA',
                'provCode' => 712,
            ),
            246 => 
            array (
                'citymunCode' => 71210,
                'citymunDesc' => 'CALAPE',
                'provCode' => 712,
            ),
            247 => 
            array (
                'citymunCode' => 71211,
                'citymunDesc' => 'CANDIJAY',
                'provCode' => 712,
            ),
            248 => 
            array (
                'citymunCode' => 71212,
                'citymunDesc' => 'CARMEN',
                'provCode' => 712,
            ),
            249 => 
            array (
                'citymunCode' => 71213,
                'citymunDesc' => 'CATIGBIAN',
                'provCode' => 712,
            ),
            250 => 
            array (
                'citymunCode' => 71214,
                'citymunDesc' => 'CLARIN',
                'provCode' => 712,
            ),
            251 => 
            array (
                'citymunCode' => 71215,
                'citymunDesc' => 'CORELLA',
                'provCode' => 712,
            ),
            252 => 
            array (
                'citymunCode' => 71216,
                'citymunDesc' => 'CORTES',
                'provCode' => 712,
            ),
            253 => 
            array (
                'citymunCode' => 71217,
                'citymunDesc' => 'DAGOHOY',
                'provCode' => 712,
            ),
            254 => 
            array (
                'citymunCode' => 71218,
                'citymunDesc' => 'DANAO',
                'provCode' => 712,
            ),
            255 => 
            array (
                'citymunCode' => 71219,
                'citymunDesc' => 'DAUIS',
                'provCode' => 712,
            ),
            256 => 
            array (
                'citymunCode' => 71220,
                'citymunDesc' => 'DIMIAO',
                'provCode' => 712,
            ),
            257 => 
            array (
                'citymunCode' => 71221,
                'citymunDesc' => 'DUERO',
                'provCode' => 712,
            ),
            258 => 
            array (
                'citymunCode' => 71222,
                'citymunDesc' => 'GARCIA HERNANDEZ',
                'provCode' => 712,
            ),
            259 => 
            array (
                'citymunCode' => 71223,
                'citymunDesc' => 'GUINDULMAN',
                'provCode' => 712,
            ),
            260 => 
            array (
                'citymunCode' => 71224,
                'citymunDesc' => 'INABANGA',
                'provCode' => 712,
            ),
            261 => 
            array (
                'citymunCode' => 71225,
                'citymunDesc' => 'JAGNA',
                'provCode' => 712,
            ),
            262 => 
            array (
                'citymunCode' => 71226,
                'citymunDesc' => 'JETAFE',
                'provCode' => 712,
            ),
            263 => 
            array (
                'citymunCode' => 71227,
                'citymunDesc' => 'LILA',
                'provCode' => 712,
            ),
            264 => 
            array (
                'citymunCode' => 71228,
                'citymunDesc' => 'LOAY',
                'provCode' => 712,
            ),
            265 => 
            array (
                'citymunCode' => 71229,
                'citymunDesc' => 'LOBOC',
                'provCode' => 712,
            ),
            266 => 
            array (
                'citymunCode' => 71230,
                'citymunDesc' => 'LOON',
                'provCode' => 712,
            ),
            267 => 
            array (
                'citymunCode' => 71231,
                'citymunDesc' => 'MABINI',
                'provCode' => 712,
            ),
            268 => 
            array (
                'citymunCode' => 71232,
                'citymunDesc' => 'MARIBOJOC',
                'provCode' => 712,
            ),
            269 => 
            array (
                'citymunCode' => 71233,
                'citymunDesc' => 'PANGLAO',
                'provCode' => 712,
            ),
            270 => 
            array (
                'citymunCode' => 71234,
                'citymunDesc' => 'PILAR',
                'provCode' => 712,
            ),
            271 => 
            array (
                'citymunCode' => 71235,
            'citymunDesc' => 'PRES. CARLOS P. GARCIA (PITOGO)',
                'provCode' => 712,
            ),
            272 => 
            array (
                'citymunCode' => 71236,
            'citymunDesc' => 'SAGBAYAN (BORJA)',
                'provCode' => 712,
            ),
            273 => 
            array (
                'citymunCode' => 71237,
                'citymunDesc' => 'SAN ISIDRO',
                'provCode' => 712,
            ),
            274 => 
            array (
                'citymunCode' => 71238,
                'citymunDesc' => 'SAN MIGUEL',
                'provCode' => 712,
            ),
            275 => 
            array (
                'citymunCode' => 71239,
                'citymunDesc' => 'SEVILLA',
                'provCode' => 712,
            ),
            276 => 
            array (
                'citymunCode' => 71240,
                'citymunDesc' => 'SIERRA BULLONES',
                'provCode' => 712,
            ),
            277 => 
            array (
                'citymunCode' => 71241,
                'citymunDesc' => 'SIKATUNA',
                'provCode' => 712,
            ),
            278 => 
            array (
                'citymunCode' => 71242,
            'citymunDesc' => 'TAGBILARAN CITY (Capital)',
                'provCode' => 712,
            ),
            279 => 
            array (
                'citymunCode' => 71243,
                'citymunDesc' => 'TALIBON',
                'provCode' => 712,
            ),
            280 => 
            array (
                'citymunCode' => 71244,
                'citymunDesc' => 'TRINIDAD',
                'provCode' => 712,
            ),
            281 => 
            array (
                'citymunCode' => 71245,
                'citymunDesc' => 'TUBIGON',
                'provCode' => 712,
            ),
            282 => 
            array (
                'citymunCode' => 71246,
                'citymunDesc' => 'UBAY',
                'provCode' => 712,
            ),
            283 => 
            array (
                'citymunCode' => 71247,
                'citymunDesc' => 'VALENCIA',
                'provCode' => 712,
            ),
            284 => 
            array (
                'citymunCode' => 71248,
                'citymunDesc' => 'BIEN UNIDO',
                'provCode' => 712,
            ),
            285 => 
            array (
                'citymunCode' => 72201,
                'citymunDesc' => 'ALCANTARA',
                'provCode' => 722,
            ),
            286 => 
            array (
                'citymunCode' => 72202,
                'citymunDesc' => 'ALCOY',
                'provCode' => 722,
            ),
            287 => 
            array (
                'citymunCode' => 72203,
                'citymunDesc' => 'ALEGRIA',
                'provCode' => 722,
            ),
            288 => 
            array (
                'citymunCode' => 72204,
                'citymunDesc' => 'ALOGUINSAN',
                'provCode' => 722,
            ),
            289 => 
            array (
                'citymunCode' => 72205,
                'citymunDesc' => 'ARGAO',
                'provCode' => 722,
            ),
            290 => 
            array (
                'citymunCode' => 72206,
                'citymunDesc' => 'ASTURIAS',
                'provCode' => 722,
            ),
            291 => 
            array (
                'citymunCode' => 72207,
                'citymunDesc' => 'BADIAN',
                'provCode' => 722,
            ),
            292 => 
            array (
                'citymunCode' => 72208,
                'citymunDesc' => 'BALAMBAN',
                'provCode' => 722,
            ),
            293 => 
            array (
                'citymunCode' => 72209,
                'citymunDesc' => 'BANTAYAN',
                'provCode' => 722,
            ),
            294 => 
            array (
                'citymunCode' => 72210,
                'citymunDesc' => 'BARILI',
                'provCode' => 722,
            ),
            295 => 
            array (
                'citymunCode' => 72211,
                'citymunDesc' => 'CITY OF BOGO',
                'provCode' => 722,
            ),
            296 => 
            array (
                'citymunCode' => 72212,
                'citymunDesc' => 'BOLJOON',
                'provCode' => 722,
            ),
            297 => 
            array (
                'citymunCode' => 72213,
                'citymunDesc' => 'BORBON',
                'provCode' => 722,
            ),
            298 => 
            array (
                'citymunCode' => 72214,
                'citymunDesc' => 'CITY OF CARCAR',
                'provCode' => 722,
            ),
            299 => 
            array (
                'citymunCode' => 72215,
                'citymunDesc' => 'CARMEN',
                'provCode' => 722,
            ),
            300 => 
            array (
                'citymunCode' => 72216,
                'citymunDesc' => 'CATMON',
                'provCode' => 722,
            ),
            301 => 
            array (
                'citymunCode' => 72217,
            'citymunDesc' => 'CEBU CITY (Capital)',
                'provCode' => 722,
            ),
            302 => 
            array (
                'citymunCode' => 72218,
                'citymunDesc' => 'COMPOSTELA',
                'provCode' => 722,
            ),
            303 => 
            array (
                'citymunCode' => 72219,
                'citymunDesc' => 'CONSOLACION',
                'provCode' => 722,
            ),
            304 => 
            array (
                'citymunCode' => 72220,
                'citymunDesc' => 'CORDOVA',
                'provCode' => 722,
            ),
            305 => 
            array (
                'citymunCode' => 72221,
                'citymunDesc' => 'DAANBANTAYAN',
                'provCode' => 722,
            ),
            306 => 
            array (
                'citymunCode' => 72222,
                'citymunDesc' => 'DALAGUETE',
                'provCode' => 722,
            ),
            307 => 
            array (
                'citymunCode' => 72223,
                'citymunDesc' => 'DANAO CITY',
                'provCode' => 722,
            ),
            308 => 
            array (
                'citymunCode' => 72224,
                'citymunDesc' => 'DUMANJUG',
                'provCode' => 722,
            ),
            309 => 
            array (
                'citymunCode' => 72225,
                'citymunDesc' => 'GINATILAN',
                'provCode' => 722,
            ),
            310 => 
            array (
                'citymunCode' => 72226,
            'citymunDesc' => 'LAPU-LAPU CITY (OPON)',
                'provCode' => 722,
            ),
            311 => 
            array (
                'citymunCode' => 72227,
                'citymunDesc' => 'LILOAN',
                'provCode' => 722,
            ),
            312 => 
            array (
                'citymunCode' => 72228,
                'citymunDesc' => 'MADRIDEJOS',
                'provCode' => 722,
            ),
            313 => 
            array (
                'citymunCode' => 72229,
                'citymunDesc' => 'MALABUYOC',
                'provCode' => 722,
            ),
            314 => 
            array (
                'citymunCode' => 72230,
                'citymunDesc' => 'MANDAUE CITY',
                'provCode' => 722,
            ),
            315 => 
            array (
                'citymunCode' => 72231,
                'citymunDesc' => 'MEDELLIN',
                'provCode' => 722,
            ),
            316 => 
            array (
                'citymunCode' => 72232,
                'citymunDesc' => 'MINGLANILLA',
                'provCode' => 722,
            ),
            317 => 
            array (
                'citymunCode' => 72233,
                'citymunDesc' => 'MOALBOAL',
                'provCode' => 722,
            ),
            318 => 
            array (
                'citymunCode' => 72234,
                'citymunDesc' => 'CITY OF NAGA',
                'provCode' => 722,
            ),
            319 => 
            array (
                'citymunCode' => 72235,
                'citymunDesc' => 'OSLOB',
                'provCode' => 722,
            ),
            320 => 
            array (
                'citymunCode' => 72236,
                'citymunDesc' => 'PILAR',
                'provCode' => 722,
            ),
            321 => 
            array (
                'citymunCode' => 72237,
                'citymunDesc' => 'PINAMUNGAHAN',
                'provCode' => 722,
            ),
            322 => 
            array (
                'citymunCode' => 72238,
                'citymunDesc' => 'PORO',
                'provCode' => 722,
            ),
            323 => 
            array (
                'citymunCode' => 72239,
                'citymunDesc' => 'RONDA',
                'provCode' => 722,
            ),
            324 => 
            array (
                'citymunCode' => 72240,
                'citymunDesc' => 'SAMBOAN',
                'provCode' => 722,
            ),
            325 => 
            array (
                'citymunCode' => 72241,
                'citymunDesc' => 'SAN FERNANDO',
                'provCode' => 722,
            ),
            326 => 
            array (
                'citymunCode' => 72242,
                'citymunDesc' => 'SAN FRANCISCO',
                'provCode' => 722,
            ),
            327 => 
            array (
                'citymunCode' => 72243,
                'citymunDesc' => 'SAN REMIGIO',
                'provCode' => 722,
            ),
            328 => 
            array (
                'citymunCode' => 72244,
                'citymunDesc' => 'SANTA FE',
                'provCode' => 722,
            ),
            329 => 
            array (
                'citymunCode' => 72245,
                'citymunDesc' => 'SANTANDER',
                'provCode' => 722,
            ),
            330 => 
            array (
                'citymunCode' => 72246,
                'citymunDesc' => 'SIBONGA',
                'provCode' => 722,
            ),
            331 => 
            array (
                'citymunCode' => 72247,
                'citymunDesc' => 'SOGOD',
                'provCode' => 722,
            ),
            332 => 
            array (
                'citymunCode' => 72248,
                'citymunDesc' => 'TABOGON',
                'provCode' => 722,
            ),
            333 => 
            array (
                'citymunCode' => 72249,
                'citymunDesc' => 'TABUELAN',
                'provCode' => 722,
            ),
            334 => 
            array (
                'citymunCode' => 72250,
                'citymunDesc' => 'CITY OF TALISAY',
                'provCode' => 722,
            ),
            335 => 
            array (
                'citymunCode' => 72251,
                'citymunDesc' => 'TOLEDO CITY',
                'provCode' => 722,
            ),
            336 => 
            array (
                'citymunCode' => 72252,
                'citymunDesc' => 'TUBURAN',
                'provCode' => 722,
            ),
            337 => 
            array (
                'citymunCode' => 72253,
                'citymunDesc' => 'TUDELA',
                'provCode' => 722,
            ),
            338 => 
            array (
                'citymunCode' => 74601,
            'citymunDesc' => 'AMLAN (AYUQUITAN)',
                'provCode' => 746,
            ),
            339 => 
            array (
                'citymunCode' => 74602,
                'citymunDesc' => 'AYUNGON',
                'provCode' => 746,
            ),
            340 => 
            array (
                'citymunCode' => 74603,
                'citymunDesc' => 'BACONG',
                'provCode' => 746,
            ),
            341 => 
            array (
                'citymunCode' => 74604,
                'citymunDesc' => 'BAIS CITY',
                'provCode' => 746,
            ),
            342 => 
            array (
                'citymunCode' => 74605,
                'citymunDesc' => 'BASAY',
                'provCode' => 746,
            ),
            343 => 
            array (
                'citymunCode' => 74606,
            'citymunDesc' => 'CITY OF BAYAWAN (TULONG)',
                'provCode' => 746,
            ),
            344 => 
            array (
                'citymunCode' => 74607,
            'citymunDesc' => 'BINDOY (PAYABON)',
                'provCode' => 746,
            ),
            345 => 
            array (
                'citymunCode' => 74608,
                'citymunDesc' => 'CANLAON CITY',
                'provCode' => 746,
            ),
            346 => 
            array (
                'citymunCode' => 74609,
                'citymunDesc' => 'DAUIN',
                'provCode' => 746,
            ),
            347 => 
            array (
                'citymunCode' => 74610,
            'citymunDesc' => 'DUMAGUETE CITY (Capital)',
                'provCode' => 746,
            ),
            348 => 
            array (
                'citymunCode' => 74611,
                'citymunDesc' => 'CITY OF GUIHULNGAN',
                'provCode' => 746,
            ),
            349 => 
            array (
                'citymunCode' => 74612,
                'citymunDesc' => 'JIMALALUD',
                'provCode' => 746,
            ),
            350 => 
            array (
                'citymunCode' => 74613,
                'citymunDesc' => 'LA LIBERTAD',
                'provCode' => 746,
            ),
            351 => 
            array (
                'citymunCode' => 74614,
                'citymunDesc' => 'MABINAY',
                'provCode' => 746,
            ),
            352 => 
            array (
                'citymunCode' => 74615,
                'citymunDesc' => 'MANJUYOD',
                'provCode' => 746,
            ),
            353 => 
            array (
                'citymunCode' => 74616,
                'citymunDesc' => 'PAMPLONA',
                'provCode' => 746,
            ),
            354 => 
            array (
                'citymunCode' => 74617,
                'citymunDesc' => 'SAN JOSE',
                'provCode' => 746,
            ),
            355 => 
            array (
                'citymunCode' => 74618,
                'citymunDesc' => 'SANTA CATALINA',
                'provCode' => 746,
            ),
            356 => 
            array (
                'citymunCode' => 74619,
                'citymunDesc' => 'SIATON',
                'provCode' => 746,
            ),
            357 => 
            array (
                'citymunCode' => 74620,
                'citymunDesc' => 'SIBULAN',
                'provCode' => 746,
            ),
            358 => 
            array (
                'citymunCode' => 74621,
                'citymunDesc' => 'CITY OF TANJAY',
                'provCode' => 746,
            ),
            359 => 
            array (
                'citymunCode' => 74622,
                'citymunDesc' => 'TAYASAN',
                'provCode' => 746,
            ),
            360 => 
            array (
                'citymunCode' => 74623,
            'citymunDesc' => 'VALENCIA (LUZURRIAGA)',
                'provCode' => 746,
            ),
            361 => 
            array (
                'citymunCode' => 74624,
                'citymunDesc' => 'VALLEHERMOSO',
                'provCode' => 746,
            ),
            362 => 
            array (
                'citymunCode' => 74625,
                'citymunDesc' => 'ZAMBOANGUITA',
                'provCode' => 746,
            ),
            363 => 
            array (
                'citymunCode' => 76101,
                'citymunDesc' => 'ENRIQUE VILLANUEVA',
                'provCode' => 761,
            ),
            364 => 
            array (
                'citymunCode' => 76102,
                'citymunDesc' => 'LARENA',
                'provCode' => 761,
            ),
            365 => 
            array (
                'citymunCode' => 76103,
                'citymunDesc' => 'LAZI',
                'provCode' => 761,
            ),
            366 => 
            array (
                'citymunCode' => 76104,
                'citymunDesc' => 'MARIA',
                'provCode' => 761,
            ),
            367 => 
            array (
                'citymunCode' => 76105,
                'citymunDesc' => 'SAN JUAN',
                'provCode' => 761,
            ),
            368 => 
            array (
                'citymunCode' => 76106,
            'citymunDesc' => 'SIQUIJOR (Capital)',
                'provCode' => 761,
            ),
            369 => 
            array (
                'citymunCode' => 82601,
                'citymunDesc' => 'ARTECHE',
                'provCode' => 826,
            ),
            370 => 
            array (
                'citymunCode' => 82602,
                'citymunDesc' => 'BALANGIGA',
                'provCode' => 826,
            ),
            371 => 
            array (
                'citymunCode' => 82603,
                'citymunDesc' => 'BALANGKAYAN',
                'provCode' => 826,
            ),
            372 => 
            array (
                'citymunCode' => 82604,
            'citymunDesc' => 'CITY OF BORONGAN (Capital)',
                'provCode' => 826,
            ),
            373 => 
            array (
                'citymunCode' => 82605,
                'citymunDesc' => 'CAN-AVID',
                'provCode' => 826,
            ),
            374 => 
            array (
                'citymunCode' => 82606,
                'citymunDesc' => 'DOLORES',
                'provCode' => 826,
            ),
            375 => 
            array (
                'citymunCode' => 82607,
                'citymunDesc' => 'GENERAL MACARTHUR',
                'provCode' => 826,
            ),
            376 => 
            array (
                'citymunCode' => 82608,
                'citymunDesc' => 'GIPORLOS',
                'provCode' => 826,
            ),
            377 => 
            array (
                'citymunCode' => 82609,
                'citymunDesc' => 'GUIUAN',
                'provCode' => 826,
            ),
            378 => 
            array (
                'citymunCode' => 82610,
                'citymunDesc' => 'HERNANI',
                'provCode' => 826,
            ),
            379 => 
            array (
                'citymunCode' => 82611,
                'citymunDesc' => 'JIPAPAD',
                'provCode' => 826,
            ),
            380 => 
            array (
                'citymunCode' => 82612,
                'citymunDesc' => 'LAWAAN',
                'provCode' => 826,
            ),
            381 => 
            array (
                'citymunCode' => 82613,
                'citymunDesc' => 'LLORENTE',
                'provCode' => 826,
            ),
            382 => 
            array (
                'citymunCode' => 82614,
                'citymunDesc' => 'MASLOG',
                'provCode' => 826,
            ),
            383 => 
            array (
                'citymunCode' => 82615,
                'citymunDesc' => 'MAYDOLONG',
                'provCode' => 826,
            ),
            384 => 
            array (
                'citymunCode' => 82616,
                'citymunDesc' => 'MERCEDES',
                'provCode' => 826,
            ),
            385 => 
            array (
                'citymunCode' => 82617,
                'citymunDesc' => 'ORAS',
                'provCode' => 826,
            ),
            386 => 
            array (
                'citymunCode' => 82618,
                'citymunDesc' => 'QUINAPONDAN',
                'provCode' => 826,
            ),
            387 => 
            array (
                'citymunCode' => 82619,
                'citymunDesc' => 'SALCEDO',
                'provCode' => 826,
            ),
            388 => 
            array (
                'citymunCode' => 82620,
                'citymunDesc' => 'SAN JULIAN',
                'provCode' => 826,
            ),
            389 => 
            array (
                'citymunCode' => 82621,
                'citymunDesc' => 'SAN POLICARPO',
                'provCode' => 826,
            ),
            390 => 
            array (
                'citymunCode' => 82622,
                'citymunDesc' => 'SULAT',
                'provCode' => 826,
            ),
            391 => 
            array (
                'citymunCode' => 82623,
                'citymunDesc' => 'TAFT',
                'provCode' => 826,
            ),
            392 => 
            array (
                'citymunCode' => 83701,
                'citymunDesc' => 'ABUYOG',
                'provCode' => 837,
            ),
            393 => 
            array (
                'citymunCode' => 83702,
                'citymunDesc' => 'ALANGALANG',
                'provCode' => 837,
            ),
            394 => 
            array (
                'citymunCode' => 83703,
                'citymunDesc' => 'ALBUERA',
                'provCode' => 837,
            ),
            395 => 
            array (
                'citymunCode' => 83705,
                'citymunDesc' => 'BABATNGON',
                'provCode' => 837,
            ),
            396 => 
            array (
                'citymunCode' => 83706,
                'citymunDesc' => 'BARUGO',
                'provCode' => 837,
            ),
            397 => 
            array (
                'citymunCode' => 83707,
                'citymunDesc' => 'BATO',
                'provCode' => 837,
            ),
            398 => 
            array (
                'citymunCode' => 83708,
                'citymunDesc' => 'CITY OF BAYBAY',
                'provCode' => 837,
            ),
            399 => 
            array (
                'citymunCode' => 83710,
                'citymunDesc' => 'BURAUEN',
                'provCode' => 837,
            ),
            400 => 
            array (
                'citymunCode' => 83713,
                'citymunDesc' => 'CALUBIAN',
                'provCode' => 837,
            ),
            401 => 
            array (
                'citymunCode' => 83714,
                'citymunDesc' => 'CAPOOCAN',
                'provCode' => 837,
            ),
            402 => 
            array (
                'citymunCode' => 83715,
                'citymunDesc' => 'CARIGARA',
                'provCode' => 837,
            ),
            403 => 
            array (
                'citymunCode' => 83717,
                'citymunDesc' => 'DAGAMI',
                'provCode' => 837,
            ),
            404 => 
            array (
                'citymunCode' => 83718,
                'citymunDesc' => 'DULAG',
                'provCode' => 837,
            ),
            405 => 
            array (
                'citymunCode' => 83719,
                'citymunDesc' => 'HILONGOS',
                'provCode' => 837,
            ),
            406 => 
            array (
                'citymunCode' => 83720,
                'citymunDesc' => 'HINDANG',
                'provCode' => 837,
            ),
            407 => 
            array (
                'citymunCode' => 83721,
                'citymunDesc' => 'INOPACAN',
                'provCode' => 837,
            ),
            408 => 
            array (
                'citymunCode' => 83722,
                'citymunDesc' => 'ISABEL',
                'provCode' => 837,
            ),
            409 => 
            array (
                'citymunCode' => 83723,
                'citymunDesc' => 'JARO',
                'provCode' => 837,
            ),
            410 => 
            array (
                'citymunCode' => 83724,
            'citymunDesc' => 'JAVIER (BUGHO)',
                'provCode' => 837,
            ),
            411 => 
            array (
                'citymunCode' => 83725,
                'citymunDesc' => 'JULITA',
                'provCode' => 837,
            ),
            412 => 
            array (
                'citymunCode' => 83726,
                'citymunDesc' => 'KANANGA',
                'provCode' => 837,
            ),
            413 => 
            array (
                'citymunCode' => 83728,
                'citymunDesc' => 'LA PAZ',
                'provCode' => 837,
            ),
            414 => 
            array (
                'citymunCode' => 83729,
                'citymunDesc' => 'LEYTE',
                'provCode' => 837,
            ),
            415 => 
            array (
                'citymunCode' => 83730,
                'citymunDesc' => 'MACARTHUR',
                'provCode' => 837,
            ),
            416 => 
            array (
                'citymunCode' => 83731,
                'citymunDesc' => 'MAHAPLAG',
                'provCode' => 837,
            ),
            417 => 
            array (
                'citymunCode' => 83733,
                'citymunDesc' => 'MATAG-OB',
                'provCode' => 837,
            ),
            418 => 
            array (
                'citymunCode' => 83734,
                'citymunDesc' => 'MATALOM',
                'provCode' => 837,
            ),
            419 => 
            array (
                'citymunCode' => 83735,
                'citymunDesc' => 'MAYORGA',
                'provCode' => 837,
            ),
            420 => 
            array (
                'citymunCode' => 83736,
                'citymunDesc' => 'MERIDA',
                'provCode' => 837,
            ),
            421 => 
            array (
                'citymunCode' => 83738,
                'citymunDesc' => 'ORMOC CITY',
                'provCode' => 837,
            ),
            422 => 
            array (
                'citymunCode' => 83739,
                'citymunDesc' => 'PALO',
                'provCode' => 837,
            ),
            423 => 
            array (
                'citymunCode' => 83740,
                'citymunDesc' => 'PALOMPON',
                'provCode' => 837,
            ),
            424 => 
            array (
                'citymunCode' => 83741,
                'citymunDesc' => 'PASTRANA',
                'provCode' => 837,
            ),
            425 => 
            array (
                'citymunCode' => 83742,
                'citymunDesc' => 'SAN ISIDRO',
                'provCode' => 837,
            ),
            426 => 
            array (
                'citymunCode' => 83743,
                'citymunDesc' => 'SAN MIGUEL',
                'provCode' => 837,
            ),
            427 => 
            array (
                'citymunCode' => 83744,
                'citymunDesc' => 'SANTA FE',
                'provCode' => 837,
            ),
            428 => 
            array (
                'citymunCode' => 83745,
                'citymunDesc' => 'TABANGO',
                'provCode' => 837,
            ),
            429 => 
            array (
                'citymunCode' => 83746,
                'citymunDesc' => 'TABONTABON',
                'provCode' => 837,
            ),
            430 => 
            array (
                'citymunCode' => 83747,
            'citymunDesc' => 'TACLOBAN CITY (Capital)',
                'provCode' => 837,
            ),
            431 => 
            array (
                'citymunCode' => 83748,
                'citymunDesc' => 'TANAUAN',
                'provCode' => 837,
            ),
            432 => 
            array (
                'citymunCode' => 83749,
                'citymunDesc' => 'TOLOSA',
                'provCode' => 837,
            ),
            433 => 
            array (
                'citymunCode' => 83750,
                'citymunDesc' => 'TUNGA',
                'provCode' => 837,
            ),
            434 => 
            array (
                'citymunCode' => 83751,
                'citymunDesc' => 'VILLABA',
                'provCode' => 837,
            ),
            435 => 
            array (
                'citymunCode' => 84801,
                'citymunDesc' => 'ALLEN',
                'provCode' => 848,
            ),
            436 => 
            array (
                'citymunCode' => 84802,
                'citymunDesc' => 'BIRI',
                'provCode' => 848,
            ),
            437 => 
            array (
                'citymunCode' => 84803,
                'citymunDesc' => 'BOBON',
                'provCode' => 848,
            ),
            438 => 
            array (
                'citymunCode' => 84804,
                'citymunDesc' => 'CAPUL',
                'provCode' => 848,
            ),
            439 => 
            array (
                'citymunCode' => 84805,
            'citymunDesc' => 'CATARMAN (Capital)',
                'provCode' => 848,
            ),
            440 => 
            array (
                'citymunCode' => 84806,
                'citymunDesc' => 'CATUBIG',
                'provCode' => 848,
            ),
            441 => 
            array (
                'citymunCode' => 84807,
                'citymunDesc' => 'GAMAY',
                'provCode' => 848,
            ),
            442 => 
            array (
                'citymunCode' => 84808,
                'citymunDesc' => 'LAOANG',
                'provCode' => 848,
            ),
            443 => 
            array (
                'citymunCode' => 84809,
                'citymunDesc' => 'LAPINIG',
                'provCode' => 848,
            ),
            444 => 
            array (
                'citymunCode' => 84810,
                'citymunDesc' => 'LAS NAVAS',
                'provCode' => 848,
            ),
            445 => 
            array (
                'citymunCode' => 84811,
                'citymunDesc' => 'LAVEZARES',
                'provCode' => 848,
            ),
            446 => 
            array (
                'citymunCode' => 84812,
                'citymunDesc' => 'MAPANAS',
                'provCode' => 848,
            ),
            447 => 
            array (
                'citymunCode' => 84813,
                'citymunDesc' => 'MONDRAGON',
                'provCode' => 848,
            ),
            448 => 
            array (
                'citymunCode' => 84814,
                'citymunDesc' => 'PALAPAG',
                'provCode' => 848,
            ),
            449 => 
            array (
                'citymunCode' => 84815,
                'citymunDesc' => 'PAMBUJAN',
                'provCode' => 848,
            ),
            450 => 
            array (
                'citymunCode' => 84816,
                'citymunDesc' => 'ROSARIO',
                'provCode' => 848,
            ),
            451 => 
            array (
                'citymunCode' => 84817,
                'citymunDesc' => 'SAN ANTONIO',
                'provCode' => 848,
            ),
            452 => 
            array (
                'citymunCode' => 84818,
                'citymunDesc' => 'SAN ISIDRO',
                'provCode' => 848,
            ),
            453 => 
            array (
                'citymunCode' => 84819,
                'citymunDesc' => 'SAN JOSE',
                'provCode' => 848,
            ),
            454 => 
            array (
                'citymunCode' => 84820,
                'citymunDesc' => 'SAN ROQUE',
                'provCode' => 848,
            ),
            455 => 
            array (
                'citymunCode' => 84821,
                'citymunDesc' => 'SAN VICENTE',
                'provCode' => 848,
            ),
            456 => 
            array (
                'citymunCode' => 84822,
                'citymunDesc' => 'SILVINO LOBOS',
                'provCode' => 848,
            ),
            457 => 
            array (
                'citymunCode' => 84823,
                'citymunDesc' => 'VICTORIA',
                'provCode' => 848,
            ),
            458 => 
            array (
                'citymunCode' => 84824,
                'citymunDesc' => 'LOPE DE VEGA',
                'provCode' => 848,
            ),
            459 => 
            array (
                'citymunCode' => 86001,
                'citymunDesc' => 'ALMAGRO',
                'provCode' => 860,
            ),
            460 => 
            array (
                'citymunCode' => 86002,
                'citymunDesc' => 'BASEY',
                'provCode' => 860,
            ),
            461 => 
            array (
                'citymunCode' => 86003,
                'citymunDesc' => 'CALBAYOG CITY',
                'provCode' => 860,
            ),
            462 => 
            array (
                'citymunCode' => 86004,
                'citymunDesc' => 'CALBIGA',
                'provCode' => 860,
            ),
            463 => 
            array (
                'citymunCode' => 86005,
            'citymunDesc' => 'CITY OF CATBALOGAN (Capital)',
                'provCode' => 860,
            ),
            464 => 
            array (
                'citymunCode' => 86006,
                'citymunDesc' => 'DARAM',
                'provCode' => 860,
            ),
            465 => 
            array (
                'citymunCode' => 86007,
                'citymunDesc' => 'GANDARA',
                'provCode' => 860,
            ),
            466 => 
            array (
                'citymunCode' => 86008,
                'citymunDesc' => 'HINABANGAN',
                'provCode' => 860,
            ),
            467 => 
            array (
                'citymunCode' => 86009,
                'citymunDesc' => 'JIABONG',
                'provCode' => 860,
            ),
            468 => 
            array (
                'citymunCode' => 86010,
                'citymunDesc' => 'MARABUT',
                'provCode' => 860,
            ),
            469 => 
            array (
                'citymunCode' => 86011,
                'citymunDesc' => 'MATUGUINAO',
                'provCode' => 860,
            ),
            470 => 
            array (
                'citymunCode' => 86012,
                'citymunDesc' => 'MOTIONG',
                'provCode' => 860,
            ),
            471 => 
            array (
                'citymunCode' => 86013,
                'citymunDesc' => 'PINABACDAO',
                'provCode' => 860,
            ),
            472 => 
            array (
                'citymunCode' => 86014,
                'citymunDesc' => 'SAN JOSE DE BUAN',
                'provCode' => 860,
            ),
            473 => 
            array (
                'citymunCode' => 86015,
                'citymunDesc' => 'SAN SEBASTIAN',
                'provCode' => 860,
            ),
            474 => 
            array (
                'citymunCode' => 86016,
                'citymunDesc' => 'SANTA MARGARITA',
                'provCode' => 860,
            ),
            475 => 
            array (
                'citymunCode' => 86017,
                'citymunDesc' => 'SANTA RITA',
                'provCode' => 860,
            ),
            476 => 
            array (
                'citymunCode' => 86018,
                'citymunDesc' => 'SANTO NIÑO',
                'provCode' => 860,
            ),
            477 => 
            array (
                'citymunCode' => 86019,
                'citymunDesc' => 'TALALORA',
                'provCode' => 860,
            ),
            478 => 
            array (
                'citymunCode' => 86020,
                'citymunDesc' => 'TARANGNAN',
                'provCode' => 860,
            ),
            479 => 
            array (
                'citymunCode' => 86021,
                'citymunDesc' => 'VILLAREAL',
                'provCode' => 860,
            ),
            480 => 
            array (
                'citymunCode' => 86022,
            'citymunDesc' => 'PARANAS (WRIGHT)',
                'provCode' => 860,
            ),
            481 => 
            array (
                'citymunCode' => 86023,
                'citymunDesc' => 'ZUMARRAGA',
                'provCode' => 860,
            ),
            482 => 
            array (
                'citymunCode' => 86024,
                'citymunDesc' => 'TAGAPUL-AN',
                'provCode' => 860,
            ),
            483 => 
            array (
                'citymunCode' => 86025,
                'citymunDesc' => 'SAN JORGE',
                'provCode' => 860,
            ),
            484 => 
            array (
                'citymunCode' => 86026,
                'citymunDesc' => 'PAGSANGHAN',
                'provCode' => 860,
            ),
            485 => 
            array (
                'citymunCode' => 86401,
                'citymunDesc' => 'ANAHAWAN',
                'provCode' => 864,
            ),
            486 => 
            array (
                'citymunCode' => 86402,
                'citymunDesc' => 'BONTOC',
                'provCode' => 864,
            ),
            487 => 
            array (
                'citymunCode' => 86403,
                'citymunDesc' => 'HINUNANGAN',
                'provCode' => 864,
            ),
            488 => 
            array (
                'citymunCode' => 86404,
                'citymunDesc' => 'HINUNDAYAN',
                'provCode' => 864,
            ),
            489 => 
            array (
                'citymunCode' => 86405,
                'citymunDesc' => 'LIBAGON',
                'provCode' => 864,
            ),
            490 => 
            array (
                'citymunCode' => 86406,
                'citymunDesc' => 'LILOAN',
                'provCode' => 864,
            ),
            491 => 
            array (
                'citymunCode' => 86407,
            'citymunDesc' => 'CITY OF MAASIN (Capital)',
                'provCode' => 864,
            ),
            492 => 
            array (
                'citymunCode' => 86408,
                'citymunDesc' => 'MACROHON',
                'provCode' => 864,
            ),
            493 => 
            array (
                'citymunCode' => 86409,
                'citymunDesc' => 'MALITBOG',
                'provCode' => 864,
            ),
            494 => 
            array (
                'citymunCode' => 86410,
                'citymunDesc' => 'PADRE BURGOS',
                'provCode' => 864,
            ),
            495 => 
            array (
                'citymunCode' => 86411,
                'citymunDesc' => 'PINTUYAN',
                'provCode' => 864,
            ),
            496 => 
            array (
                'citymunCode' => 86412,
                'citymunDesc' => 'SAINT BERNARD',
                'provCode' => 864,
            ),
            497 => 
            array (
                'citymunCode' => 86413,
                'citymunDesc' => 'SAN FRANCISCO',
                'provCode' => 864,
            ),
            498 => 
            array (
                'citymunCode' => 86414,
            'citymunDesc' => 'SAN JUAN (CABALIAN)',
                'provCode' => 864,
            ),
            499 => 
            array (
                'citymunCode' => 86415,
                'citymunDesc' => 'SAN RICARDO',
                'provCode' => 864,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'citymunCode' => 86416,
                'citymunDesc' => 'SILAGO',
                'provCode' => 864,
            ),
            1 => 
            array (
                'citymunCode' => 86417,
                'citymunDesc' => 'SOGOD',
                'provCode' => 864,
            ),
            2 => 
            array (
                'citymunCode' => 86418,
                'citymunDesc' => 'TOMAS OPPUS',
                'provCode' => 864,
            ),
            3 => 
            array (
                'citymunCode' => 86419,
                'citymunDesc' => 'LIMASAWA',
                'provCode' => 864,
            ),
            4 => 
            array (
                'citymunCode' => 87801,
                'citymunDesc' => 'ALMERIA',
                'provCode' => 878,
            ),
            5 => 
            array (
                'citymunCode' => 87802,
                'citymunDesc' => 'BILIRAN',
                'provCode' => 878,
            ),
            6 => 
            array (
                'citymunCode' => 87803,
                'citymunDesc' => 'CABUCGAYAN',
                'provCode' => 878,
            ),
            7 => 
            array (
                'citymunCode' => 87804,
                'citymunDesc' => 'CAIBIRAN',
                'provCode' => 878,
            ),
            8 => 
            array (
                'citymunCode' => 87805,
                'citymunDesc' => 'CULABA',
                'provCode' => 878,
            ),
            9 => 
            array (
                'citymunCode' => 87806,
                'citymunDesc' => 'KAWAYAN',
                'provCode' => 878,
            ),
            10 => 
            array (
                'citymunCode' => 87807,
                'citymunDesc' => 'MARIPIPI',
                'provCode' => 878,
            ),
            11 => 
            array (
                'citymunCode' => 87808,
            'citymunDesc' => 'NAVAL (Capital)',
                'provCode' => 878,
            ),
            12 => 
            array (
                'citymunCode' => 97201,
                'citymunDesc' => 'DAPITAN CITY',
                'provCode' => 972,
            ),
            13 => 
            array (
                'citymunCode' => 97202,
            'citymunDesc' => 'DIPOLOG CITY (Capital)',
                'provCode' => 972,
            ),
            14 => 
            array (
                'citymunCode' => 97203,
                'citymunDesc' => 'KATIPUNAN',
                'provCode' => 972,
            ),
            15 => 
            array (
                'citymunCode' => 97204,
                'citymunDesc' => 'LA LIBERTAD',
                'provCode' => 972,
            ),
            16 => 
            array (
                'citymunCode' => 97205,
                'citymunDesc' => 'LABASON',
                'provCode' => 972,
            ),
            17 => 
            array (
                'citymunCode' => 97206,
                'citymunDesc' => 'LILOY',
                'provCode' => 972,
            ),
            18 => 
            array (
                'citymunCode' => 97207,
                'citymunDesc' => 'MANUKAN',
                'provCode' => 972,
            ),
            19 => 
            array (
                'citymunCode' => 97208,
                'citymunDesc' => 'MUTIA',
                'provCode' => 972,
            ),
            20 => 
            array (
                'citymunCode' => 97209,
            'citymunDesc' => 'PIÑAN (NEW PIÑAN)',
                'provCode' => 972,
            ),
            21 => 
            array (
                'citymunCode' => 97210,
                'citymunDesc' => 'POLANCO',
                'provCode' => 972,
            ),
            22 => 
            array (
                'citymunCode' => 97211,
                'citymunDesc' => 'PRES. MANUEL A. ROXAS',
                'provCode' => 972,
            ),
            23 => 
            array (
                'citymunCode' => 97212,
                'citymunDesc' => 'RIZAL',
                'provCode' => 972,
            ),
            24 => 
            array (
                'citymunCode' => 97213,
                'citymunDesc' => 'SALUG',
                'provCode' => 972,
            ),
            25 => 
            array (
                'citymunCode' => 97214,
                'citymunDesc' => 'SERGIO OSMEÑA SR.',
                'provCode' => 972,
            ),
            26 => 
            array (
                'citymunCode' => 97215,
                'citymunDesc' => 'SIAYAN',
                'provCode' => 972,
            ),
            27 => 
            array (
                'citymunCode' => 97216,
                'citymunDesc' => 'SIBUCO',
                'provCode' => 972,
            ),
            28 => 
            array (
                'citymunCode' => 97217,
                'citymunDesc' => 'SIBUTAD',
                'provCode' => 972,
            ),
            29 => 
            array (
                'citymunCode' => 97218,
                'citymunDesc' => 'SINDANGAN',
                'provCode' => 972,
            ),
            30 => 
            array (
                'citymunCode' => 97219,
                'citymunDesc' => 'SIOCON',
                'provCode' => 972,
            ),
            31 => 
            array (
                'citymunCode' => 97220,
                'citymunDesc' => 'SIRAWAI',
                'provCode' => 972,
            ),
            32 => 
            array (
                'citymunCode' => 97221,
                'citymunDesc' => 'TAMPILISAN',
                'provCode' => 972,
            ),
            33 => 
            array (
                'citymunCode' => 97222,
            'citymunDesc' => 'JOSE DALMAN (PONOT)',
                'provCode' => 972,
            ),
            34 => 
            array (
                'citymunCode' => 97223,
                'citymunDesc' => 'GUTALAC',
                'provCode' => 972,
            ),
            35 => 
            array (
                'citymunCode' => 97224,
                'citymunDesc' => 'BALIGUIAN',
                'provCode' => 972,
            ),
            36 => 
            array (
                'citymunCode' => 97225,
                'citymunDesc' => 'GODOD',
                'provCode' => 972,
            ),
            37 => 
            array (
                'citymunCode' => 97226,
            'citymunDesc' => 'BACUNGAN (Leon T. Postigo)',
                'provCode' => 972,
            ),
            38 => 
            array (
                'citymunCode' => 97227,
                'citymunDesc' => 'KALAWIT',
                'provCode' => 972,
            ),
            39 => 
            array (
                'citymunCode' => 97302,
                'citymunDesc' => 'AURORA',
                'provCode' => 973,
            ),
            40 => 
            array (
                'citymunCode' => 97303,
                'citymunDesc' => 'BAYOG',
                'provCode' => 973,
            ),
            41 => 
            array (
                'citymunCode' => 97305,
                'citymunDesc' => 'DIMATALING',
                'provCode' => 973,
            ),
            42 => 
            array (
                'citymunCode' => 97306,
                'citymunDesc' => 'DINAS',
                'provCode' => 973,
            ),
            43 => 
            array (
                'citymunCode' => 97307,
                'citymunDesc' => 'DUMALINAO',
                'provCode' => 973,
            ),
            44 => 
            array (
                'citymunCode' => 97308,
                'citymunDesc' => 'DUMINGAG',
                'provCode' => 973,
            ),
            45 => 
            array (
                'citymunCode' => 97311,
                'citymunDesc' => 'KUMALARANG',
                'provCode' => 973,
            ),
            46 => 
            array (
                'citymunCode' => 97312,
                'citymunDesc' => 'LABANGAN',
                'provCode' => 973,
            ),
            47 => 
            array (
                'citymunCode' => 97313,
                'citymunDesc' => 'LAPUYAN',
                'provCode' => 973,
            ),
            48 => 
            array (
                'citymunCode' => 97315,
                'citymunDesc' => 'MAHAYAG',
                'provCode' => 973,
            ),
            49 => 
            array (
                'citymunCode' => 97317,
                'citymunDesc' => 'MARGOSATUBIG',
                'provCode' => 973,
            ),
            50 => 
            array (
                'citymunCode' => 97318,
                'citymunDesc' => 'MIDSALIP',
                'provCode' => 973,
            ),
            51 => 
            array (
                'citymunCode' => 97319,
                'citymunDesc' => 'MOLAVE',
                'provCode' => 973,
            ),
            52 => 
            array (
                'citymunCode' => 97322,
            'citymunDesc' => 'PAGADIAN CITY (Capital)',
                'provCode' => 973,
            ),
            53 => 
            array (
                'citymunCode' => 97323,
            'citymunDesc' => 'RAMON MAGSAYSAY (LIARGO)',
                'provCode' => 973,
            ),
            54 => 
            array (
                'citymunCode' => 97324,
                'citymunDesc' => 'SAN MIGUEL',
                'provCode' => 973,
            ),
            55 => 
            array (
                'citymunCode' => 97325,
                'citymunDesc' => 'SAN PABLO',
                'provCode' => 973,
            ),
            56 => 
            array (
                'citymunCode' => 97327,
                'citymunDesc' => 'TABINA',
                'provCode' => 973,
            ),
            57 => 
            array (
                'citymunCode' => 97328,
                'citymunDesc' => 'TAMBULIG',
                'provCode' => 973,
            ),
            58 => 
            array (
                'citymunCode' => 97330,
                'citymunDesc' => 'TUKURAN',
                'provCode' => 973,
            ),
            59 => 
            array (
                'citymunCode' => 97332,
                'citymunDesc' => 'ZAMBOANGA CITY',
                'provCode' => 973,
            ),
            60 => 
            array (
                'citymunCode' => 97333,
                'citymunDesc' => 'LAKEWOOD',
                'provCode' => 973,
            ),
            61 => 
            array (
                'citymunCode' => 97337,
                'citymunDesc' => 'JOSEFINA',
                'provCode' => 973,
            ),
            62 => 
            array (
                'citymunCode' => 97338,
                'citymunDesc' => 'PITOGO',
                'provCode' => 973,
            ),
            63 => 
            array (
                'citymunCode' => 97340,
            'citymunDesc' => 'SOMINOT (DON MARIANO MARCOS)',
                'provCode' => 973,
            ),
            64 => 
            array (
                'citymunCode' => 97341,
                'citymunDesc' => 'VINCENZO A. SAGUN',
                'provCode' => 973,
            ),
            65 => 
            array (
                'citymunCode' => 97343,
                'citymunDesc' => 'GUIPOS',
                'provCode' => 973,
            ),
            66 => 
            array (
                'citymunCode' => 97344,
                'citymunDesc' => 'TIGBAO',
                'provCode' => 973,
            ),
            67 => 
            array (
                'citymunCode' => 98301,
                'citymunDesc' => 'ALICIA',
                'provCode' => 983,
            ),
            68 => 
            array (
                'citymunCode' => 98302,
                'citymunDesc' => 'BUUG',
                'provCode' => 983,
            ),
            69 => 
            array (
                'citymunCode' => 98303,
                'citymunDesc' => 'DIPLAHAN',
                'provCode' => 983,
            ),
            70 => 
            array (
                'citymunCode' => 98304,
                'citymunDesc' => 'IMELDA',
                'provCode' => 983,
            ),
            71 => 
            array (
                'citymunCode' => 98305,
            'citymunDesc' => 'IPIL (Capital)',
                'provCode' => 983,
            ),
            72 => 
            array (
                'citymunCode' => 98306,
                'citymunDesc' => 'KABASALAN',
                'provCode' => 983,
            ),
            73 => 
            array (
                'citymunCode' => 98307,
                'citymunDesc' => 'MABUHAY',
                'provCode' => 983,
            ),
            74 => 
            array (
                'citymunCode' => 98308,
                'citymunDesc' => 'MALANGAS',
                'provCode' => 983,
            ),
            75 => 
            array (
                'citymunCode' => 98309,
                'citymunDesc' => 'NAGA',
                'provCode' => 983,
            ),
            76 => 
            array (
                'citymunCode' => 98310,
                'citymunDesc' => 'OLUTANGA',
                'provCode' => 983,
            ),
            77 => 
            array (
                'citymunCode' => 98311,
                'citymunDesc' => 'PAYAO',
                'provCode' => 983,
            ),
            78 => 
            array (
                'citymunCode' => 98312,
                'citymunDesc' => 'ROSELLER LIM',
                'provCode' => 983,
            ),
            79 => 
            array (
                'citymunCode' => 98313,
                'citymunDesc' => 'SIAY',
                'provCode' => 983,
            ),
            80 => 
            array (
                'citymunCode' => 98314,
                'citymunDesc' => 'TALUSAN',
                'provCode' => 983,
            ),
            81 => 
            array (
                'citymunCode' => 98315,
                'citymunDesc' => 'TITAY',
                'provCode' => 983,
            ),
            82 => 
            array (
                'citymunCode' => 98316,
                'citymunDesc' => 'TUNGAWAN',
                'provCode' => 983,
            ),
            83 => 
            array (
                'citymunCode' => 99701,
                'citymunDesc' => 'CITY OF ISABELA',
                'provCode' => 997,
            ),
            84 => 
            array (
                'citymunCode' => 101301,
                'citymunDesc' => 'BAUNGON',
                'provCode' => 1013,
            ),
            85 => 
            array (
                'citymunCode' => 101302,
                'citymunDesc' => 'DAMULOG',
                'provCode' => 1013,
            ),
            86 => 
            array (
                'citymunCode' => 101303,
                'citymunDesc' => 'DANGCAGAN',
                'provCode' => 1013,
            ),
            87 => 
            array (
                'citymunCode' => 101304,
                'citymunDesc' => 'DON CARLOS',
                'provCode' => 1013,
            ),
            88 => 
            array (
                'citymunCode' => 101305,
                'citymunDesc' => 'IMPASUG-ONG',
                'provCode' => 1013,
            ),
            89 => 
            array (
                'citymunCode' => 101306,
                'citymunDesc' => 'KADINGILAN',
                'provCode' => 1013,
            ),
            90 => 
            array (
                'citymunCode' => 101307,
                'citymunDesc' => 'KALILANGAN',
                'provCode' => 1013,
            ),
            91 => 
            array (
                'citymunCode' => 101308,
                'citymunDesc' => 'KIBAWE',
                'provCode' => 1013,
            ),
            92 => 
            array (
                'citymunCode' => 101309,
                'citymunDesc' => 'KITAOTAO',
                'provCode' => 1013,
            ),
            93 => 
            array (
                'citymunCode' => 101310,
                'citymunDesc' => 'LANTAPAN',
                'provCode' => 1013,
            ),
            94 => 
            array (
                'citymunCode' => 101311,
                'citymunDesc' => 'LIBONA',
                'provCode' => 1013,
            ),
            95 => 
            array (
                'citymunCode' => 101312,
            'citymunDesc' => 'CITY OF MALAYBALAY (Capital)',
                'provCode' => 1013,
            ),
            96 => 
            array (
                'citymunCode' => 101313,
                'citymunDesc' => 'MALITBOG',
                'provCode' => 1013,
            ),
            97 => 
            array (
                'citymunCode' => 101314,
                'citymunDesc' => 'MANOLO FORTICH',
                'provCode' => 1013,
            ),
            98 => 
            array (
                'citymunCode' => 101315,
                'citymunDesc' => 'MARAMAG',
                'provCode' => 1013,
            ),
            99 => 
            array (
                'citymunCode' => 101316,
                'citymunDesc' => 'PANGANTUCAN',
                'provCode' => 1013,
            ),
            100 => 
            array (
                'citymunCode' => 101317,
                'citymunDesc' => 'QUEZON',
                'provCode' => 1013,
            ),
            101 => 
            array (
                'citymunCode' => 101318,
                'citymunDesc' => 'SAN FERNANDO',
                'provCode' => 1013,
            ),
            102 => 
            array (
                'citymunCode' => 101319,
                'citymunDesc' => 'SUMILAO',
                'provCode' => 1013,
            ),
            103 => 
            array (
                'citymunCode' => 101320,
                'citymunDesc' => 'TALAKAG',
                'provCode' => 1013,
            ),
            104 => 
            array (
                'citymunCode' => 101321,
                'citymunDesc' => 'CITY OF VALENCIA',
                'provCode' => 1013,
            ),
            105 => 
            array (
                'citymunCode' => 101322,
                'citymunDesc' => 'CABANGLASAN',
                'provCode' => 1013,
            ),
            106 => 
            array (
                'citymunCode' => 101801,
                'citymunDesc' => 'CATARMAN',
                'provCode' => 1018,
            ),
            107 => 
            array (
                'citymunCode' => 101802,
                'citymunDesc' => 'GUINSILIBAN',
                'provCode' => 1018,
            ),
            108 => 
            array (
                'citymunCode' => 101803,
                'citymunDesc' => 'MAHINOG',
                'provCode' => 1018,
            ),
            109 => 
            array (
                'citymunCode' => 101804,
            'citymunDesc' => 'MAMBAJAO (Capital)',
                'provCode' => 1018,
            ),
            110 => 
            array (
                'citymunCode' => 101805,
                'citymunDesc' => 'SAGAY',
                'provCode' => 1018,
            ),
            111 => 
            array (
                'citymunCode' => 103501,
                'citymunDesc' => 'BACOLOD',
                'provCode' => 1035,
            ),
            112 => 
            array (
                'citymunCode' => 103502,
                'citymunDesc' => 'BALOI',
                'provCode' => 1035,
            ),
            113 => 
            array (
                'citymunCode' => 103503,
                'citymunDesc' => 'BAROY',
                'provCode' => 1035,
            ),
            114 => 
            array (
                'citymunCode' => 103504,
                'citymunDesc' => 'ILIGAN CITY',
                'provCode' => 1035,
            ),
            115 => 
            array (
                'citymunCode' => 103505,
                'citymunDesc' => 'KAPATAGAN',
                'provCode' => 1035,
            ),
            116 => 
            array (
                'citymunCode' => 103506,
            'citymunDesc' => 'SULTAN NAGA DIMAPORO (KAROMATAN)',
                'provCode' => 1035,
            ),
            117 => 
            array (
                'citymunCode' => 103507,
                'citymunDesc' => 'KAUSWAGAN',
                'provCode' => 1035,
            ),
            118 => 
            array (
                'citymunCode' => 103508,
                'citymunDesc' => 'KOLAMBUGAN',
                'provCode' => 1035,
            ),
            119 => 
            array (
                'citymunCode' => 103509,
                'citymunDesc' => 'LALA',
                'provCode' => 1035,
            ),
            120 => 
            array (
                'citymunCode' => 103510,
                'citymunDesc' => 'LINAMON',
                'provCode' => 1035,
            ),
            121 => 
            array (
                'citymunCode' => 103511,
                'citymunDesc' => 'MAGSAYSAY',
                'provCode' => 1035,
            ),
            122 => 
            array (
                'citymunCode' => 103512,
                'citymunDesc' => 'MAIGO',
                'provCode' => 1035,
            ),
            123 => 
            array (
                'citymunCode' => 103513,
                'citymunDesc' => 'MATUNGAO',
                'provCode' => 1035,
            ),
            124 => 
            array (
                'citymunCode' => 103514,
                'citymunDesc' => 'MUNAI',
                'provCode' => 1035,
            ),
            125 => 
            array (
                'citymunCode' => 103515,
                'citymunDesc' => 'NUNUNGAN',
                'provCode' => 1035,
            ),
            126 => 
            array (
                'citymunCode' => 103516,
                'citymunDesc' => 'PANTAO RAGAT',
                'provCode' => 1035,
            ),
            127 => 
            array (
                'citymunCode' => 103517,
                'citymunDesc' => 'POONA PIAGAPO',
                'provCode' => 1035,
            ),
            128 => 
            array (
                'citymunCode' => 103518,
                'citymunDesc' => 'SALVADOR',
                'provCode' => 1035,
            ),
            129 => 
            array (
                'citymunCode' => 103519,
                'citymunDesc' => 'SAPAD',
                'provCode' => 1035,
            ),
            130 => 
            array (
                'citymunCode' => 103520,
                'citymunDesc' => 'TAGOLOAN',
                'provCode' => 1035,
            ),
            131 => 
            array (
                'citymunCode' => 103521,
                'citymunDesc' => 'TANGCAL',
                'provCode' => 1035,
            ),
            132 => 
            array (
                'citymunCode' => 103522,
            'citymunDesc' => 'TUBOD (Capital)',
                'provCode' => 1035,
            ),
            133 => 
            array (
                'citymunCode' => 103523,
                'citymunDesc' => 'PANTAR',
                'provCode' => 1035,
            ),
            134 => 
            array (
                'citymunCode' => 104201,
                'citymunDesc' => 'ALORAN',
                'provCode' => 1042,
            ),
            135 => 
            array (
                'citymunCode' => 104202,
                'citymunDesc' => 'BALIANGAO',
                'provCode' => 1042,
            ),
            136 => 
            array (
                'citymunCode' => 104203,
                'citymunDesc' => 'BONIFACIO',
                'provCode' => 1042,
            ),
            137 => 
            array (
                'citymunCode' => 104204,
                'citymunDesc' => 'CALAMBA',
                'provCode' => 1042,
            ),
            138 => 
            array (
                'citymunCode' => 104205,
                'citymunDesc' => 'CLARIN',
                'provCode' => 1042,
            ),
            139 => 
            array (
                'citymunCode' => 104206,
                'citymunDesc' => 'CONCEPCION',
                'provCode' => 1042,
            ),
            140 => 
            array (
                'citymunCode' => 104207,
                'citymunDesc' => 'JIMENEZ',
                'provCode' => 1042,
            ),
            141 => 
            array (
                'citymunCode' => 104208,
                'citymunDesc' => 'LOPEZ JAENA',
                'provCode' => 1042,
            ),
            142 => 
            array (
                'citymunCode' => 104209,
            'citymunDesc' => 'OROQUIETA CITY (Capital)',
                'provCode' => 1042,
            ),
            143 => 
            array (
                'citymunCode' => 104210,
                'citymunDesc' => 'OZAMIS CITY',
                'provCode' => 1042,
            ),
            144 => 
            array (
                'citymunCode' => 104211,
                'citymunDesc' => 'PANAON',
                'provCode' => 1042,
            ),
            145 => 
            array (
                'citymunCode' => 104212,
                'citymunDesc' => 'PLARIDEL',
                'provCode' => 1042,
            ),
            146 => 
            array (
                'citymunCode' => 104213,
                'citymunDesc' => 'SAPANG DALAGA',
                'provCode' => 1042,
            ),
            147 => 
            array (
                'citymunCode' => 104214,
                'citymunDesc' => 'SINACABAN',
                'provCode' => 1042,
            ),
            148 => 
            array (
                'citymunCode' => 104215,
                'citymunDesc' => 'TANGUB CITY',
                'provCode' => 1042,
            ),
            149 => 
            array (
                'citymunCode' => 104216,
                'citymunDesc' => 'TUDELA',
                'provCode' => 1042,
            ),
            150 => 
            array (
                'citymunCode' => 104217,
            'citymunDesc' => 'DON VICTORIANO CHIONGBIAN  (DON MARIANO MARCOS)',
                'provCode' => 1042,
            ),
            151 => 
            array (
                'citymunCode' => 104301,
                'citymunDesc' => 'ALUBIJID',
                'provCode' => 1043,
            ),
            152 => 
            array (
                'citymunCode' => 104302,
                'citymunDesc' => 'BALINGASAG',
                'provCode' => 1043,
            ),
            153 => 
            array (
                'citymunCode' => 104303,
                'citymunDesc' => 'BALINGOAN',
                'provCode' => 1043,
            ),
            154 => 
            array (
                'citymunCode' => 104304,
                'citymunDesc' => 'BINUANGAN',
                'provCode' => 1043,
            ),
            155 => 
            array (
                'citymunCode' => 104305,
            'citymunDesc' => 'CAGAYAN DE ORO CITY (Capital)',
                'provCode' => 1043,
            ),
            156 => 
            array (
                'citymunCode' => 104306,
                'citymunDesc' => 'CLAVERIA',
                'provCode' => 1043,
            ),
            157 => 
            array (
                'citymunCode' => 104307,
                'citymunDesc' => 'CITY OF EL SALVADOR',
                'provCode' => 1043,
            ),
            158 => 
            array (
                'citymunCode' => 104308,
                'citymunDesc' => 'GINGOOG CITY',
                'provCode' => 1043,
            ),
            159 => 
            array (
                'citymunCode' => 104309,
                'citymunDesc' => 'GITAGUM',
                'provCode' => 1043,
            ),
            160 => 
            array (
                'citymunCode' => 104310,
                'citymunDesc' => 'INITAO',
                'provCode' => 1043,
            ),
            161 => 
            array (
                'citymunCode' => 104311,
                'citymunDesc' => 'JASAAN',
                'provCode' => 1043,
            ),
            162 => 
            array (
                'citymunCode' => 104312,
                'citymunDesc' => 'KINOGUITAN',
                'provCode' => 1043,
            ),
            163 => 
            array (
                'citymunCode' => 104313,
                'citymunDesc' => 'LAGONGLONG',
                'provCode' => 1043,
            ),
            164 => 
            array (
                'citymunCode' => 104314,
                'citymunDesc' => 'LAGUINDINGAN',
                'provCode' => 1043,
            ),
            165 => 
            array (
                'citymunCode' => 104315,
                'citymunDesc' => 'LIBERTAD',
                'provCode' => 1043,
            ),
            166 => 
            array (
                'citymunCode' => 104316,
                'citymunDesc' => 'LUGAIT',
                'provCode' => 1043,
            ),
            167 => 
            array (
                'citymunCode' => 104317,
            'citymunDesc' => 'MAGSAYSAY (LINUGOS)',
                'provCode' => 1043,
            ),
            168 => 
            array (
                'citymunCode' => 104318,
                'citymunDesc' => 'MANTICAO',
                'provCode' => 1043,
            ),
            169 => 
            array (
                'citymunCode' => 104319,
                'citymunDesc' => 'MEDINA',
                'provCode' => 1043,
            ),
            170 => 
            array (
                'citymunCode' => 104320,
                'citymunDesc' => 'NAAWAN',
                'provCode' => 1043,
            ),
            171 => 
            array (
                'citymunCode' => 104321,
                'citymunDesc' => 'OPOL',
                'provCode' => 1043,
            ),
            172 => 
            array (
                'citymunCode' => 104322,
                'citymunDesc' => 'SALAY',
                'provCode' => 1043,
            ),
            173 => 
            array (
                'citymunCode' => 104323,
                'citymunDesc' => 'SUGBONGCOGON',
                'provCode' => 1043,
            ),
            174 => 
            array (
                'citymunCode' => 104324,
                'citymunDesc' => 'TAGOLOAN',
                'provCode' => 1043,
            ),
            175 => 
            array (
                'citymunCode' => 104325,
                'citymunDesc' => 'TALISAYAN',
                'provCode' => 1043,
            ),
            176 => 
            array (
                'citymunCode' => 104326,
                'citymunDesc' => 'VILLANUEVA',
                'provCode' => 1043,
            ),
            177 => 
            array (
                'citymunCode' => 112301,
            'citymunDesc' => 'ASUNCION (SAUG)',
                'provCode' => 1123,
            ),
            178 => 
            array (
                'citymunCode' => 112303,
                'citymunDesc' => 'CARMEN',
                'provCode' => 1123,
            ),
            179 => 
            array (
                'citymunCode' => 112305,
                'citymunDesc' => 'KAPALONG',
                'provCode' => 1123,
            ),
            180 => 
            array (
                'citymunCode' => 112314,
                'citymunDesc' => 'NEW CORELLA',
                'provCode' => 1123,
            ),
            181 => 
            array (
                'citymunCode' => 112315,
                'citymunDesc' => 'CITY OF PANABO',
                'provCode' => 1123,
            ),
            182 => 
            array (
                'citymunCode' => 112317,
                'citymunDesc' => 'ISLAND GARDEN CITY OF SAMAL',
                'provCode' => 1123,
            ),
            183 => 
            array (
                'citymunCode' => 112318,
                'citymunDesc' => 'SANTO TOMAS',
                'provCode' => 1123,
            ),
            184 => 
            array (
                'citymunCode' => 112319,
            'citymunDesc' => 'CITY OF TAGUM (Capital)',
                'provCode' => 1123,
            ),
            185 => 
            array (
                'citymunCode' => 112322,
                'citymunDesc' => 'TALAINGOD',
                'provCode' => 1123,
            ),
            186 => 
            array (
                'citymunCode' => 112323,
                'citymunDesc' => 'BRAULIO E. DUJALI',
                'provCode' => 1123,
            ),
            187 => 
            array (
                'citymunCode' => 112324,
                'citymunDesc' => 'SAN ISIDRO',
                'provCode' => 1123,
            ),
            188 => 
            array (
                'citymunCode' => 112401,
                'citymunDesc' => 'BANSALAN',
                'provCode' => 1124,
            ),
            189 => 
            array (
                'citymunCode' => 112402,
                'citymunDesc' => 'DAVAO CITY',
                'provCode' => 1124,
            ),
            190 => 
            array (
                'citymunCode' => 112403,
            'citymunDesc' => 'CITY OF DIGOS (Capital)',
                'provCode' => 1124,
            ),
            191 => 
            array (
                'citymunCode' => 112404,
                'citymunDesc' => 'HAGONOY',
                'provCode' => 1124,
            ),
            192 => 
            array (
                'citymunCode' => 112406,
                'citymunDesc' => 'KIBLAWAN',
                'provCode' => 1124,
            ),
            193 => 
            array (
                'citymunCode' => 112407,
                'citymunDesc' => 'MAGSAYSAY',
                'provCode' => 1124,
            ),
            194 => 
            array (
                'citymunCode' => 112408,
                'citymunDesc' => 'MALALAG',
                'provCode' => 1124,
            ),
            195 => 
            array (
                'citymunCode' => 112410,
                'citymunDesc' => 'MATANAO',
                'provCode' => 1124,
            ),
            196 => 
            array (
                'citymunCode' => 112411,
                'citymunDesc' => 'PADADA',
                'provCode' => 1124,
            ),
            197 => 
            array (
                'citymunCode' => 112412,
                'citymunDesc' => 'SANTA CRUZ',
                'provCode' => 1124,
            ),
            198 => 
            array (
                'citymunCode' => 112414,
                'citymunDesc' => 'SULOP',
                'provCode' => 1124,
            ),
            199 => 
            array (
                'citymunCode' => 112501,
                'citymunDesc' => 'BAGANGA',
                'provCode' => 1125,
            ),
            200 => 
            array (
                'citymunCode' => 112502,
                'citymunDesc' => 'BANAYBANAY',
                'provCode' => 1125,
            ),
            201 => 
            array (
                'citymunCode' => 112503,
                'citymunDesc' => 'BOSTON',
                'provCode' => 1125,
            ),
            202 => 
            array (
                'citymunCode' => 112504,
                'citymunDesc' => 'CARAGA',
                'provCode' => 1125,
            ),
            203 => 
            array (
                'citymunCode' => 112505,
                'citymunDesc' => 'CATEEL',
                'provCode' => 1125,
            ),
            204 => 
            array (
                'citymunCode' => 112506,
                'citymunDesc' => 'GOVERNOR GENEROSO',
                'provCode' => 1125,
            ),
            205 => 
            array (
                'citymunCode' => 112507,
                'citymunDesc' => 'LUPON',
                'provCode' => 1125,
            ),
            206 => 
            array (
                'citymunCode' => 112508,
                'citymunDesc' => 'MANAY',
                'provCode' => 1125,
            ),
            207 => 
            array (
                'citymunCode' => 112509,
            'citymunDesc' => 'CITY OF MATI (Capital)',
                'provCode' => 1125,
            ),
            208 => 
            array (
                'citymunCode' => 112510,
                'citymunDesc' => 'SAN ISIDRO',
                'provCode' => 1125,
            ),
            209 => 
            array (
                'citymunCode' => 112511,
                'citymunDesc' => 'TARRAGONA',
                'provCode' => 1125,
            ),
            210 => 
            array (
                'citymunCode' => 118201,
                'citymunDesc' => 'COMPOSTELA',
                'provCode' => 1182,
            ),
            211 => 
            array (
                'citymunCode' => 118202,
            'citymunDesc' => 'LAAK (SAN VICENTE)',
                'provCode' => 1182,
            ),
            212 => 
            array (
                'citymunCode' => 118203,
            'citymunDesc' => 'MABINI (DOÑA ALICIA)',
                'provCode' => 1182,
            ),
            213 => 
            array (
                'citymunCode' => 118204,
                'citymunDesc' => 'MACO',
                'provCode' => 1182,
            ),
            214 => 
            array (
                'citymunCode' => 118205,
            'citymunDesc' => 'MARAGUSAN (SAN MARIANO)',
                'provCode' => 1182,
            ),
            215 => 
            array (
                'citymunCode' => 118206,
                'citymunDesc' => 'MAWAB',
                'provCode' => 1182,
            ),
            216 => 
            array (
                'citymunCode' => 118207,
                'citymunDesc' => 'MONKAYO',
                'provCode' => 1182,
            ),
            217 => 
            array (
                'citymunCode' => 118208,
                'citymunDesc' => 'MONTEVISTA',
                'provCode' => 1182,
            ),
            218 => 
            array (
                'citymunCode' => 118209,
            'citymunDesc' => 'NABUNTURAN (Capital)',
                'provCode' => 1182,
            ),
            219 => 
            array (
                'citymunCode' => 118210,
                'citymunDesc' => 'NEW BATAAN',
                'provCode' => 1182,
            ),
            220 => 
            array (
                'citymunCode' => 118211,
                'citymunDesc' => 'PANTUKAN',
                'provCode' => 1182,
            ),
            221 => 
            array (
                'citymunCode' => 118601,
                'citymunDesc' => 'DON MARCELINO',
                'provCode' => 1186,
            ),
            222 => 
            array (
                'citymunCode' => 118602,
            'citymunDesc' => 'JOSE ABAD SANTOS (TRINIDAD)',
                'provCode' => 1186,
            ),
            223 => 
            array (
                'citymunCode' => 118603,
                'citymunDesc' => 'MALITA',
                'provCode' => 1186,
            ),
            224 => 
            array (
                'citymunCode' => 118604,
                'citymunDesc' => 'SANTA MARIA',
                'provCode' => 1186,
            ),
            225 => 
            array (
                'citymunCode' => 118605,
                'citymunDesc' => 'SARANGANI',
                'provCode' => 1186,
            ),
            226 => 
            array (
                'citymunCode' => 124701,
                'citymunDesc' => 'ALAMADA',
                'provCode' => 1247,
            ),
            227 => 
            array (
                'citymunCode' => 124702,
                'citymunDesc' => 'CARMEN',
                'provCode' => 1247,
            ),
            228 => 
            array (
                'citymunCode' => 124703,
                'citymunDesc' => 'KABACAN',
                'provCode' => 1247,
            ),
            229 => 
            array (
                'citymunCode' => 124704,
            'citymunDesc' => 'CITY OF KIDAPAWAN (Capital)',
                'provCode' => 1247,
            ),
            230 => 
            array (
                'citymunCode' => 124705,
                'citymunDesc' => 'LIBUNGAN',
                'provCode' => 1247,
            ),
            231 => 
            array (
                'citymunCode' => 124706,
                'citymunDesc' => 'MAGPET',
                'provCode' => 1247,
            ),
            232 => 
            array (
                'citymunCode' => 124707,
                'citymunDesc' => 'MAKILALA',
                'provCode' => 1247,
            ),
            233 => 
            array (
                'citymunCode' => 124708,
                'citymunDesc' => 'MATALAM',
                'provCode' => 1247,
            ),
            234 => 
            array (
                'citymunCode' => 124709,
                'citymunDesc' => 'MIDSAYAP',
                'provCode' => 1247,
            ),
            235 => 
            array (
                'citymunCode' => 124710,
                'citymunDesc' => 'M\'LANG',
                'provCode' => 1247,
            ),
            236 => 
            array (
                'citymunCode' => 124711,
                'citymunDesc' => 'PIGKAWAYAN',
                'provCode' => 1247,
            ),
            237 => 
            array (
                'citymunCode' => 124712,
                'citymunDesc' => 'PIKIT',
                'provCode' => 1247,
            ),
            238 => 
            array (
                'citymunCode' => 124713,
                'citymunDesc' => 'PRESIDENT ROXAS',
                'provCode' => 1247,
            ),
            239 => 
            array (
                'citymunCode' => 124714,
                'citymunDesc' => 'TULUNAN',
                'provCode' => 1247,
            ),
            240 => 
            array (
                'citymunCode' => 124715,
                'citymunDesc' => 'ANTIPAS',
                'provCode' => 1247,
            ),
            241 => 
            array (
                'citymunCode' => 124716,
                'citymunDesc' => 'BANISILAN',
                'provCode' => 1247,
            ),
            242 => 
            array (
                'citymunCode' => 124717,
                'citymunDesc' => 'ALEOSAN',
                'provCode' => 1247,
            ),
            243 => 
            array (
                'citymunCode' => 124718,
                'citymunDesc' => 'ARAKAN',
                'provCode' => 1247,
            ),
            244 => 
            array (
                'citymunCode' => 126302,
                'citymunDesc' => 'BANGA',
                'provCode' => 1263,
            ),
            245 => 
            array (
                'citymunCode' => 126303,
            'citymunDesc' => 'GENERAL SANTOS CITY (DADIANGAS)',
                'provCode' => 1263,
            ),
            246 => 
            array (
                'citymunCode' => 126306,
            'citymunDesc' => 'CITY OF KORONADAL (Capital)',
                'provCode' => 1263,
            ),
            247 => 
            array (
                'citymunCode' => 126311,
                'citymunDesc' => 'NORALA',
                'provCode' => 1263,
            ),
            248 => 
            array (
                'citymunCode' => 126312,
                'citymunDesc' => 'POLOMOLOK',
                'provCode' => 1263,
            ),
            249 => 
            array (
                'citymunCode' => 126313,
                'citymunDesc' => 'SURALLAH',
                'provCode' => 1263,
            ),
            250 => 
            array (
                'citymunCode' => 126314,
                'citymunDesc' => 'TAMPAKAN',
                'provCode' => 1263,
            ),
            251 => 
            array (
                'citymunCode' => 126315,
                'citymunDesc' => 'TANTANGAN',
                'provCode' => 1263,
            ),
            252 => 
            array (
                'citymunCode' => 126316,
                'citymunDesc' => 'T\'BOLI',
                'provCode' => 1263,
            ),
            253 => 
            array (
                'citymunCode' => 126317,
                'citymunDesc' => 'TUPI',
                'provCode' => 1263,
            ),
            254 => 
            array (
                'citymunCode' => 126318,
                'citymunDesc' => 'SANTO NIÑO',
                'provCode' => 1263,
            ),
            255 => 
            array (
                'citymunCode' => 126319,
                'citymunDesc' => 'LAKE SEBU',
                'provCode' => 1263,
            ),
            256 => 
            array (
                'citymunCode' => 126501,
                'citymunDesc' => 'BAGUMBAYAN',
                'provCode' => 1265,
            ),
            257 => 
            array (
                'citymunCode' => 126502,
                'citymunDesc' => 'COLUMBIO',
                'provCode' => 1265,
            ),
            258 => 
            array (
                'citymunCode' => 126503,
                'citymunDesc' => 'ESPERANZA',
                'provCode' => 1265,
            ),
            259 => 
            array (
                'citymunCode' => 126504,
            'citymunDesc' => 'ISULAN (Capital)',
                'provCode' => 1265,
            ),
            260 => 
            array (
                'citymunCode' => 126505,
                'citymunDesc' => 'KALAMANSIG',
                'provCode' => 1265,
            ),
            261 => 
            array (
                'citymunCode' => 126506,
                'citymunDesc' => 'LEBAK',
                'provCode' => 1265,
            ),
            262 => 
            array (
                'citymunCode' => 126507,
                'citymunDesc' => 'LUTAYAN',
                'provCode' => 1265,
            ),
            263 => 
            array (
                'citymunCode' => 126508,
            'citymunDesc' => 'LAMBAYONG (MARIANO MARCOS)',
                'provCode' => 1265,
            ),
            264 => 
            array (
                'citymunCode' => 126509,
                'citymunDesc' => 'PALIMBANG',
                'provCode' => 1265,
            ),
            265 => 
            array (
                'citymunCode' => 126510,
                'citymunDesc' => 'PRESIDENT QUIRINO',
                'provCode' => 1265,
            ),
            266 => 
            array (
                'citymunCode' => 126511,
                'citymunDesc' => 'CITY OF TACURONG',
                'provCode' => 1265,
            ),
            267 => 
            array (
                'citymunCode' => 126512,
                'citymunDesc' => 'SEN. NINOY AQUINO',
                'provCode' => 1265,
            ),
            268 => 
            array (
                'citymunCode' => 128001,
            'citymunDesc' => 'ALABEL (Capital)',
                'provCode' => 1280,
            ),
            269 => 
            array (
                'citymunCode' => 128002,
                'citymunDesc' => 'GLAN',
                'provCode' => 1280,
            ),
            270 => 
            array (
                'citymunCode' => 128003,
                'citymunDesc' => 'KIAMBA',
                'provCode' => 1280,
            ),
            271 => 
            array (
                'citymunCode' => 128004,
                'citymunDesc' => 'MAASIM',
                'provCode' => 1280,
            ),
            272 => 
            array (
                'citymunCode' => 128005,
                'citymunDesc' => 'MAITUM',
                'provCode' => 1280,
            ),
            273 => 
            array (
                'citymunCode' => 128006,
                'citymunDesc' => 'MALAPATAN',
                'provCode' => 1280,
            ),
            274 => 
            array (
                'citymunCode' => 128007,
                'citymunDesc' => 'MALUNGON',
                'provCode' => 1280,
            ),
            275 => 
            array (
                'citymunCode' => 129804,
                'citymunDesc' => 'COTABATO CITY',
                'provCode' => 1298,
            ),
            276 => 
            array (
                'citymunCode' => 133901,
                'citymunDesc' => 'TONDO I / II',
                'provCode' => 1339,
            ),
            277 => 
            array (
                'citymunCode' => 133902,
                'citymunDesc' => 'BINONDO',
                'provCode' => 1339,
            ),
            278 => 
            array (
                'citymunCode' => 133903,
                'citymunDesc' => 'QUIAPO',
                'provCode' => 1339,
            ),
            279 => 
            array (
                'citymunCode' => 133904,
                'citymunDesc' => 'SAN NICOLAS',
                'provCode' => 1339,
            ),
            280 => 
            array (
                'citymunCode' => 133905,
                'citymunDesc' => 'SANTA CRUZ',
                'provCode' => 1339,
            ),
            281 => 
            array (
                'citymunCode' => 133906,
                'citymunDesc' => 'SAMPALOC',
                'provCode' => 1339,
            ),
            282 => 
            array (
                'citymunCode' => 133907,
                'citymunDesc' => 'SAN MIGUEL',
                'provCode' => 1339,
            ),
            283 => 
            array (
                'citymunCode' => 133908,
                'citymunDesc' => 'ERMITA',
                'provCode' => 1339,
            ),
            284 => 
            array (
                'citymunCode' => 133909,
                'citymunDesc' => 'INTRAMUROS',
                'provCode' => 1339,
            ),
            285 => 
            array (
                'citymunCode' => 133910,
                'citymunDesc' => 'MALATE',
                'provCode' => 1339,
            ),
            286 => 
            array (
                'citymunCode' => 133911,
                'citymunDesc' => 'PACO',
                'provCode' => 1339,
            ),
            287 => 
            array (
                'citymunCode' => 133912,
                'citymunDesc' => 'PANDACAN',
                'provCode' => 1339,
            ),
            288 => 
            array (
                'citymunCode' => 133913,
                'citymunDesc' => 'PORT AREA',
                'provCode' => 1339,
            ),
            289 => 
            array (
                'citymunCode' => 133914,
                'citymunDesc' => 'SANTA ANA',
                'provCode' => 1339,
            ),
            290 => 
            array (
                'citymunCode' => 137401,
                'citymunDesc' => 'CITY OF MANDALUYONG',
                'provCode' => 1374,
            ),
            291 => 
            array (
                'citymunCode' => 137402,
                'citymunDesc' => 'CITY OF MARIKINA',
                'provCode' => 1374,
            ),
            292 => 
            array (
                'citymunCode' => 137403,
                'citymunDesc' => 'CITY OF PASIG',
                'provCode' => 1374,
            ),
            293 => 
            array (
                'citymunCode' => 137404,
                'citymunDesc' => 'QUEZON CITY',
                'provCode' => 1374,
            ),
            294 => 
            array (
                'citymunCode' => 137405,
                'citymunDesc' => 'CITY OF SAN JUAN',
                'provCode' => 1374,
            ),
            295 => 
            array (
                'citymunCode' => 137501,
                'citymunDesc' => 'CALOOCAN CITY',
                'provCode' => 1375,
            ),
            296 => 
            array (
                'citymunCode' => 137502,
                'citymunDesc' => 'CITY OF MALABON',
                'provCode' => 1375,
            ),
            297 => 
            array (
                'citymunCode' => 137503,
                'citymunDesc' => 'CITY OF NAVOTAS',
                'provCode' => 1375,
            ),
            298 => 
            array (
                'citymunCode' => 137504,
                'citymunDesc' => 'CITY OF VALENZUELA',
                'provCode' => 1375,
            ),
            299 => 
            array (
                'citymunCode' => 137601,
                'citymunDesc' => 'CITY OF LAS PIÑAS',
                'provCode' => 1376,
            ),
            300 => 
            array (
                'citymunCode' => 137602,
                'citymunDesc' => 'CITY OF MAKATI',
                'provCode' => 1376,
            ),
            301 => 
            array (
                'citymunCode' => 137603,
                'citymunDesc' => 'CITY OF MUNTINLUPA',
                'provCode' => 1376,
            ),
            302 => 
            array (
                'citymunCode' => 137604,
                'citymunDesc' => 'CITY OF PARAÑAQUE',
                'provCode' => 1376,
            ),
            303 => 
            array (
                'citymunCode' => 137605,
                'citymunDesc' => 'PASAY CITY',
                'provCode' => 1376,
            ),
            304 => 
            array (
                'citymunCode' => 137606,
                'citymunDesc' => 'PATEROS',
                'provCode' => 1376,
            ),
            305 => 
            array (
                'citymunCode' => 137607,
                'citymunDesc' => 'TAGUIG CITY',
                'provCode' => 1376,
            ),
            306 => 
            array (
                'citymunCode' => 140101,
            'citymunDesc' => 'BANGUED (Capital)',
                'provCode' => 1401,
            ),
            307 => 
            array (
                'citymunCode' => 140102,
                'citymunDesc' => 'BOLINEY',
                'provCode' => 1401,
            ),
            308 => 
            array (
                'citymunCode' => 140103,
                'citymunDesc' => 'BUCAY',
                'provCode' => 1401,
            ),
            309 => 
            array (
                'citymunCode' => 140104,
                'citymunDesc' => 'BUCLOC',
                'provCode' => 1401,
            ),
            310 => 
            array (
                'citymunCode' => 140105,
                'citymunDesc' => 'DAGUIOMAN',
                'provCode' => 1401,
            ),
            311 => 
            array (
                'citymunCode' => 140106,
                'citymunDesc' => 'DANGLAS',
                'provCode' => 1401,
            ),
            312 => 
            array (
                'citymunCode' => 140107,
                'citymunDesc' => 'DOLORES',
                'provCode' => 1401,
            ),
            313 => 
            array (
                'citymunCode' => 140108,
                'citymunDesc' => 'LA PAZ',
                'provCode' => 1401,
            ),
            314 => 
            array (
                'citymunCode' => 140109,
                'citymunDesc' => 'LACUB',
                'provCode' => 1401,
            ),
            315 => 
            array (
                'citymunCode' => 140110,
                'citymunDesc' => 'LAGANGILANG',
                'provCode' => 1401,
            ),
            316 => 
            array (
                'citymunCode' => 140111,
                'citymunDesc' => 'LAGAYAN',
                'provCode' => 1401,
            ),
            317 => 
            array (
                'citymunCode' => 140112,
                'citymunDesc' => 'LANGIDEN',
                'provCode' => 1401,
            ),
            318 => 
            array (
                'citymunCode' => 140113,
            'citymunDesc' => 'LICUAN-BAAY (LICUAN)',
                'provCode' => 1401,
            ),
            319 => 
            array (
                'citymunCode' => 140114,
                'citymunDesc' => 'LUBA',
                'provCode' => 1401,
            ),
            320 => 
            array (
                'citymunCode' => 140115,
                'citymunDesc' => 'MALIBCONG',
                'provCode' => 1401,
            ),
            321 => 
            array (
                'citymunCode' => 140116,
                'citymunDesc' => 'MANABO',
                'provCode' => 1401,
            ),
            322 => 
            array (
                'citymunCode' => 140117,
                'citymunDesc' => 'PEÑARRUBIA',
                'provCode' => 1401,
            ),
            323 => 
            array (
                'citymunCode' => 140118,
                'citymunDesc' => 'PIDIGAN',
                'provCode' => 1401,
            ),
            324 => 
            array (
                'citymunCode' => 140119,
                'citymunDesc' => 'PILAR',
                'provCode' => 1401,
            ),
            325 => 
            array (
                'citymunCode' => 140120,
                'citymunDesc' => 'SALLAPADAN',
                'provCode' => 1401,
            ),
            326 => 
            array (
                'citymunCode' => 140121,
                'citymunDesc' => 'SAN ISIDRO',
                'provCode' => 1401,
            ),
            327 => 
            array (
                'citymunCode' => 140122,
                'citymunDesc' => 'SAN JUAN',
                'provCode' => 1401,
            ),
            328 => 
            array (
                'citymunCode' => 140123,
                'citymunDesc' => 'SAN QUINTIN',
                'provCode' => 1401,
            ),
            329 => 
            array (
                'citymunCode' => 140124,
                'citymunDesc' => 'TAYUM',
                'provCode' => 1401,
            ),
            330 => 
            array (
                'citymunCode' => 140125,
                'citymunDesc' => 'TINEG',
                'provCode' => 1401,
            ),
            331 => 
            array (
                'citymunCode' => 140126,
                'citymunDesc' => 'TUBO',
                'provCode' => 1401,
            ),
            332 => 
            array (
                'citymunCode' => 140127,
                'citymunDesc' => 'VILLAVICIOSA',
                'provCode' => 1401,
            ),
            333 => 
            array (
                'citymunCode' => 141101,
                'citymunDesc' => 'ATOK',
                'provCode' => 1411,
            ),
            334 => 
            array (
                'citymunCode' => 141102,
                'citymunDesc' => 'BAGUIO CITY',
                'provCode' => 1411,
            ),
            335 => 
            array (
                'citymunCode' => 141103,
                'citymunDesc' => 'BAKUN',
                'provCode' => 1411,
            ),
            336 => 
            array (
                'citymunCode' => 141104,
                'citymunDesc' => 'BOKOD',
                'provCode' => 1411,
            ),
            337 => 
            array (
                'citymunCode' => 141105,
                'citymunDesc' => 'BUGUIAS',
                'provCode' => 1411,
            ),
            338 => 
            array (
                'citymunCode' => 141106,
                'citymunDesc' => 'ITOGON',
                'provCode' => 1411,
            ),
            339 => 
            array (
                'citymunCode' => 141107,
                'citymunDesc' => 'KABAYAN',
                'provCode' => 1411,
            ),
            340 => 
            array (
                'citymunCode' => 141108,
                'citymunDesc' => 'KAPANGAN',
                'provCode' => 1411,
            ),
            341 => 
            array (
                'citymunCode' => 141109,
                'citymunDesc' => 'KIBUNGAN',
                'provCode' => 1411,
            ),
            342 => 
            array (
                'citymunCode' => 141110,
            'citymunDesc' => 'LA TRINIDAD (Capital)',
                'provCode' => 1411,
            ),
            343 => 
            array (
                'citymunCode' => 141111,
                'citymunDesc' => 'MANKAYAN',
                'provCode' => 1411,
            ),
            344 => 
            array (
                'citymunCode' => 141112,
                'citymunDesc' => 'SABLAN',
                'provCode' => 1411,
            ),
            345 => 
            array (
                'citymunCode' => 141113,
                'citymunDesc' => 'TUBA',
                'provCode' => 1411,
            ),
            346 => 
            array (
                'citymunCode' => 141114,
                'citymunDesc' => 'TUBLAY',
                'provCode' => 1411,
            ),
            347 => 
            array (
                'citymunCode' => 142701,
                'citymunDesc' => 'BANAUE',
                'provCode' => 1427,
            ),
            348 => 
            array (
                'citymunCode' => 142702,
                'citymunDesc' => 'HUNGDUAN',
                'provCode' => 1427,
            ),
            349 => 
            array (
                'citymunCode' => 142703,
                'citymunDesc' => 'KIANGAN',
                'provCode' => 1427,
            ),
            350 => 
            array (
                'citymunCode' => 142704,
            'citymunDesc' => 'LAGAWE (Capital)',
                'provCode' => 1427,
            ),
            351 => 
            array (
                'citymunCode' => 142705,
                'citymunDesc' => 'LAMUT',
                'provCode' => 1427,
            ),
            352 => 
            array (
                'citymunCode' => 142706,
                'citymunDesc' => 'MAYOYAO',
                'provCode' => 1427,
            ),
            353 => 
            array (
                'citymunCode' => 142707,
            'citymunDesc' => 'ALFONSO LISTA (POTIA)',
                'provCode' => 1427,
            ),
            354 => 
            array (
                'citymunCode' => 142708,
                'citymunDesc' => 'AGUINALDO',
                'provCode' => 1427,
            ),
            355 => 
            array (
                'citymunCode' => 142709,
                'citymunDesc' => 'HINGYON',
                'provCode' => 1427,
            ),
            356 => 
            array (
                'citymunCode' => 142710,
                'citymunDesc' => 'TINOC',
                'provCode' => 1427,
            ),
            357 => 
            array (
                'citymunCode' => 142711,
                'citymunDesc' => 'ASIPULO',
                'provCode' => 1427,
            ),
            358 => 
            array (
                'citymunCode' => 143201,
                'citymunDesc' => 'BALBALAN',
                'provCode' => 1432,
            ),
            359 => 
            array (
                'citymunCode' => 143206,
                'citymunDesc' => 'LUBUAGAN',
                'provCode' => 1432,
            ),
            360 => 
            array (
                'citymunCode' => 143208,
                'citymunDesc' => 'PASIL',
                'provCode' => 1432,
            ),
            361 => 
            array (
                'citymunCode' => 143209,
                'citymunDesc' => 'PINUKPUK',
                'provCode' => 1432,
            ),
            362 => 
            array (
                'citymunCode' => 143211,
            'citymunDesc' => 'RIZAL (LIWAN)',
                'provCode' => 1432,
            ),
            363 => 
            array (
                'citymunCode' => 143213,
            'citymunDesc' => 'CITY OF TABUK (Capital)',
                'provCode' => 1432,
            ),
            364 => 
            array (
                'citymunCode' => 143214,
                'citymunDesc' => 'TANUDAN',
                'provCode' => 1432,
            ),
            365 => 
            array (
                'citymunCode' => 143215,
                'citymunDesc' => 'TINGLAYAN',
                'provCode' => 1432,
            ),
            366 => 
            array (
                'citymunCode' => 144401,
                'citymunDesc' => 'BARLIG',
                'provCode' => 1444,
            ),
            367 => 
            array (
                'citymunCode' => 144402,
                'citymunDesc' => 'BAUKO',
                'provCode' => 1444,
            ),
            368 => 
            array (
                'citymunCode' => 144403,
                'citymunDesc' => 'BESAO',
                'provCode' => 1444,
            ),
            369 => 
            array (
                'citymunCode' => 144404,
            'citymunDesc' => 'BONTOC (Capital)',
                'provCode' => 1444,
            ),
            370 => 
            array (
                'citymunCode' => 144405,
                'citymunDesc' => 'NATONIN',
                'provCode' => 1444,
            ),
            371 => 
            array (
                'citymunCode' => 144406,
                'citymunDesc' => 'PARACELIS',
                'provCode' => 1444,
            ),
            372 => 
            array (
                'citymunCode' => 144407,
                'citymunDesc' => 'SABANGAN',
                'provCode' => 1444,
            ),
            373 => 
            array (
                'citymunCode' => 144408,
                'citymunDesc' => 'SADANGA',
                'provCode' => 1444,
            ),
            374 => 
            array (
                'citymunCode' => 144409,
                'citymunDesc' => 'SAGADA',
                'provCode' => 1444,
            ),
            375 => 
            array (
                'citymunCode' => 144410,
                'citymunDesc' => 'TADIAN',
                'provCode' => 1444,
            ),
            376 => 
            array (
                'citymunCode' => 148101,
            'citymunDesc' => 'CALANASAN (BAYAG)',
                'provCode' => 1481,
            ),
            377 => 
            array (
                'citymunCode' => 148102,
                'citymunDesc' => 'CONNER',
                'provCode' => 1481,
            ),
            378 => 
            array (
                'citymunCode' => 148103,
                'citymunDesc' => 'FLORA',
                'provCode' => 1481,
            ),
            379 => 
            array (
                'citymunCode' => 148104,
            'citymunDesc' => 'KABUGAO (Capital)',
                'provCode' => 1481,
            ),
            380 => 
            array (
                'citymunCode' => 148105,
                'citymunDesc' => 'LUNA',
                'provCode' => 1481,
            ),
            381 => 
            array (
                'citymunCode' => 148106,
                'citymunDesc' => 'PUDTOL',
                'provCode' => 1481,
            ),
            382 => 
            array (
                'citymunCode' => 148107,
                'citymunDesc' => 'SANTA MARCELA',
                'provCode' => 1481,
            ),
            383 => 
            array (
                'citymunCode' => 150702,
                'citymunDesc' => 'CITY OF LAMITAN',
                'provCode' => 1507,
            ),
            384 => 
            array (
                'citymunCode' => 150703,
                'citymunDesc' => 'LANTAWAN',
                'provCode' => 1507,
            ),
            385 => 
            array (
                'citymunCode' => 150704,
                'citymunDesc' => 'MALUSO',
                'provCode' => 1507,
            ),
            386 => 
            array (
                'citymunCode' => 150705,
                'citymunDesc' => 'SUMISIP',
                'provCode' => 1507,
            ),
            387 => 
            array (
                'citymunCode' => 150706,
                'citymunDesc' => 'TIPO-TIPO',
                'provCode' => 1507,
            ),
            388 => 
            array (
                'citymunCode' => 150707,
                'citymunDesc' => 'TUBURAN',
                'provCode' => 1507,
            ),
            389 => 
            array (
                'citymunCode' => 150708,
                'citymunDesc' => 'AKBAR',
                'provCode' => 1507,
            ),
            390 => 
            array (
                'citymunCode' => 150709,
                'citymunDesc' => 'AL-BARKA',
                'provCode' => 1507,
            ),
            391 => 
            array (
                'citymunCode' => 150710,
                'citymunDesc' => 'HADJI MOHAMMAD AJUL',
                'provCode' => 1507,
            ),
            392 => 
            array (
                'citymunCode' => 150711,
                'citymunDesc' => 'UNGKAYA PUKAN',
                'provCode' => 1507,
            ),
            393 => 
            array (
                'citymunCode' => 150712,
                'citymunDesc' => 'HADJI MUHTAMAD',
                'provCode' => 1507,
            ),
            394 => 
            array (
                'citymunCode' => 150713,
                'citymunDesc' => 'TABUAN-LASA',
                'provCode' => 1507,
            ),
            395 => 
            array (
                'citymunCode' => 153601,
            'citymunDesc' => 'BACOLOD-KALAWI (BACOLOD GRANDE)',
                'provCode' => 1536,
            ),
            396 => 
            array (
                'citymunCode' => 153602,
                'citymunDesc' => 'BALABAGAN',
                'provCode' => 1536,
            ),
            397 => 
            array (
                'citymunCode' => 153603,
            'citymunDesc' => 'BALINDONG (WATU)',
                'provCode' => 1536,
            ),
            398 => 
            array (
                'citymunCode' => 153604,
                'citymunDesc' => 'BAYANG',
                'provCode' => 1536,
            ),
            399 => 
            array (
                'citymunCode' => 153605,
                'citymunDesc' => 'BINIDAYAN',
                'provCode' => 1536,
            ),
            400 => 
            array (
                'citymunCode' => 153606,
                'citymunDesc' => 'BUBONG',
                'provCode' => 1536,
            ),
            401 => 
            array (
                'citymunCode' => 153607,
                'citymunDesc' => 'BUTIG',
                'provCode' => 1536,
            ),
            402 => 
            array (
                'citymunCode' => 153609,
                'citymunDesc' => 'GANASSI',
                'provCode' => 1536,
            ),
            403 => 
            array (
                'citymunCode' => 153610,
                'citymunDesc' => 'KAPAI',
                'provCode' => 1536,
            ),
            404 => 
            array (
                'citymunCode' => 153611,
            'citymunDesc' => 'LUMBA-BAYABAO (MAGUING)',
                'provCode' => 1536,
            ),
            405 => 
            array (
                'citymunCode' => 153612,
                'citymunDesc' => 'LUMBATAN',
                'provCode' => 1536,
            ),
            406 => 
            array (
                'citymunCode' => 153613,
                'citymunDesc' => 'MADALUM',
                'provCode' => 1536,
            ),
            407 => 
            array (
                'citymunCode' => 153614,
                'citymunDesc' => 'MADAMBA',
                'provCode' => 1536,
            ),
            408 => 
            array (
                'citymunCode' => 153615,
                'citymunDesc' => 'MALABANG',
                'provCode' => 1536,
            ),
            409 => 
            array (
                'citymunCode' => 153616,
                'citymunDesc' => 'MARANTAO',
                'provCode' => 1536,
            ),
            410 => 
            array (
                'citymunCode' => 153617,
            'citymunDesc' => 'MARAWI CITY (Capital)',
                'provCode' => 1536,
            ),
            411 => 
            array (
                'citymunCode' => 153618,
                'citymunDesc' => 'MASIU',
                'provCode' => 1536,
            ),
            412 => 
            array (
                'citymunCode' => 153619,
                'citymunDesc' => 'MULONDO',
                'provCode' => 1536,
            ),
            413 => 
            array (
                'citymunCode' => 153620,
            'citymunDesc' => 'PAGAYAWAN (TATARIKAN)',
                'provCode' => 1536,
            ),
            414 => 
            array (
                'citymunCode' => 153621,
                'citymunDesc' => 'PIAGAPO',
                'provCode' => 1536,
            ),
            415 => 
            array (
                'citymunCode' => 153622,
            'citymunDesc' => 'POONA BAYABAO (GATA)',
                'provCode' => 1536,
            ),
            416 => 
            array (
                'citymunCode' => 153623,
                'citymunDesc' => 'PUALAS',
                'provCode' => 1536,
            ),
            417 => 
            array (
                'citymunCode' => 153624,
                'citymunDesc' => 'DITSAAN-RAMAIN',
                'provCode' => 1536,
            ),
            418 => 
            array (
                'citymunCode' => 153625,
                'citymunDesc' => 'SAGUIARAN',
                'provCode' => 1536,
            ),
            419 => 
            array (
                'citymunCode' => 153626,
                'citymunDesc' => 'TAMPARAN',
                'provCode' => 1536,
            ),
            420 => 
            array (
                'citymunCode' => 153627,
                'citymunDesc' => 'TARAKA',
                'provCode' => 1536,
            ),
            421 => 
            array (
                'citymunCode' => 153628,
                'citymunDesc' => 'TUBARAN',
                'provCode' => 1536,
            ),
            422 => 
            array (
                'citymunCode' => 153629,
                'citymunDesc' => 'TUGAYA',
                'provCode' => 1536,
            ),
            423 => 
            array (
                'citymunCode' => 153630,
                'citymunDesc' => 'WAO',
                'provCode' => 1536,
            ),
            424 => 
            array (
                'citymunCode' => 153631,
                'citymunDesc' => 'MAROGONG',
                'provCode' => 1536,
            ),
            425 => 
            array (
                'citymunCode' => 153632,
                'citymunDesc' => 'CALANOGAS',
                'provCode' => 1536,
            ),
            426 => 
            array (
                'citymunCode' => 153633,
                'citymunDesc' => 'BUADIPOSO-BUNTONG',
                'provCode' => 1536,
            ),
            427 => 
            array (
                'citymunCode' => 153634,
                'citymunDesc' => 'MAGUING',
                'provCode' => 1536,
            ),
            428 => 
            array (
                'citymunCode' => 153635,
            'citymunDesc' => 'PICONG (SULTAN GUMANDER)',
                'provCode' => 1536,
            ),
            429 => 
            array (
                'citymunCode' => 153636,
                'citymunDesc' => 'LUMBAYANAGUE',
                'provCode' => 1536,
            ),
            430 => 
            array (
                'citymunCode' => 153637,
                'citymunDesc' => 'BUMBARAN',
                'provCode' => 1536,
            ),
            431 => 
            array (
                'citymunCode' => 153638,
                'citymunDesc' => 'TAGOLOAN II',
                'provCode' => 1536,
            ),
            432 => 
            array (
                'citymunCode' => 153639,
                'citymunDesc' => 'KAPATAGAN',
                'provCode' => 1536,
            ),
            433 => 
            array (
                'citymunCode' => 153640,
                'citymunDesc' => 'SULTAN DUMALONDONG',
                'provCode' => 1536,
            ),
            434 => 
            array (
                'citymunCode' => 153641,
                'citymunDesc' => 'LUMBACA-UNAYAN',
                'provCode' => 1536,
            ),
            435 => 
            array (
                'citymunCode' => 153801,
                'citymunDesc' => 'AMPATUAN',
                'provCode' => 1538,
            ),
            436 => 
            array (
                'citymunCode' => 153802,
                'citymunDesc' => 'BULDON',
                'provCode' => 1538,
            ),
            437 => 
            array (
                'citymunCode' => 153803,
                'citymunDesc' => 'BULUAN',
                'provCode' => 1538,
            ),
            438 => 
            array (
                'citymunCode' => 153805,
                'citymunDesc' => 'DATU PAGLAS',
                'provCode' => 1538,
            ),
            439 => 
            array (
                'citymunCode' => 153806,
                'citymunDesc' => 'DATU PIANG',
                'provCode' => 1538,
            ),
            440 => 
            array (
                'citymunCode' => 153807,
            'citymunDesc' => 'DATU ODIN SINSUAT (DINAIG)',
                'provCode' => 1538,
            ),
            441 => 
            array (
                'citymunCode' => 153808,
            'citymunDesc' => 'SHARIFF AGUAK (MAGANOY) (Capital)',
                'provCode' => 1538,
            ),
            442 => 
            array (
                'citymunCode' => 153809,
                'citymunDesc' => 'MATANOG',
                'provCode' => 1538,
            ),
            443 => 
            array (
                'citymunCode' => 153810,
                'citymunDesc' => 'PAGALUNGAN',
                'provCode' => 1538,
            ),
            444 => 
            array (
                'citymunCode' => 153811,
                'citymunDesc' => 'PARANG',
                'provCode' => 1538,
            ),
            445 => 
            array (
                'citymunCode' => 153812,
            'citymunDesc' => 'SULTAN KUDARAT (NULING)',
                'provCode' => 1538,
            ),
            446 => 
            array (
                'citymunCode' => 153813,
            'citymunDesc' => 'SULTAN SA BARONGIS (LAMBAYONG)',
                'provCode' => 1538,
            ),
            447 => 
            array (
                'citymunCode' => 153814,
            'citymunDesc' => 'KABUNTALAN (TUMBAO)',
                'provCode' => 1538,
            ),
            448 => 
            array (
                'citymunCode' => 153815,
                'citymunDesc' => 'UPI',
                'provCode' => 1538,
            ),
            449 => 
            array (
                'citymunCode' => 153816,
                'citymunDesc' => 'TALAYAN',
                'provCode' => 1538,
            ),
            450 => 
            array (
                'citymunCode' => 153817,
                'citymunDesc' => 'SOUTH UPI',
                'provCode' => 1538,
            ),
            451 => 
            array (
                'citymunCode' => 153818,
                'citymunDesc' => 'BARIRA',
                'provCode' => 1538,
            ),
            452 => 
            array (
                'citymunCode' => 153819,
                'citymunDesc' => 'GEN. S. K. PENDATUN',
                'provCode' => 1538,
            ),
            453 => 
            array (
                'citymunCode' => 153820,
                'citymunDesc' => 'MAMASAPANO',
                'provCode' => 1538,
            ),
            454 => 
            array (
                'citymunCode' => 153821,
                'citymunDesc' => 'TALITAY',
                'provCode' => 1538,
            ),
            455 => 
            array (
                'citymunCode' => 153822,
                'citymunDesc' => 'PAGAGAWAN',
                'provCode' => 1538,
            ),
            456 => 
            array (
                'citymunCode' => 153823,
                'citymunDesc' => 'PAGLAT',
                'provCode' => 1538,
            ),
            457 => 
            array (
                'citymunCode' => 153824,
                'citymunDesc' => 'SULTAN MASTURA',
                'provCode' => 1538,
            ),
            458 => 
            array (
                'citymunCode' => 153825,
                'citymunDesc' => 'GUINDULUNGAN',
                'provCode' => 1538,
            ),
            459 => 
            array (
                'citymunCode' => 153826,
                'citymunDesc' => 'DATU SAUDI-AMPATUAN',
                'provCode' => 1538,
            ),
            460 => 
            array (
                'citymunCode' => 153827,
                'citymunDesc' => 'DATU UNSAY',
                'provCode' => 1538,
            ),
            461 => 
            array (
                'citymunCode' => 153828,
                'citymunDesc' => 'DATU ABDULLAH SANGKI',
                'provCode' => 1538,
            ),
            462 => 
            array (
                'citymunCode' => 153829,
                'citymunDesc' => 'RAJAH BUAYAN',
                'provCode' => 1538,
            ),
            463 => 
            array (
                'citymunCode' => 153830,
                'citymunDesc' => 'DATU BLAH T. SINSUAT',
                'provCode' => 1538,
            ),
            464 => 
            array (
                'citymunCode' => 153831,
                'citymunDesc' => 'DATU ANGGAL MIDTIMBANG',
                'provCode' => 1538,
            ),
            465 => 
            array (
                'citymunCode' => 153832,
                'citymunDesc' => 'MANGUDADATU',
                'provCode' => 1538,
            ),
            466 => 
            array (
                'citymunCode' => 153833,
                'citymunDesc' => 'PANDAG',
                'provCode' => 1538,
            ),
            467 => 
            array (
                'citymunCode' => 153834,
                'citymunDesc' => 'NORTHERN KABUNTALAN',
                'provCode' => 1538,
            ),
            468 => 
            array (
                'citymunCode' => 153835,
                'citymunDesc' => 'DATU HOFFER AMPATUAN',
                'provCode' => 1538,
            ),
            469 => 
            array (
                'citymunCode' => 153836,
                'citymunDesc' => 'DATU SALIBO',
                'provCode' => 1538,
            ),
            470 => 
            array (
                'citymunCode' => 153837,
                'citymunDesc' => 'SHARIFF SAYDONA MUSTAPHA',
                'provCode' => 1538,
            ),
            471 => 
            array (
                'citymunCode' => 156601,
                'citymunDesc' => 'INDANAN',
                'provCode' => 1566,
            ),
            472 => 
            array (
                'citymunCode' => 156602,
            'citymunDesc' => 'JOLO (Capital)',
                'provCode' => 1566,
            ),
            473 => 
            array (
                'citymunCode' => 156603,
                'citymunDesc' => 'KALINGALAN CALUANG',
                'provCode' => 1566,
            ),
            474 => 
            array (
                'citymunCode' => 156604,
                'citymunDesc' => 'LUUK',
                'provCode' => 1566,
            ),
            475 => 
            array (
                'citymunCode' => 156605,
                'citymunDesc' => 'MAIMBUNG',
                'provCode' => 1566,
            ),
            476 => 
            array (
                'citymunCode' => 156606,
            'citymunDesc' => 'HADJI PANGLIMA TAHIL (MARUNGGAS)',
                'provCode' => 1566,
            ),
            477 => 
            array (
                'citymunCode' => 156607,
                'citymunDesc' => 'OLD PANAMAO',
                'provCode' => 1566,
            ),
            478 => 
            array (
                'citymunCode' => 156608,
                'citymunDesc' => 'PANGUTARAN',
                'provCode' => 1566,
            ),
            479 => 
            array (
                'citymunCode' => 156609,
                'citymunDesc' => 'PARANG',
                'provCode' => 1566,
            ),
            480 => 
            array (
                'citymunCode' => 156610,
                'citymunDesc' => 'PATA',
                'provCode' => 1566,
            ),
            481 => 
            array (
                'citymunCode' => 156611,
                'citymunDesc' => 'PATIKUL',
                'provCode' => 1566,
            ),
            482 => 
            array (
                'citymunCode' => 156612,
                'citymunDesc' => 'SIASI',
                'provCode' => 1566,
            ),
            483 => 
            array (
                'citymunCode' => 156613,
                'citymunDesc' => 'TALIPAO',
                'provCode' => 1566,
            ),
            484 => 
            array (
                'citymunCode' => 156614,
                'citymunDesc' => 'TAPUL',
                'provCode' => 1566,
            ),
            485 => 
            array (
                'citymunCode' => 156615,
                'citymunDesc' => 'TONGKIL',
                'provCode' => 1566,
            ),
            486 => 
            array (
                'citymunCode' => 156616,
            'citymunDesc' => 'PANGLIMA ESTINO (NEW PANAMAO)',
                'provCode' => 1566,
            ),
            487 => 
            array (
                'citymunCode' => 156617,
                'citymunDesc' => 'LUGUS',
                'provCode' => 1566,
            ),
            488 => 
            array (
                'citymunCode' => 156618,
                'citymunDesc' => 'PANDAMI',
                'provCode' => 1566,
            ),
            489 => 
            array (
                'citymunCode' => 156619,
                'citymunDesc' => 'OMAR',
                'provCode' => 1566,
            ),
            490 => 
            array (
                'citymunCode' => 157001,
            'citymunDesc' => 'PANGLIMA SUGALA (BALIMBING)',
                'provCode' => 1570,
            ),
            491 => 
            array (
                'citymunCode' => 157002,
            'citymunDesc' => 'BONGAO (Capital)',
                'provCode' => 1570,
            ),
            492 => 
            array (
                'citymunCode' => 157003,
            'citymunDesc' => 'MAPUN (CAGAYAN DE TAWI-TAWI)',
                'provCode' => 1570,
            ),
            493 => 
            array (
                'citymunCode' => 157004,
                'citymunDesc' => 'SIMUNUL',
                'provCode' => 1570,
            ),
            494 => 
            array (
                'citymunCode' => 157005,
                'citymunDesc' => 'SITANGKAI',
                'provCode' => 1570,
            ),
            495 => 
            array (
                'citymunCode' => 157006,
                'citymunDesc' => 'SOUTH UBIAN',
                'provCode' => 1570,
            ),
            496 => 
            array (
                'citymunCode' => 157007,
                'citymunDesc' => 'TANDUBAS',
                'provCode' => 1570,
            ),
            497 => 
            array (
                'citymunCode' => 157008,
                'citymunDesc' => 'TURTLE ISLANDS',
                'provCode' => 1570,
            ),
            498 => 
            array (
                'citymunCode' => 157009,
                'citymunDesc' => 'LANGUYAN',
                'provCode' => 1570,
            ),
            499 => 
            array (
                'citymunCode' => 157010,
                'citymunDesc' => 'SAPA-SAPA',
                'provCode' => 1570,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'citymunCode' => 157011,
                'citymunDesc' => 'SIBUTU',
                'provCode' => 1570,
            ),
            1 => 
            array (
                'citymunCode' => 160201,
                'citymunDesc' => 'BUENAVISTA',
                'provCode' => 1602,
            ),
            2 => 
            array (
                'citymunCode' => 160202,
            'citymunDesc' => 'BUTUAN CITY (Capital)',
                'provCode' => 1602,
            ),
            3 => 
            array (
                'citymunCode' => 160203,
                'citymunDesc' => 'CITY OF CABADBARAN',
                'provCode' => 1602,
            ),
            4 => 
            array (
                'citymunCode' => 160204,
                'citymunDesc' => 'CARMEN',
                'provCode' => 1602,
            ),
            5 => 
            array (
                'citymunCode' => 160205,
                'citymunDesc' => 'JABONGA',
                'provCode' => 1602,
            ),
            6 => 
            array (
                'citymunCode' => 160206,
                'citymunDesc' => 'KITCHARAO',
                'provCode' => 1602,
            ),
            7 => 
            array (
                'citymunCode' => 160207,
                'citymunDesc' => 'LAS NIEVES',
                'provCode' => 1602,
            ),
            8 => 
            array (
                'citymunCode' => 160208,
                'citymunDesc' => 'MAGALLANES',
                'provCode' => 1602,
            ),
            9 => 
            array (
                'citymunCode' => 160209,
                'citymunDesc' => 'NASIPIT',
                'provCode' => 1602,
            ),
            10 => 
            array (
                'citymunCode' => 160210,
                'citymunDesc' => 'SANTIAGO',
                'provCode' => 1602,
            ),
            11 => 
            array (
                'citymunCode' => 160211,
                'citymunDesc' => 'TUBAY',
                'provCode' => 1602,
            ),
            12 => 
            array (
                'citymunCode' => 160212,
                'citymunDesc' => 'REMEDIOS T. ROMUALDEZ',
                'provCode' => 1602,
            ),
            13 => 
            array (
                'citymunCode' => 160301,
                'citymunDesc' => 'CITY OF BAYUGAN',
                'provCode' => 1603,
            ),
            14 => 
            array (
                'citymunCode' => 160302,
                'citymunDesc' => 'BUNAWAN',
                'provCode' => 1603,
            ),
            15 => 
            array (
                'citymunCode' => 160303,
                'citymunDesc' => 'ESPERANZA',
                'provCode' => 1603,
            ),
            16 => 
            array (
                'citymunCode' => 160304,
                'citymunDesc' => 'LA PAZ',
                'provCode' => 1603,
            ),
            17 => 
            array (
                'citymunCode' => 160305,
                'citymunDesc' => 'LORETO',
                'provCode' => 1603,
            ),
            18 => 
            array (
                'citymunCode' => 160306,
            'citymunDesc' => 'PROSPERIDAD (Capital)',
                'provCode' => 1603,
            ),
            19 => 
            array (
                'citymunCode' => 160307,
                'citymunDesc' => 'ROSARIO',
                'provCode' => 1603,
            ),
            20 => 
            array (
                'citymunCode' => 160308,
                'citymunDesc' => 'SAN FRANCISCO',
                'provCode' => 1603,
            ),
            21 => 
            array (
                'citymunCode' => 160309,
                'citymunDesc' => 'SAN LUIS',
                'provCode' => 1603,
            ),
            22 => 
            array (
                'citymunCode' => 160310,
                'citymunDesc' => 'SANTA JOSEFA',
                'provCode' => 1603,
            ),
            23 => 
            array (
                'citymunCode' => 160311,
                'citymunDesc' => 'TALACOGON',
                'provCode' => 1603,
            ),
            24 => 
            array (
                'citymunCode' => 160312,
                'citymunDesc' => 'TRENTO',
                'provCode' => 1603,
            ),
            25 => 
            array (
                'citymunCode' => 160313,
                'citymunDesc' => 'VERUELA',
                'provCode' => 1603,
            ),
            26 => 
            array (
                'citymunCode' => 160314,
                'citymunDesc' => 'SIBAGAT',
                'provCode' => 1603,
            ),
            27 => 
            array (
                'citymunCode' => 166701,
                'citymunDesc' => 'ALEGRIA',
                'provCode' => 1667,
            ),
            28 => 
            array (
                'citymunCode' => 166702,
                'citymunDesc' => 'BACUAG',
                'provCode' => 1667,
            ),
            29 => 
            array (
                'citymunCode' => 166704,
                'citymunDesc' => 'BURGOS',
                'provCode' => 1667,
            ),
            30 => 
            array (
                'citymunCode' => 166706,
                'citymunDesc' => 'CLAVER',
                'provCode' => 1667,
            ),
            31 => 
            array (
                'citymunCode' => 166707,
                'citymunDesc' => 'DAPA',
                'provCode' => 1667,
            ),
            32 => 
            array (
                'citymunCode' => 166708,
                'citymunDesc' => 'DEL CARMEN',
                'provCode' => 1667,
            ),
            33 => 
            array (
                'citymunCode' => 166710,
                'citymunDesc' => 'GENERAL LUNA',
                'provCode' => 1667,
            ),
            34 => 
            array (
                'citymunCode' => 166711,
                'citymunDesc' => 'GIGAQUIT',
                'provCode' => 1667,
            ),
            35 => 
            array (
                'citymunCode' => 166714,
                'citymunDesc' => 'MAINIT',
                'provCode' => 1667,
            ),
            36 => 
            array (
                'citymunCode' => 166715,
                'citymunDesc' => 'MALIMONO',
                'provCode' => 1667,
            ),
            37 => 
            array (
                'citymunCode' => 166716,
                'citymunDesc' => 'PILAR',
                'provCode' => 1667,
            ),
            38 => 
            array (
                'citymunCode' => 166717,
                'citymunDesc' => 'PLACER',
                'provCode' => 1667,
            ),
            39 => 
            array (
                'citymunCode' => 166718,
                'citymunDesc' => 'SAN BENITO',
                'provCode' => 1667,
            ),
            40 => 
            array (
                'citymunCode' => 166719,
            'citymunDesc' => 'SAN FRANCISCO (ANAO-AON)',
                'provCode' => 1667,
            ),
            41 => 
            array (
                'citymunCode' => 166720,
                'citymunDesc' => 'SAN ISIDRO',
                'provCode' => 1667,
            ),
            42 => 
            array (
                'citymunCode' => 166721,
            'citymunDesc' => 'SANTA MONICA (SAPAO)',
                'provCode' => 1667,
            ),
            43 => 
            array (
                'citymunCode' => 166722,
                'citymunDesc' => 'SISON',
                'provCode' => 1667,
            ),
            44 => 
            array (
                'citymunCode' => 166723,
                'citymunDesc' => 'SOCORRO',
                'provCode' => 1667,
            ),
            45 => 
            array (
                'citymunCode' => 166724,
            'citymunDesc' => 'SURIGAO CITY (Capital)',
                'provCode' => 1667,
            ),
            46 => 
            array (
                'citymunCode' => 166725,
                'citymunDesc' => 'TAGANA-AN',
                'provCode' => 1667,
            ),
            47 => 
            array (
                'citymunCode' => 166727,
                'citymunDesc' => 'TUBOD',
                'provCode' => 1667,
            ),
            48 => 
            array (
                'citymunCode' => 166801,
                'citymunDesc' => 'BAROBO',
                'provCode' => 1668,
            ),
            49 => 
            array (
                'citymunCode' => 166802,
                'citymunDesc' => 'BAYABAS',
                'provCode' => 1668,
            ),
            50 => 
            array (
                'citymunCode' => 166803,
                'citymunDesc' => 'CITY OF BISLIG',
                'provCode' => 1668,
            ),
            51 => 
            array (
                'citymunCode' => 166804,
                'citymunDesc' => 'CAGWAIT',
                'provCode' => 1668,
            ),
            52 => 
            array (
                'citymunCode' => 166805,
                'citymunDesc' => 'CANTILAN',
                'provCode' => 1668,
            ),
            53 => 
            array (
                'citymunCode' => 166806,
                'citymunDesc' => 'CARMEN',
                'provCode' => 1668,
            ),
            54 => 
            array (
                'citymunCode' => 166807,
                'citymunDesc' => 'CARRASCAL',
                'provCode' => 1668,
            ),
            55 => 
            array (
                'citymunCode' => 166808,
                'citymunDesc' => 'CORTES',
                'provCode' => 1668,
            ),
            56 => 
            array (
                'citymunCode' => 166809,
                'citymunDesc' => 'HINATUAN',
                'provCode' => 1668,
            ),
            57 => 
            array (
                'citymunCode' => 166810,
                'citymunDesc' => 'LANUZA',
                'provCode' => 1668,
            ),
            58 => 
            array (
                'citymunCode' => 166811,
                'citymunDesc' => 'LIANGA',
                'provCode' => 1668,
            ),
            59 => 
            array (
                'citymunCode' => 166812,
                'citymunDesc' => 'LINGIG',
                'provCode' => 1668,
            ),
            60 => 
            array (
                'citymunCode' => 166813,
                'citymunDesc' => 'MADRID',
                'provCode' => 1668,
            ),
            61 => 
            array (
                'citymunCode' => 166814,
                'citymunDesc' => 'MARIHATAG',
                'provCode' => 1668,
            ),
            62 => 
            array (
                'citymunCode' => 166815,
                'citymunDesc' => 'SAN AGUSTIN',
                'provCode' => 1668,
            ),
            63 => 
            array (
                'citymunCode' => 166816,
                'citymunDesc' => 'SAN MIGUEL',
                'provCode' => 1668,
            ),
            64 => 
            array (
                'citymunCode' => 166817,
                'citymunDesc' => 'TAGBINA',
                'provCode' => 1668,
            ),
            65 => 
            array (
                'citymunCode' => 166818,
                'citymunDesc' => 'TAGO',
                'provCode' => 1668,
            ),
            66 => 
            array (
                'citymunCode' => 166819,
            'citymunDesc' => 'CITY OF TANDAG (Capital)',
                'provCode' => 1668,
            ),
            67 => 
            array (
                'citymunCode' => 168501,
            'citymunDesc' => 'BASILISA (RIZAL)',
                'provCode' => 1685,
            ),
            68 => 
            array (
                'citymunCode' => 168502,
                'citymunDesc' => 'CAGDIANAO',
                'provCode' => 1685,
            ),
            69 => 
            array (
                'citymunCode' => 168503,
                'citymunDesc' => 'DINAGAT',
                'provCode' => 1685,
            ),
            70 => 
            array (
                'citymunCode' => 168504,
            'citymunDesc' => 'LIBJO (ALBOR)',
                'provCode' => 1685,
            ),
            71 => 
            array (
                'citymunCode' => 168505,
                'citymunDesc' => 'LORETO',
                'provCode' => 1685,
            ),
            72 => 
            array (
                'citymunCode' => 168506,
            'citymunDesc' => 'SAN JOSE (Capital)',
                'provCode' => 1685,
            ),
            73 => 
            array (
                'citymunCode' => 168507,
                'citymunDesc' => 'TUBAJON',
                'provCode' => 1685,
            ),
            74 => 
            array (
                'citymunCode' => 174001,
            'citymunDesc' => 'BOAC (Capital)',
                'provCode' => 1740,
            ),
            75 => 
            array (
                'citymunCode' => 174002,
                'citymunDesc' => 'BUENAVISTA',
                'provCode' => 1740,
            ),
            76 => 
            array (
                'citymunCode' => 174003,
                'citymunDesc' => 'GASAN',
                'provCode' => 1740,
            ),
            77 => 
            array (
                'citymunCode' => 174004,
                'citymunDesc' => 'MOGPOG',
                'provCode' => 1740,
            ),
            78 => 
            array (
                'citymunCode' => 174005,
                'citymunDesc' => 'SANTA CRUZ',
                'provCode' => 1740,
            ),
            79 => 
            array (
                'citymunCode' => 174006,
                'citymunDesc' => 'TORRIJOS',
                'provCode' => 1740,
            ),
            80 => 
            array (
                'citymunCode' => 175101,
                'citymunDesc' => 'ABRA DE ILOG',
                'provCode' => 1751,
            ),
            81 => 
            array (
                'citymunCode' => 175102,
                'citymunDesc' => 'CALINTAAN',
                'provCode' => 1751,
            ),
            82 => 
            array (
                'citymunCode' => 175103,
                'citymunDesc' => 'LOOC',
                'provCode' => 1751,
            ),
            83 => 
            array (
                'citymunCode' => 175104,
                'citymunDesc' => 'LUBANG',
                'provCode' => 1751,
            ),
            84 => 
            array (
                'citymunCode' => 175105,
                'citymunDesc' => 'MAGSAYSAY',
                'provCode' => 1751,
            ),
            85 => 
            array (
                'citymunCode' => 175106,
            'citymunDesc' => 'MAMBURAO (Capital)',
                'provCode' => 1751,
            ),
            86 => 
            array (
                'citymunCode' => 175107,
                'citymunDesc' => 'PALUAN',
                'provCode' => 1751,
            ),
            87 => 
            array (
                'citymunCode' => 175108,
                'citymunDesc' => 'RIZAL',
                'provCode' => 1751,
            ),
            88 => 
            array (
                'citymunCode' => 175109,
                'citymunDesc' => 'SABLAYAN',
                'provCode' => 1751,
            ),
            89 => 
            array (
                'citymunCode' => 175110,
                'citymunDesc' => 'SAN JOSE',
                'provCode' => 1751,
            ),
            90 => 
            array (
                'citymunCode' => 175111,
                'citymunDesc' => 'SANTA CRUZ',
                'provCode' => 1751,
            ),
            91 => 
            array (
                'citymunCode' => 175201,
                'citymunDesc' => 'BACO',
                'provCode' => 1752,
            ),
            92 => 
            array (
                'citymunCode' => 175202,
                'citymunDesc' => 'BANSUD',
                'provCode' => 1752,
            ),
            93 => 
            array (
                'citymunCode' => 175203,
                'citymunDesc' => 'BONGABONG',
                'provCode' => 1752,
            ),
            94 => 
            array (
                'citymunCode' => 175204,
            'citymunDesc' => 'BULALACAO (SAN PEDRO)',
                'provCode' => 1752,
            ),
            95 => 
            array (
                'citymunCode' => 175205,
            'citymunDesc' => 'CITY OF CALAPAN (Capital)',
                'provCode' => 1752,
            ),
            96 => 
            array (
                'citymunCode' => 175206,
                'citymunDesc' => 'GLORIA',
                'provCode' => 1752,
            ),
            97 => 
            array (
                'citymunCode' => 175207,
                'citymunDesc' => 'MANSALAY',
                'provCode' => 1752,
            ),
            98 => 
            array (
                'citymunCode' => 175208,
                'citymunDesc' => 'NAUJAN',
                'provCode' => 1752,
            ),
            99 => 
            array (
                'citymunCode' => 175209,
                'citymunDesc' => 'PINAMALAYAN',
                'provCode' => 1752,
            ),
            100 => 
            array (
                'citymunCode' => 175210,
                'citymunDesc' => 'POLA',
                'provCode' => 1752,
            ),
            101 => 
            array (
                'citymunCode' => 175211,
                'citymunDesc' => 'PUERTO GALERA',
                'provCode' => 1752,
            ),
            102 => 
            array (
                'citymunCode' => 175212,
                'citymunDesc' => 'ROXAS',
                'provCode' => 1752,
            ),
            103 => 
            array (
                'citymunCode' => 175213,
                'citymunDesc' => 'SAN TEODORO',
                'provCode' => 1752,
            ),
            104 => 
            array (
                'citymunCode' => 175214,
                'citymunDesc' => 'SOCORRO',
                'provCode' => 1752,
            ),
            105 => 
            array (
                'citymunCode' => 175215,
                'citymunDesc' => 'VICTORIA',
                'provCode' => 1752,
            ),
            106 => 
            array (
                'citymunCode' => 175301,
                'citymunDesc' => 'ABORLAN',
                'provCode' => 1753,
            ),
            107 => 
            array (
                'citymunCode' => 175302,
                'citymunDesc' => 'AGUTAYA',
                'provCode' => 1753,
            ),
            108 => 
            array (
                'citymunCode' => 175303,
                'citymunDesc' => 'ARACELI',
                'provCode' => 1753,
            ),
            109 => 
            array (
                'citymunCode' => 175304,
                'citymunDesc' => 'BALABAC',
                'provCode' => 1753,
            ),
            110 => 
            array (
                'citymunCode' => 175305,
                'citymunDesc' => 'BATARAZA',
                'provCode' => 1753,
            ),
            111 => 
            array (
                'citymunCode' => 175306,
                'citymunDesc' => 'BROOKE\'S POINT',
                'provCode' => 1753,
            ),
            112 => 
            array (
                'citymunCode' => 175307,
                'citymunDesc' => 'BUSUANGA',
                'provCode' => 1753,
            ),
            113 => 
            array (
                'citymunCode' => 175308,
                'citymunDesc' => 'CAGAYANCILLO',
                'provCode' => 1753,
            ),
            114 => 
            array (
                'citymunCode' => 175309,
                'citymunDesc' => 'CORON',
                'provCode' => 1753,
            ),
            115 => 
            array (
                'citymunCode' => 175310,
                'citymunDesc' => 'CUYO',
                'provCode' => 1753,
            ),
            116 => 
            array (
                'citymunCode' => 175311,
                'citymunDesc' => 'DUMARAN',
                'provCode' => 1753,
            ),
            117 => 
            array (
                'citymunCode' => 175312,
            'citymunDesc' => 'EL NIDO (BACUIT)',
                'provCode' => 1753,
            ),
            118 => 
            array (
                'citymunCode' => 175313,
                'citymunDesc' => 'LINAPACAN',
                'provCode' => 1753,
            ),
            119 => 
            array (
                'citymunCode' => 175314,
                'citymunDesc' => 'MAGSAYSAY',
                'provCode' => 1753,
            ),
            120 => 
            array (
                'citymunCode' => 175315,
                'citymunDesc' => 'NARRA',
                'provCode' => 1753,
            ),
            121 => 
            array (
                'citymunCode' => 175316,
            'citymunDesc' => 'PUERTO PRINCESA CITY (Capital)',
                'provCode' => 1753,
            ),
            122 => 
            array (
                'citymunCode' => 175317,
                'citymunDesc' => 'QUEZON',
                'provCode' => 1753,
            ),
            123 => 
            array (
                'citymunCode' => 175318,
                'citymunDesc' => 'ROXAS',
                'provCode' => 1753,
            ),
            124 => 
            array (
                'citymunCode' => 175319,
                'citymunDesc' => 'SAN VICENTE',
                'provCode' => 1753,
            ),
            125 => 
            array (
                'citymunCode' => 175320,
                'citymunDesc' => 'TAYTAY',
                'provCode' => 1753,
            ),
            126 => 
            array (
                'citymunCode' => 175321,
                'citymunDesc' => 'KALAYAAN',
                'provCode' => 1753,
            ),
            127 => 
            array (
                'citymunCode' => 175322,
                'citymunDesc' => 'CULION',
                'provCode' => 1753,
            ),
            128 => 
            array (
                'citymunCode' => 175323,
            'citymunDesc' => 'RIZAL (MARCOS)',
                'provCode' => 1753,
            ),
            129 => 
            array (
                'citymunCode' => 175324,
                'citymunDesc' => 'SOFRONIO ESPAÑOLA',
                'provCode' => 1753,
            ),
            130 => 
            array (
                'citymunCode' => 175901,
                'citymunDesc' => 'ALCANTARA',
                'provCode' => 1759,
            ),
            131 => 
            array (
                'citymunCode' => 175902,
                'citymunDesc' => 'BANTON',
                'provCode' => 1759,
            ),
            132 => 
            array (
                'citymunCode' => 175903,
                'citymunDesc' => 'CAJIDIOCAN',
                'provCode' => 1759,
            ),
            133 => 
            array (
                'citymunCode' => 175904,
                'citymunDesc' => 'CALATRAVA',
                'provCode' => 1759,
            ),
            134 => 
            array (
                'citymunCode' => 175905,
                'citymunDesc' => 'CONCEPCION',
                'provCode' => 1759,
            ),
            135 => 
            array (
                'citymunCode' => 175906,
                'citymunDesc' => 'CORCUERA',
                'provCode' => 1759,
            ),
            136 => 
            array (
                'citymunCode' => 175907,
                'citymunDesc' => 'LOOC',
                'provCode' => 1759,
            ),
            137 => 
            array (
                'citymunCode' => 175908,
                'citymunDesc' => 'MAGDIWANG',
                'provCode' => 1759,
            ),
            138 => 
            array (
                'citymunCode' => 175909,
                'citymunDesc' => 'ODIONGAN',
                'provCode' => 1759,
            ),
            139 => 
            array (
                'citymunCode' => 175910,
            'citymunDesc' => 'ROMBLON (Capital)',
                'provCode' => 1759,
            ),
            140 => 
            array (
                'citymunCode' => 175911,
                'citymunDesc' => 'SAN AGUSTIN',
                'provCode' => 1759,
            ),
            141 => 
            array (
                'citymunCode' => 175912,
                'citymunDesc' => 'SAN ANDRES',
                'provCode' => 1759,
            ),
            142 => 
            array (
                'citymunCode' => 175913,
                'citymunDesc' => 'SAN FERNANDO',
                'provCode' => 1759,
            ),
            143 => 
            array (
                'citymunCode' => 175914,
                'citymunDesc' => 'SAN JOSE',
                'provCode' => 1759,
            ),
            144 => 
            array (
                'citymunCode' => 175915,
                'citymunDesc' => 'SANTA FE',
                'provCode' => 1759,
            ),
            145 => 
            array (
                'citymunCode' => 175916,
                'citymunDesc' => 'FERROL',
                'provCode' => 1759,
            ),
            146 => 
            array (
                'citymunCode' => 175917,
            'citymunDesc' => 'SANTA MARIA (IMELDA)',
                'provCode' => 1759,
            ),
        ));
        
        
    }
}
