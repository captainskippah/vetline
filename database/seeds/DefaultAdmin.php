<?php

use App\Models\Auth\Employee;
use Illuminate\Database\Seeder;

class DefaultAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	Employee::insert([
			'password'  => bcrypt('password'),
			'username'  => 'admin',
			'firstname' => 'John',
			'lastname'  => 'Doe'
     	]);   
    }
}
