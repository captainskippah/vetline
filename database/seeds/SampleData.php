<?php

use App\Models\Address;
use App\Models\Appointment;
use App\Models\Auth\Employee;
use App\Models\Cage;
use App\Models\Customer;
use App\Models\Medical;
use App\Models\Pet;
use App\Models\Phone;
use App\Models\Procedure;
use App\Models\Treatment;
use App\Models\Vaccine;
use Illuminate\Database\Seeder;

class SampleData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            $this->call(DefaultAdmin::class);

            // Insert default procedures
            Procedure::insert([
                ['name' => 'CBC'],
                ['name' => 'Blood chemistry'],
                ['name' => 'Skin test'],
                ['name' => 'Skin smear'],
                ['name' => 'Direct fecal smear'],
                ['name' => 'Ear swab']
            ]);

            // Insert default treatments
            Treatment::insert([
                ['name' => 'Nail clipping'],
                ['name' => 'Ear cleaning']
            ]);

            // Create Customer data and its relationships
            factory(Customer::class, 100)->create()->each(function ($customer) {
                factory(Pet::class, rand(0,5))->create(['customer_id' => $customer->id]);
                factory(Address::class)->create(['customer_id' => $customer->id]);
                factory(Phone::class, rand(1,2))->create(['customer_id' => $customer->id]);
            });

            // Random pet to be caged
            Pet::inRandomOrder()->limit(20)->get()->each(function ($pet) {
                if (rand(0,1)) {
                    factory(Cage::class)->states('boarded')->create([
                        'pet_id' => $pet->id
                    ]);
                } else {
                    factory(Cage::class)->create();
                }
            });

            // Random pet to have medical records
            $user_id =  Employee::first()->id;
            Pet::inRandomOrder()->limit(100)->get()->each(function ($pet) use ($user_id) {
                if (rand(0,1)) {
                    factory(Medical::class, rand(5,10))->create(['author_id' => $user_id, 'pet_id' => $pet->id]);
                }

                if (rand(0,1)) {
                    factory(Appointment::class)->create(['pet_id' => $pet->id]);
                }
            });

            // Random medical records with vaccines
            Medical::inRandomOrder()->limit(50)->get()->each(function ($medical) {
                factory(Vaccine::class, rand(0,3))->create(['medical_id' => $medical->id]);
            });

            // Random medical records with procedures
            Medical::inRandomOrder()->limit(50)->get()->each(function ($medical) {
                $procedures = Procedure::inRandomOrder()->limit(rand(1,3))->get();
                $medical->procedures()->saveMany($procedures);
            });

            // Random medical records with treatments
            Medical::inRandomOrder()->limit(50)->get()->each(function ($medical) {
                $treatments = Treatment::inRandomOrder()->limit(rand(1,2))->get();
                $medical->treatments()->saveMany($treatments);
            });
        });
    }
}
