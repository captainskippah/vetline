<?php

use App\Models\Appointment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 10);
        });

        $this->insertInitialStatusCodes();

        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pet_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->string('notes');
            $table->dateTime('start_time');
            $table->dateTime('end_time')->nullable();
            $table->softDeletes();

            $table->foreign('pet_id')
                ->references('id')->on('pets');

            $table->foreign('status_id')
                ->references('id')->on('appointment_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
        Schema::dropIfExists('appointment_status');
    }

    protected function insertInitialStatusCodes()
    {
        DB::table('appointment_status')->insert(array_map(function ($v){
            return [
                'id' => array_search($v, Appointment::STATUS),
                'name' => $v
            ];
        }, Appointment::STATUS));
    }
}
