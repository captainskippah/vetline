<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id')->unsigned();
            $table->integer('pet_id')->unsigned();
            $table->string('complaint')->nullable();
            $table->string('history')->nullable();
            $table->text('observation')->nullable();
            $table->string('diagnosis')->nullable();
            $table->date('date');
            $table->softDeletes();

            $table->foreign('author_id')
                ->references('id')->on('employees');

            $table->foreign('pet_id')
                ->references('id')->on('pets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicals');
    }
}
