<?php

use App\Models\Cage;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cage_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        $this->insertInitialStatusCodes();

        Schema::create('cages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pet_id')->nullable()->unsigned();
            $table->integer('customer_id')->nullable()->unsigned();
            $table->integer('status_id')->unsigned();
            $table->datetime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('pet_id')
                ->references('id')->on('pets');

            $table->foreign('customer_id')
                ->references('id')->on('customers');

            $table->foreign('status_id')
                ->references('id')->on('cage_status');                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cages');
        Schema::dropIfExists('cage_status');  
    }

    protected function insertInitialStatusCodes()
    {
        DB::table('cage_status')->insert(array_map(function ($k, $v) {
            return [
                'id' => $k,
                'name' => $v
            ];
        }, array_keys(Cage::STATUS), Cage::STATUS));
    }
}
