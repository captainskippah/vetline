<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangays', function (Blueprint $table) {
            $table->increments('brgyCode')->unsigned();
            $table->string('brgyDesc', 50);
            $table->integer('citymunCode')->unsigned();

            $table->foreign('citymunCode')
                ->references('citymunCode')
                ->on('cities')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });

        Artisan::call('db:seed', [
            '--class' => 'BarangaysTableSeeder',
            '--force' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangays');
    }
}
