<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalTreatmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_treatment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('medical_id')->unsigned();
            $table->integer('treatment_id')->unsigned();

            $table->foreign('medical_id')
                ->references('id')->on('medicals');

            $table->foreign('treatment_id')
                ->references('id')->on('treatments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_treatment');
    }
}
