<?php

namespace Tests\Unit\Appointment;

use Carbon\Carbon;
use Tests\TestCase;
use App\Models\Appointment;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CheckDateTime extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_return_false_if_datetime_exists()
	{
		$appt = factory(Appointment::class)->create();

		$this->assertFalse(Appointment::checkDateTime($appt->start_time));
	}

	/** @test */
	public function it_should_return_true_if_datetime_is_available()
	{
		$appt = factory(Appointment::class)->create();

		$new_time = (new Carbon($appt->start_time))->addMinutes(10)->toAtomString();

		$this->assertTrue(Appointment::checkDateTime($new_time));
	}
}
