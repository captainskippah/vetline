<?php

namespace Tests\Unit\Cage;

use App\Models\Pet;
use Tests\TestCase;
use App\Models\Cage;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReserveCageTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_board_an_available_cage_for_a_pet()
	{
		$pet = factory(Pet::class)->create();

		$cage = factory(Cage::class)->create();

		$this->assertTrue($cage->board($pet->id));
		$this->assertEquals($cage->pet_id, $pet->id);
	}

	/** @test */
	public function it_should_board_a_reserved_cage_for_a_pet()
	{
		$pet = factory(Pet::class)->create();

		$cage = factory(Cage::class)->states('reserved')->create();

		$this->assertTrue($cage->board($pet->id));
		$this->assertTrue($cage->pet_id === $pet->id);
		$this->assertTrue($cage->status_id === Cage::BOARDED);
	}

	/** @test */
	public function it_should_not_board_a_boarded_cage()
	{
		$pet = factory(Pet::class)->create();

		$cage = factory(Cage::class)->states('boarded')->create();

		$this->expectException(ValidationException::class);
		$cage->board($pet->id);
	}

	/** @test */
	public function it_should_not_board_without_pet_id()
	{
		$cage = factory(Cage::class)->create();

		$this->expectException(ValidationException::class);
		$cage->board('');

		$this->expectException(ValidationException::class);
		$cage->board(null);
	}

	/** @test */
	public function it_should_not_board_with_an_invalid_pet_id()
	{
		$cage = factory(Cage::class)->create();

		$this->expectException(ValidationException::class);
		$cage->board(-1);
	}
}
