<?php

namespace Tests\Unit\Cage;

use Tests\TestCase;
use App\Models\Cage;
use App\Models\Customer;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReserveCageTest_ extends TestCase
{
	use DatabaseTransactions;

	protected $user;

	public function setUp()
	{
		parent::setUp();

		$this->user = factory(Customer::class)->create();
		$this->actingAs($this->user);
	}

	/** @test */
	public function it_should_reserve_available_cage()
	{
		$cage = factory(Cage::class)->create();

		$this->assertTrue($cage->reserve($this->user->id));
		$this->assertNull($cage->pet_id);
		$this->assertTrue($cage->customer_id === $this->user->id);
		$this->assertTrue($cage->status_id === Cage::RESERVED);
	}

	/** @test */
	public function it_should_not_reserve_boarded_cage()
	{
		$cage = factory(Cage::class)->states('boarded')->create();

		$this->expectException(ValidationException::class);
		$cage->reserve($this->user->id);
	}

	/** @test */
	public function it_should_not_reseve_reserved_cage()
	{
		$cage = factory(Cage::class)->states('reserved')->create();

		$this->expectException(ValidationException::class);
		$cage->reserve($this->user->id);
	}

	/** @test */
	public function it_should_not_reserve_without_customer_id()
	{
		$cage = factory(Cage::class)->create();

		$this->expectException(ValidationException::class);
		$cage->reserve('');

		$this->expectException(ValidationException::class);
		$cage->reserve(null);
	}

	/** @test */
	public function it_should_not_reserve_with_nonexisting_customer_id()
	{
		$cage = factory(Cage::class)->create();

		$this->expectException(ValidationException::class);
		$cage->reserve(-1);
	}
}
