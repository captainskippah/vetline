<?php

namespace Tests\Unit\Cage;

use App\Models\Pet;
use Tests\TestCase;
use App\Models\Cage;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VacateCageTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_vacate_a_boarded_cage()
	{
		$cage = factory(Cage::class)->states('boarded')->create();

		$this->assertTrue($cage->vacate());
		$this->assertNull($cage->pet_id);
		$this->assertTrue($cage->status_id === Cage::AVAILABLE);
	}

	/** @test */
	public function it_should_vacate_a_reserved_cage()
	{
		$cage = factory(Cage::class)->states('reserved')->create();

		$this->assertTrue($cage->vacate());
		$this->assertNull($cage->pet_id);
		$this->assertTrue($cage->status_id === Cage::AVAILABLE);
	}
}
