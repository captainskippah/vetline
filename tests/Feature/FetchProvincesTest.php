<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Province;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FetchProvincesTest extends TestCase
{
	use DatabaseTransactions;

	public function setUp()
    {
    	parent::setUp();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');
    }

	/** @test */
	public function it_should_fetch_provinces()
	{
		$provinces = Province::get();

		$this->get('/api/v1/provinces')
			->assertExactJson($provinces->toArray());
	}

	/** @test */
	public function it_should_fetch_a_province()
	{
		$province = Province::first();

		$this->get('/api/v1/provinces/' . $province->provCode)
			->assertExactJson($province->toArray());
	}

	/** @test */
	public function it_should_return_404_when_not_found()
	{
		$this->get('/api/v1/provinces/123456789')
			->assertStatus(404);
	}
}
