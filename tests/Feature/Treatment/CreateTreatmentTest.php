<?php

namespace Tests\Feature\Treatment;

use Tests\TestCase;
use App\Models\Treatment;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateTreatmentTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_create_a_treatment()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$treatment = factory(Treatment::class)->make()->toArray();

		$old_treatment_count = Treatment::count();

		$this->json('POST', '/api/v1/treatments', $treatment)
			->assertJsonStructure(['id', 'name'])
			->assertJsonFragment($treatment);

		$this->assertEquals($old_treatment_count + 1, Treatment::count());
	}
}
