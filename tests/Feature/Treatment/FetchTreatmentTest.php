<?php

namespace Tests\Feature\Treatment;

use Tests\TestCase;
use App\Models\Treatment;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FetchTreatmentTest extends TestCase
{
    use DatabaseTransactions;

	/** @test */
	public function it_should_fetch_treatments()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$res = $this->json('GET', '/api/v1/treatments');

		$this->assertCount(Treatment::count(), $res->getOriginalContent());
	}
}
