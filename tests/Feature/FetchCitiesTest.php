<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\City;
use App\Models\Province;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FetchCitiesTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
    	parent::setUp();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');
    }

	/** @test */
	public function it_should_fetch_cities_of_a_province()
	{
		$province = Province::inRandomOrder()->first();

		$cities = City::where(['provCode' => $province->provCode])->get();

		$this->get('/api/v1/provinces/' . $province->provCode . '/cities')
			->assertExactJson($cities->toArray());
	}

	/** @test */
	public function it_should_fetch_a_city()
	{
		$city = City::first();

		$this->get('/api/v1/cities/' . $city->citymunCode)
			->assertExactJson($city->toArray());
	}

	/** @test */
	public function it_should_return_404_when_not_found()
	{
		$this->get('/api/v1/cities/123456789')
			->assertStatus(404);
	}
}
