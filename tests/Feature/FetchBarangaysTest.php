<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\City;
use App\Models\Barangay;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FetchBarangaysTest extends TestCase
{
	use DatabaseTransactions;

	public function setUp()
	{
		parent::setUp();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');
	}

	/** @test */
	public function it_should_fetch_barangays_of_a_city()
	{
		$city = City::inRandomOrder()->first();

		$barangays = Barangay::where(['citymunCode' => $city->citymunCode])->get();

		$this->get("/api/v1/cities/{$city->citymunCode}/barangays")
			->assertExactJson($barangays->toArray());
	}

	/** @test */
	public function it_should_fetch_a_barangay()
	{
		$barangay = Barangay::first();

		$this->get('/api/v1/barangays/' . $barangay->brgyCode)
			->assertExactJson($barangay->toArray());
	}

	/** @test */
	public function it_should_return_404_when_not_found()
	{
		$this->get('/api/v1/barangays/123456789')
			->assertStatus(404);
	}
}
