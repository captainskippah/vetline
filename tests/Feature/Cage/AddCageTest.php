<?php

namespace Tests\Feature\Cage;

use Tests\TestCase;
use App\Models\Cage;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AddCageTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_add_a_cage()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$old_count = Cage::count();

		$this->post('/api/v1/cages/add');

		$this->assertEquals($old_count + 1, Cage::count());
	}
}
