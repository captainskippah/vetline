<?php

namespace Tests\Feature\Cage;

use Tests\TestCase;
use App\Models\Cage;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VacateCageTest extends TestCase
{
	use DatabaseTransactions;

	public function setUp()
	{
		parent::setUp();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');
	}

	/** @test */
	public function it_should_vacate_a_boarded_cage()
	{
		$cage = factory(Cage::class)->states('boarded')->create();

		$res = $this->put("/api/v1/cages/vacant/{$cage->id}");

		// Expect cage is now available
		$cage->status_id = Cage::AVAILABLE;

		$expected = Cage::transform($cage);

		$res->assertJson($expected);
	}
}
