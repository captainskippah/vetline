<?php

namespace Tests\Feature\Cage;

use Tests\TestCase;
use App\Models\Cage;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeleteCageTest extends TestCase
{
	use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        $this->actingAs(factory(Employee::class)->create(), 'api_employee');
    }

    /** @test */
    public function it_should_delete_a_cage_by_id()
    {
    	$this->disableExceptionHandling();

    	$cage = factory(Cage::class)->create();

        $old_count = Cage::count();

    	$this->delete('/api/v1/cages/' . $cage->id);

    	$this->assertEquals($old_count - 1, Cage::count());
    }

    /** @test */
    public function it_should_return_error_if_id_is_not_found()
    {
        $this->disableExceptionHandling();

    	$this->delete('/api/v1/cages/999')
    		->assertStatus(404);
    }
}
