<?php

namespace Tests\Feature\Cage;

use Tests\TestCase;
use App\Models\Cage;
use App\Models\Customer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReserveCageTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_reseve_the_first_available_cage()
	{
		$customer = factory(Customer::class)->create();

		$this->actingAs($customer, 'api_customer');

		$cage = factory(Cage::class)->create();

		$res = $this->post("/api/v1/cages/reserve", [
			'customer_id' => $customer->id
		]);

		// Factory does not set deleted_at
		$customer->deleted_at = null;

		// Expect cage is reserved by now
		$cage->setRelation('customer', $customer);
		$cage->status_id = Cage::RESERVED;

		$expected = Cage::transform($cage);

		$res->assertJson($expected);
	}

    /** @test */
    public function it_should_return_error_when_no_available_cages()
    {
        $customer = factory(Customer::class)->create();

        $cage = factory(Cage::class)->states('reserved')->create();

        $res = $this->post("/api/v1/cages/reserve", [
            'customer_id' => $customer->id
        ]);

        $res->assertStatus(422);
    }

    /** @test */
    public function it_should_return_error_when_boarding_without_customer_id()
    {
        $cage = factory(Cage::class)->create();

        $res = $this->post("/api/v1/cages/reserve");

        $res->assertStatus(422);
    }
}
