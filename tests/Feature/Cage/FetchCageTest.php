<?php

namespace Tests\Feature\Cage;

use Tests\TestCase;
use App\Models\Cage;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FetchCageTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_fetch_all_cages()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$response = $this->get('/api/v1/cages');

		$this->assertEquals(count(json_decode($response->content())), Cage::count());
	}

	/** @test */
	public function it_should_fetch_cages_by_status()
	{
		$this->disableExceptionHandling();

		$status = array_random(Cage::STATUS);

		factory(Cage::class, 5)->create();
		factory(Cage::class, 5)->states('boarded')->create();
		factory(Cage::class, 5)->states('reserved')->create();

		$res = $this->get("/api/v1/cages/?status={$status}");

		// Check if all fetched cages' status is same as $status
		$passed = array_reduce($res->getData(true), function ($prev, $cage) use ($status) {
			return (is_null($prev) || $prev === false) ?: $cage['status'] === $status;
		}, null);

		$this->assertTrue($passed);
	}
}
