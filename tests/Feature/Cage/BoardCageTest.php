<?php

namespace Tests\Feature\Cage;

use App\Models\Pet;
use Tests\TestCase;
use App\Models\Cage;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BoadCageTest extends TestCase
{
	use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        $this->actingAs(factory(Employee::class)->create(), 'api_employee');
    }

    /** @test */
    public function it_should_board_the_first_unboarded_cage()
    {
        $pet = factory(Pet::class)->create();

    	$cage = factory(Cage::class, 2)->create();

        $res = $this->post("/api/v1/cages/board", [
            'pet_id' => $pet->id,
        ]);

        // expect cage was boarded by now
        $cage->first()->status_id = Cage::BOARDED;

        $cage->first()->setRelation('pet', $pet);

        $expected = Cage::transform($cage->first());

        $res->assertJson($expected);
    }

    /** @test */
    public function it_should_return_error_when_no_available_cages()
    {
        $pet = factory(Pet::class)->create();

        $cage = factory(Cage::class)->states('boarded')->create();

        $res = $this->post("/api/v1/cages/board", [
            'pet_id' => $pet->id
        ]);

        $res->assertStatus(422);
    }

    /** @test */
    public function it_should_return_error_when_boarding_with_boarded_pet()
    {
        factory(Cage::class)->create();

        $cage = factory(Cage::class)->states('boarded')->create();

        $res = $this->post("/api/v1/cages/board", [
            'pet_id' => $cage->pet->id
        ]);

        $res->assertStatus(422);
    }

    /** @test */
    public function it_should_return_error_when_boarding_without_pet_id()
    {
        $cage = factory(Cage::class)->create();

        $res = $this->post("/api/v1/cages/board", [
            'cage_id' => $cage->id,
        ]);

        $res->assertStatus(422);
    }
}
