<?php

namespace Tests\Feature\Pet;

use App\Models\Pet;
use Tests\TestCase;
use App\Models\Customer;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FetchPetTest extends TestCase
{
	use DatabaseTransactions;

	public function setUp()
	{
		parent::setUp();
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		factory(Pet::class, 10)->create();
	}

	/** @test */
	public function it_should_fetch_a_pet_by_id()
	{
		$pet = factory(Pet::class)->create();

		$this->get('/api/v1/pets/'.$pet->id)
			->assertJson(Pet::transform($pet));
	}
}
