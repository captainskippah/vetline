<?php

namespace Tests\Feature\Pet;

use App\Models\Pet;
use Tests\TestCase;
use App\Models\Customer;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreatePetTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_create_pet_record()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$customer = factory(Customer::class)->create();

		$pet = factory(Pet::class)->make(['customer_id' => $customer->id])->toArray();

		$this->post("/api/v1/pets", $pet);

		$this->assertTrue($customer->pets()->count() === 1);
	}
}
