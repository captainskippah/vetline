<?php

namespace Tests\Feature\Pet;

use App\Models\Pet;
use Tests\TestCase;
use App\Models\Customer;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UpdatePetTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_update_a_pet()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$pet = factory(Pet::class)->create();

		$new_pet = factory(Pet::class)->make(['customer_id' => $pet->customer_id]);

		$this->put('/api/v1/pets/' . $pet->id, array_merge($pet->toArray(), $new_pet->toArray()));

		$this->assertDatabaseHas('pets', $new_pet->toArray());
	}
}
