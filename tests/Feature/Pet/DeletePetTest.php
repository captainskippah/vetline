<?php

namespace Tests\Feature\Pet;

use App\Models\Pet;
use Tests\TestCase;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeletePetTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_delete_a_pet()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$pet = factory(Pet::class)->create();
	
		$this->delete('/api/v1/pets/' . $pet->id);

		$this->assertSoftDeleted('pets', [
			'id' => $pet->id
		]);
	}
}
