<?php

namespace Tests\Feature\Appointment;

use Tests\TestCase;
use App\Models\Appointment;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeleteAppointmentTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_delete_appointment()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$appointment = factory(Appointment::class)->create();

		$this->json('DELETE', "/api/v1/appointments/{$appointment->id}");

		$this->assertNull(Appointment::find($appointment->id));
	}
}
