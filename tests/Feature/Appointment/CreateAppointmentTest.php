<?php

namespace Tests\Feature\Appointments;

use Carbon\Carbon;
use App\Models\Pet;
use Tests\TestCase;
use App\Models\Appointment;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateAppointmentTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_create_an_appointment()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$count_before = Appointment::count();

		$pet = factory(Pet::class)->create();

		$appointment = factory(Appointment::class)->make([
			'pet_id' => $pet->id
		])->toArray();

		$this->json('POST', '/api/v1/appointments', $this->transform($appointment))
			->assertStatus(201)
			->assertJsonStructure([
				'id',
				'notes',
				'start_time',
				'end_time',
				'status',
				'pet'
			]);

		$this->assertEquals($count_before + 1, Appointment::count());
	}

	private function transform(array $appt)
	{
		$carbon = new Carbon($appt['start_time']);

		return [
			'date' => $carbon->format('Y-m-d'),
			'time' => $carbon->format('h:i a'),
			'notes' => $appt['notes'],
			'pet_id' => $appt['pet_id']
		];
	}
}
