<?php

namespace Tests\Feature\Appointment;

use Tests\TestCase;
use App\Models\Appointment;
use App\Models\Auth\Employee;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ChangeStatusTest extends TestCase
{
	use DatabaseTransactions;

	public function setUp()
	{
		parent::setUp();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');
	}

	/** @test */
	public function it_should_be_able_to_change_status()
	{
		$this->disableExceptionHandling();

		$appointment = factory(Appointment::class)->create(['status' => Appointment::STATUS[0]]);

		$status = Appointment::STATUS[rand(1,2)];

		$this->json('PUT', "/api/v1/appointments/{$appointment->id}/status", [
			'status' => $status
		]);

		$this->assertEquals($status, Appointment::find($appointment->id)->status);
	}

	/** @test */
	public function it_should_set_end_time_to_now_if_status_changed_to_completed()
	{
		$appointment = factory(Appointment::class)->create();

		$this->json('PUT', "/api/v1/appointments/{$appointment->id}/status", [
			'status' => 'completed'
		]);

		$dtNow = (new \DateTime())->format(\DateTime::ATOM);
		$this->assertEquals($dtNow, Appointment::find($appointment->id)->end_time);
	}

	/** @test */
	public function it_should_set_end_time_to_now_if_status_changed_to_cancelled()
	{
		$appointment = factory(Appointment::class)->create();

		$this->json('PUT', "/api/v1/appointments/{$appointment->id}/status", [
			'status' => 'completed'
		]);

		$dtNow = (new \DateTime())->format(\DateTime::ATOM);
		$this->assertEquals($dtNow, Appointment::find($appointment->id)->end_time);
	}
}
