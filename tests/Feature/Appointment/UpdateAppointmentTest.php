<?php

namespace Tests\Feature\Appointment;

use App\Models\Pet;
use Tests\TestCase;
use App\Models\Appointment;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UpdateAppointmentTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_update_an_appointment()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$appointment = factory(Appointment::class)->create();

		$pet = factory(Pet::class)->create();

		// Use same status as the original
		// because we have a dedicated endpoint for
		// updating the status
		$appointment_new = factory(Appointment::class)->make([
			'pet_id' => $pet->id,
			'status' => $appointment->status
		]);

		$data = array_merge($appointment->toArray(), $appointment_new->toArray());
		$data = $this->transform($data);

		$this->json('PUT', "/api/v1/appointments/{$appointment->id}", $data);

		$newData = Appointment::find($appointment->id)->toArray();
		$newData = $this->transform($newData);

		$this->assertEquals($data, $newData);
	}

	private function transform(array $data)
	{
		$carbon = new \Datetime($data['start_time']);

		return [
			'id' => $data['id'],
			'date' => $carbon->format('Y-m-d'),
			'time' => $carbon->format('h:i a'),
			'notes' => $data['notes'],
			'status' => $data['status'],
			'pet_id' => $data['pet_id']
		];
	}
}
