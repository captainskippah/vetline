<?php

namespace Tests\Feature\Appointment;

use Faker\Factory;
use Tests\TestCase;
use App\Models\Appointment;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FetchAppointmentTest extends TestCase
{
	use DatabaseTransactions;

	public function setUp()
	{
		parent::setUp();

		$this->disableExceptionHandling();

		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$faker = Factory::create();

		// Old appointments
		factory(Appointment::class, 5)->create([
			'start_time' => $faker->dateTimeBetween('-2 months', '-1 month')->format(\Datetime::ATOM)
		]);

		// Appointments this month
		factory(Appointment::class, 5)->create([
			'start_time' => $faker->dateTimeBetween('now', 'now')->format(\Datetime::ATOM)
		]);

		// Future appointments
		factory(Appointment::class, 5)->create([
			'start_time' => $faker->dateTimeBetween('+1 month', '+2 months')->format(\Datetime::ATOM)
		]);
	}

	/** @test */
	public function it_should_fetch_all_appointments_of_current_month_by_default()
	{
		$monthNow = (new \DateTime())->format('F');
		$response = $this->json('GET', '/api/v1/appointments')->getOriginalContent();

		$this->assertTrue(count($response) > 0);
		
		$passed = true;

		foreach ($response as $appt) {
			$month = (new \DateTime($appt['start_time']))->format('F');

			if ($month !== $monthNow) {
				$passed = false;
				break;
			}
		}

		$this->assertTrue($passed);
	}

	/** @test */
	public function it_should_fetch_appointments_of_the_given_month()
	{
		$faker = Factory::create();

		$date = $faker->dateTimeBetween('-2 months', '+2 months');

		$month = $date->format('n');

		// Generate appointment on random date generated
		// in case factories on setUp did not make one.
		factory(Appointment::class)->create([
			'start_time' => $date->format(\Datetime::ATOM)
		]);

		$response = $this->json('GET', '/api/v1/appointments/?month=' . $month)->getOriginalContent();

 		$this->assertTrue(count($response) > 0);

		$passed = true;

		foreach ($response as $appt) {
			$month2 = (new \DateTime($appt['start_time']))->format('n');

			if ($month !== $month2) {
				$passed = false;
				break;
			}
		}

		$this->assertTrue($passed);
	}

	/** @test */
	public function it_should_fetch_appointments_with_pet()
	{
		$this->json('GET', '/api/v1/appointments')
			->assertJsonStructure([['pet']]);
	}
}
