<?php

namespace Tests\Feature\Procedures;

use Tests\TestCase;
use App\Models\Procedure;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateProcedureTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_create_a_procedure()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$procedure = factory(Procedure::class)->make()->toArray();

		$old_procedure_count = Procedure::count();

		$this->json('POST', '/api/v1/procedures', $procedure)
			->assertJsonStructure(['id', 'name'])
			->assertJsonFragment($procedure);

		$this->assertEquals($old_procedure_count + 1, Procedure::count());
	}
}
