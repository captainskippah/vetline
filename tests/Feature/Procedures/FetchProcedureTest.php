<?php

namespace Tests\Feature\Procedures;

use Tests\TestCase;
use App\Models\Procedure;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FetchProcedureTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_fetch_procedures()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$res = $this->json('GET', '/api/v1/procedures');

		$this->assertCount(Procedure::count(), $res->getOriginalContent());
	}
}
