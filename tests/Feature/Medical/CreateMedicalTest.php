<?php

namespace Tests\Feature\Medical;

use App\Models\Pet;
use Tests\TestCase;
use App\Models\Medical;
use App\Models\Vaccine;
use App\Models\Procedure;
use App\Models\Treatment;
use App\Models\Auth\Employee;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateMedicalTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_create_a_medical_record()
	{
		$this->disableExceptionHandling();

		$user = factory(Employee::class)->create();
		$pet = factory(Pet::class)->create();

		// Make medical record without 'author_id'
		// because it will be generated upon submission
		$form_data = factory(Medical::class)->make(['author_id' => '', 'pet_id' => $pet->id])->toArray();

		// Create vaccines without 'medical_id' because we are creating one
		$vaccines = factory(Vaccine::class, rand(0,3))->make(['medical_id' => '']);
		$form_data['vaccines'] = array_map([$this, 'transformVaccine'], $vaccines->toArray());

		$procedures = factory(Procedure::class, rand(0,3))->create();
		$form_data['procedures'] = array_pluck($procedures->toArray(), 'id');

		$treatments = factory(Treatment::class, rand(0,3))->create();
		$form_data['treatments'] = array_pluck($treatments->toArray(), 'id');

		$expected = array_merge(
			array_keys($form_data),
			['pet', 'author']
		);

		// We expect 'author' not 'author_id'
		unset($expected[array_search('author_id', $expected)]);

		// We expect 'pet' not 'pet_id'
		unset($expected[array_search('pet_id', $expected)]);

		$token = JWTAuth::fromUser($user);
		$this->json('POST', "/api/v1/medicals", $form_data, ['Authorization' => "Bearer {$token}"])
			->assertJsonStructure($expected);
	}

	protected function transformVaccine(array $v)
	{
		return [
			'name' => $v['name'],
			'next' => $v['next']
		];
	}
}
