<?php

namespace Tests\Feature\Medical;

use App\Models\Pet;
use Tests\TestCase;
use App\Models\Medical;
use App\Models\Vaccine;
use App\Models\Procedure;
use App\Models\Auth\Employee;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UpdateMedicalTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_remove_procedures_not_in_request()
	{
		$this->disableExceptionHandling();

		$medical = factory(Medical::class)->create();
		$user = Employee::find($medical->author_id);

		// Start with 3 procedures
		$procedures = factory(Procedure::class, 3)->create()->toArray();
		$medical->procedures()->attach(array_pluck($procedures, ['id']));

		// Remove 2 procedures
		array_splice($procedures, 1);

		$form_data = array_merge(
			$this->transform($medical->toArray()),
			['procedures' => array_pluck($procedures, ['id'])]
		);

		$assumed_response = array_merge(
			$this->transform($medical->toArray()),
			['procedures' => $procedures]
		);

		$token = JWTAuth::fromUser($user);
		$this->json('PUT', '/api/v1/medicals/' . $medical->id, $form_data, ['Authorization' => "Bearer {$token}"])
			->assertJson($assumed_response);
	}

	/** @test */
	public function it_should_add_new_procedures()
	{
		$this->disableExceptionHandling();

		$medical = factory(Medical::class)->create();
		$user = Employee::find($medical->author_id);

		// Start with 1 procedures
		$procedure = factory(Procedure::class)->create()->toArray();
		$medical->procedures()->attach(array_only($procedure, ['id']));

		// Create 2 new procedures
		$new_procedures = factory(Procedure::class, 2)->create()->toArray();

		$form_data = array_merge(
			$this->transform($medical->toArray()),
			['procedures' => array_pluck(array_merge([$procedure], $new_procedures), ['id'])]
		);

		$assumed_response = array_merge(
			$this->transform($medical->toArray()),
			['procedures' => array_merge([$procedure], $new_procedures)]
		);

		$token = JWTAuth::fromUser($user);
		$this->json('PUT', '/api/v1/medicals/' . $medical->id, $form_data, ['Authorization' => "Bearer {$token}"])
			->assertJson($assumed_response);
	}

	/** @test */
	public function it_should_remove_vaccines_not_in_request()
	{
		$this->disableExceptionHandling();

		$medical = factory(Medical::class)->create();
		$user = Employee::find($medical->author_id);

		// Start with 5 vaccines
		$vaccines = factory(Vaccine::class, 5)->create(['medical_id' => $medical->id])->toArray();

		// Remove 2 of them
		array_splice($vaccines, 3);

		// Data to be submitted
		$new_data = array_merge($medical->toArray(), [
			'vaccines' => $vaccines
		]);

		// Load medical's relationship for assertion
		$medical->load([]);

		$token = JWTAuth::fromUser($user);
		$res = $this->json('PUT', 'api/v1/medicals/' . $medical->id, $new_data, ['Authorization' => "Bearer {$token}"]);

		// Re-assign the new data because
		// we now loaded the relations
		// that is needed for assertion
		$new_data = array_merge($medical->toArray(), [
			'vaccines' => $vaccines
		]);

		$res->assertJson($this->transform($new_data));
	}

	/** @test */
	public function it_should_add_vaccines_previously_not_in_records()
	{
		$this->disableExceptionHandling();

		$medical = factory(Medical::class)->create();
		$user = Employee::find($medical->author_id);

		// Start with 2 vaccines
		$vaccines = factory(Vaccine::class, 2)->create(['medical_id' => $medical->id])->toArray();

		// Create another 3 of them
		$new_vaccines = factory(Vaccine::class, 3)->make(['medical_id' => ''])->toArray();

		// Data to be submitted
		$form_data = array_merge(
			$medical->toArray(),
			['vaccines' => array_merge($vaccines, $new_vaccines)]
		);

		$assumed_response = array_merge(
			$this->transform($medical->toArray()),
			['vaccines' => array_map([$this, 'transformVaccine'], array_merge($vaccines, $new_vaccines))]
		);

		$token = JWTAuth::fromUser($user);
		$this->json('PUT', 'api/v1/medicals/' . $medical->id, $form_data, ['Authorization' => "Bearer {$token}"])
			->assertJson($assumed_response);
	}

	protected function transform(array $data)
	{
		return [
			'complaint' => $data['complaint'],
			'history' => $data['history'],
			'observation' => $data['observation'],
			'diagnosis' => $data['diagnosis'],
			'date' => (new \Datetime($data['date']))->format('M d, Y')
		];
	}

	protected function transformVaccine(array $v)
	{
		return [
			'name' => $v['name'],
			'next' => $v['next']
		];
	}
}
