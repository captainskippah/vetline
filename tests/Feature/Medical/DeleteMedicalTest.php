<?php

namespace Tests\Feature\Medical;

use Tests\TestCase;
use App\Models\Medical;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeleteMedicalTest extends TestCase
{
	use WithoutMiddleware;
	use DatabaseTransactions;

	/** @test */
	public function it_should_soft_delete_a_medical_record()
	{
		$this->disableExceptionHandling();

		$medical = factory(Medical::class)->create();

		$this->json('DELETE', '/api/v1/medicals/' . $medical->id);

		$this->assertSoftDeleted('medicals', ['id' => $medical->id]);
	}
}
