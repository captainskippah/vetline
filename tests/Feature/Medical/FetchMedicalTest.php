<?php

namespace Tests\Feature\Medical;

use Tests\TestCase;
use App\Models\Medical;
use App\Models\Auth\Employee;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FetchMedicalTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_fetch_medical_records()
	{
		$user = factory(Employee::class)->create();

		$medicals = factory(Medical::class)->create();
		$medicals->load('pet');

		$token = JWTAuth::fromUser($user);
		$this->json('GET', '/api/v1/medicals', [], ['Authorization' => "Bearer {$token}"])
			->assertJson([Medical::transform($medicals)]);
	}
}
