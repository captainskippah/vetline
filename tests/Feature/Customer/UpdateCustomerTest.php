<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Phone;
use App\Models\Address;
use App\Models\Customer;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UpdateCustomerTest extends TestCase
{
	use DatabaseTransactions;

	public function setUp()
	{
		parent::setUp();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');
	}

	/** @test */
	public function it_should_update_a_customer()
	{
		$this->disableExceptionHandling();

		$customer = factory(Customer::class)->create();
		$address = factory(Address::class)->create(['customer_id' => $customer->id]);
		$phones = factory(Phone::class, rand(1,2))->create(['customer_id' => $customer->id]);

		$customer_new = array_merge(
			factory(Customer::class)->make()->toArray(),
			$address->toArray(),
			['phones' => $phones->pluck('number')->toArray()]
		);

		$this->json('PUT','/api/v1/customers/' . $customer->id, $customer_new);

		$this->assertEquals(
			array_only($customer_new, ['firstname', 'middlename', 'lastname']),
			array_only(Customer::find($customer->id)->toArray(), ['firstname', 'middlename', 'lastname'])
		);

	}

	/** @test */
	public function it_should_add_new_phone_numbers()
	{
		$customer = factory(Customer::class)->create();

		$address = factory(Address::class)->create(['customer_id' => $customer->id]);

		// Start with 1 phone
		$phones[] = factory(Phone::class)->create(['customer_id' => $customer->id])->number;

		// Add another 1 phone
		$phones[] = factory(Phone::class)->make(['customer_id' => $customer->id])->number;

		$res = $this->json('PUT', '/api/v1/customers/' . $customer->id, array_merge(
			$customer->toArray(),
			$address->toarray(),
			['phones' => $phones]
		));

		$this->assertEquals(2, $customer->phones()->count(), 'Result phone count is not equal to assumed phone count');
	}

	/** @test */
	public function it_should_remove_phone_number_not_in_request()
	{
		$this->disableExceptionHandling();

		$customer = factory(Customer::class)->create();
		$address = factory(Address::class)->create(['customer_id' => $customer->id]);
		$phones = factory(Phone::class, 2)->create(['customer_id' => $customer->id]);

		$this->put('/api/v1/customers/' . $customer->id, array_merge(
			$customer->toArray(),
			$address->toArray(),
			['phones' => $phones->pluck('number')->take(1)->toArray()]
		));

		$count = $customer->phones->count();

		$this->assertEquals(1, $count, 'Result phone count is not equal to assumed phone count');
	}

	/** @test */
	public function it_should_be_able_to_restore_an_archived_records()
	{
		$this->disableExceptionHandling();

		$customer = factory(Customer::class)->create();

		$address = factory(Address::class)->create(['customer_id' => $customer->id]);

		$phones = factory(Phone::class, rand(1,2))->create(['customer_id' => $customer->id]);

		$customer->delete();

		$response = $this->json('PUT', '/api/v1/customers/' . $customer->id, array_merge(
			$customer->toArray(),
			$address->toArray(),
			['phones' => $phones->pluck('number')->toArray()]
		));

		$customer->deleted_at = null;
		$customer->setRelation('address', $address);
		$customer->setRelation('phones', $phones);

		$expected = Customer::transform($customer);

		$this->assertEquals($expected, $response->getOriginalContent());
	}
}
