<?php

namespace Tests\Feature\Customer;

use Tests\TestCase;
use App\Models\Phone;
use App\Models\Customer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_login_a_customer_using_phone_and_password()
	{
		$this->disableExceptionHandling();

		$password = uniqid();

		$customer = factory(Customer::class)->create([
			'password' => bcrypt($password)
		]);

		$phone = factory(Phone::class)->create([
			'customer_id' => $customer->id
		]);

		$credentials = [
			'phone' => $phone->number,
			'password' => $password
		];

		$this->json('POST', '/api/v1/login', $credentials)
			->assertJsonStructure(['token']);
	}
}
