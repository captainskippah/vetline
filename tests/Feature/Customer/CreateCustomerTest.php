<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Phone;
use App\Models\Address;
use App\Models\Customer;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateCustomerTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_create_a_customer()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$customer = factory(Customer::class)->make();

		$phones = factory(Phone::class, rand(1,2))->make(['customer_id' => '']);

		$address = factory(Address::class)->make(['customer_id' => '']);

		$this->post('/api/v1/customers', array_merge(
			$customer->toArray(),
			$address->toArray(),
			['phones' => $phones->pluck('number')->toArray()]
		));

		$this->assertDatabaseHas('customers', [
			'firstname' => $customer->firstname,
			'middlename' => $customer->middlename,
			'lastname' => $customer->lastname
		]);
	}
}
