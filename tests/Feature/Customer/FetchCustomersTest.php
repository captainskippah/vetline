<?php

namespace Tests\Feature;

use App\Models\Pet;
use Tests\TestCase;
use App\Models\Phone;
use App\Models\Address;
use App\Models\Customer;
use App\Models\Auth\Employee;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FetchCustomersTest extends TestCase
{
	use DatabaseTransactions;

	public function setUp()
	{
		parent::setUp();
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');
	}

	/** @test */
	public function it_should_return_active_customers_by_default()
	{
		// This will create a customer also
		// but not vice-versa
		factory(Address::class, 50)->create();
		$response = $this->get('/api/v1/customers');

		$this->assertNotEmpty($response->getOriginalContent());
		$this->assertFalse($this->hasArchived($response->getData(true)));
	}

	/** @test */
	public function it_should_return_archived_customers()
	{
		$customer = factory(Address::class)->create()->customer;
		$customer->delete();

		$response = $this->json('GET', '/api/v1/customers?archived=true');

		$this->assertNotEmpty($response->getOriginalContent());
		$this->assertTrue($this->hasArchived($response->getData(true)));
	}

	/** @test */
	public function it_should_return_customer_by_id()
	{
		$customer = factory(Address::class)->create()->customer;

		$response = $this->get('/api/v1/customers/' . $customer->id)
			->assertJson(['id' => $customer->id]);
	}

	/** @test */
	public function it_should_return_customer_with_pets()
	{
		$customer = factory(Address::class)->create()->customer;

		$pets = $customer->pets->only(['id', 'name', 'breed', 'gender', 'birthday'])->toArray();

		$this->get('/api/v1/customers/' . $customer->id)
			->assertJson(['pets' => $pets]);
	}

	/** @test */
	public function it_should_return_customer_with_address()
	{
		$customer = factory(Customer::class)->create();
		$address = factory(Address::class)->create(['customer_id' => $customer->id]);

		$this->get('/api/v1/customers/' . $customer->id)
			->assertJson([
				'address' => [
					'street' => $address->street,
					'barangay_id' => $address->barangay_id,
					'barangay' => $address->barangay->brgyDesc
				]
			]);
	}

	/** @test */
	public function it_should_return_customer_with_phones()
	{
		$customer = factory(Address::class)->create()->customer;

		$phones = factory(Phone::class, rand(1,2))
			->create(['customer_id' => $customer->id])
			->pluck('number')
			->toArray();

		$this->get('/api/v1/customers/' . $customer->id)
			->assertJson(['phones' => $phones]);
	}

	private function hasArchived(array $data)
	{
		foreach($data as $customer) {
			if ($customer['archived']) {
				return true;
			}
		}

		return false;
	}
}
