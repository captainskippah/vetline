<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Customer;
use App\Models\Auth\Employee;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeleteCustomerTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function it_should_soft_delete_customer_by_id()
	{
		$this->disableExceptionHandling();
		$this->actingAs(factory(Employee::class)->create(), 'api_employee');

		$customer = factory(Customer::class)->create();

		$this->delete('/api/v1/customers/' . $customer->id);

		$this->assertSoftDeleted('customers', ['id' => $customer->id]);
	}

	/** @test */
	public function it_should_permanent_delete_customer_by_id()
	{
		$customer = factory(Customer::class)->create();

		$this->delete('/api/v1/customers/' . $customer->id, [
			'force' => true
		]);

		$this->assertDatabaseMissing('customers', ['id' => $customer->id]);
	}
}
