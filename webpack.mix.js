const mix = require('laravel-mix');


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({
  processCssUrls: false,

  cleanCss: {
    level: {
      1: {
        specialComments: 'none'
      }
    }
  },

  postCss: [
    require('postcss-discard-comments')({ removeAll: true })
  ],
});

mix
  .js('resources/assets/js/admin/login.js', 'public/js/admin/login.js')
  .js('resources/assets/js/admin/app.js', 'public/js/admin/app.js')
  .sass('resources/assets/sass/admin/default.scss', 'public/css/admin/default.css')

mix
  .js('resources/assets/js/client/index.js', 'public/js/index.js')
  .sass('resources/assets/sass/client/default.scss', 'public/css/default.css')

mix
  .vue()
  .sourceMaps()
