<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <link rel="stylesheet" type="text/css" href="/css/normalize.css">
    <title>Vetline Animal Clinic and Pet Shop</title>

	<!-- core CSS -->
    <link rel="stylesheet" type="text/css" href="/css/roboto.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/default.css">
</head>

<body class="mdc-typography">
    <div id="vue">
        <header class="mdc-toolbar mdc-toolbar--fixed" id="nav">
            <div class="mdc-toolbar__row">
                <div class="mdc-toolbar__section mdc-toolbar__section--align-start">
                    <span class="mdc-toolbar__title">Vetline</span>
                </div>
                <div class="mdc-toolbar__section mdc-toolbar__section--align-end">
                    <nav class="mdc-tab-bar">
                        <a href="#" class="mdc-tab">Home</a>
                        <a href="#services" class="mdc-tab">Services</a>
                        <a href="#app" class="mdc-tab">Download</a>
                        <a href="#schedule" class="mdc-tab">Schedule</a>
                        <a href="#contact" class="mdc-tab">Contact</a>
                    </nav>
                </div>
            </div>
        </header>

        <section id="landing" class="flex flex--vertical flex--center-h flex--center-v">
            <h1>Welcome to Vetline</h1>
            <p class="tagline">Vetline Animal Clinic and Pet Shop is dedicated to exemplary veterinary medicine and patient care, client service and employee well-being; while maintaining honesty, integrity, profitability and community involvement along the way!</p>
            <div class="overlay"></div>
        </section>

        <section id="services">
            <h2>Services We Offer</h2>
            <div class="services">
                <div class="service" id="service-groom">
                    <div class="service__image"></div>
                    <div class="service__primary">
                        <h3 class="service__title">Grooming</h3>
                        <p class="service__description">nail clipping, ear cleaning,</p>
                    </div>
                </div>

                <div class="service" id="service-boarding">
                    <div class="service__primary">
                        <h3 class="service__title">Boarding</h3>
                        <p class="service__description">confinement of pet.</p>
                    </div>
                    <div class="service__image"></div>
                </div>

                <div class="service" id="service-surgery">
                    <div class="service__image"></div>
                    <div class="service__primary">
                        <h3 class="service__title">Surgery</h3>
                        <p class="service__description">Spaying and neutering, Dental surgery, Injuries, Internal injuries, Diagnose, skin mear test, Direct fecal smear, Ear swab.</p>
                    </div>
                </div>

                <div class="service" id="service-laboratory">
                    <div class="service__primary">
                        <h3 class="service__title">Laboratory</h3>
                        <p class="service__description">X-ray, CBC, Blood Chemistry, Microscopic Exam, Ultrasound</p>
                    </div>
                    <div class="service__image"></div>
                </div>
            </div>
        </section>

        <section id="app" class="flex">
            <div class="flex flex--center-v">
                <img class="app__image" src="/images/vcare.png">
            </div>
            <div class="flex flex--vertical flex--center-v">
                <h2 class="app__title">Pet App Title Here</h2>
                <p class="app__description">With our Vetline Care app, you can now whatever you wanna do on your phone LOL this is just a sample text don't be mad xD</p>
                <a href="#" class="app__link">
                    <img src="/images/google-play-badge.png">
                </a>
            </div>
        </section>

        <section id="schedule" class="flex flex--vertical flex--center-v flex--center-h">
            <h2>Our Schedule</h2>

            <ul id="list-schedule" class="mdc-list">
                <li class="mdc-list-item schedule">
                    <div class="schedule__day">Monday</div>
                    <div class="schedule__time">08:00 AM - 06:30 PM</div>
                </li>

                <li class="mdc-list-item schedule">
                    <div class="schedule__day">Tuesday</div>
                    <div class="schedule__time">08:00 AM - 06:30 PM</div>
                </li>

                <li class="mdc-list-item schedule">
                    <div class="schedule__day">Wednesday</div>
                    <div class="schedule__time">08:00 AM - 06:30 PM</div>
                </li>

                <li class="mdc-list-item schedule">
                    <div class="schedule__day">Thursday</div>
                    <div class="schedule__time">08:00 AM - 06:30 PM</div>
                </li>

                <li class="mdc-list-item schedule">
                    <div class="schedule__day">Friday</div>
                    <div class="schedule__time">08:00 AM - 06:30 PM</div>
                </li>

                <li class="mdc-list-item schedule">
                    <div class="schedule__day">Saturday</div>
                    <div class="schedule__time">08:00 AM - 06:30 PM</div>
                </li>

                <li class="mdc-list-item schedule">
                    <div class="schedule__day">Sunday</div>
                    <div class="schedule__time">08:00 AM - 12:00 NN</div>
                </li>
            </ul>
        </section>

        <section id="veterinarians" class="flex flex--vertical flex--center-h">
            <h2>Our Veterinarians</h2>
            <div class="veterinarians">
                <div class="mdc-card veterinarian" id="vet-editha">
                    <div class="mdc-card__media"></div>
                    <div class="mdc-card__primary">
                        <h3 class="mdc-card__title">Editha</h3>
                    </div>
                </div>
                <div class="mdc-card veterinarian" id="vet-kristina">
                    <div class="mdc-card__media"></div>
                    <div class="mdc-card__primary">
                        <h3 class="mdc-card__title">Kristina</h3>
                    </div>
                </div>
                <div class="mdc-card veterinarian" id="vet-mia">
                    <div class="mdc-card__media"></div>
                    <div class="mdc-card__primary">
                        <h3 class="mdc-card__title">Mia</h3>
                    </div>
                </div>
                <div class="mdc-card veterinarian" id="vet-mich">
                    <div class="mdc-card__media"></div>
                    <div class="mdc-card__primary">
                        <h3 class="mdc-card__title">Michelle</h3>
                    </div>
                </div>
                <div class="mdc-card veterinarian" id="vet-myra">
                    <div class="mdc-card__media"></div>
                    <div class="mdc-card__primary">
                        <h3 class="mdc-card__title">Myra</h3>
                    </div>
                </div>
            </div>
        </section>

        <section id="contact" class="flex">
            <section id="contact-form">
                <contact-form />
            </section>
            <section id="location"></section>
        </section>
    </div>
</body>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8lrbIHuJfPOR7jjkX-qD73egeCvSqZAc"></script>
<script src="/js/gmaps.js"></script>
<script src="https://www.google.com/recaptcha/api.js?render=explicit"></script>
<script src="/js/index.js"></script>
</html>
