<!DOCTYPE html>
<html>
<head>
	<title>Vetline Clinic</title>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="/css/admin/default.css">
    <link rel="stylesheet" type="text/css" href="/css/roboto.css">
	<link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body class="mdc-typography">
	<div id="app"></div>
	<script src="/js/admin/app.js"></script>
</body>
</html>
