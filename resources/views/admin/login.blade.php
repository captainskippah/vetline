<!DOCTYPE html>
<html>
<head>
    <title>Vetline - Login</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <link rel="stylesheet" type="text/css" href="/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="/css/admin/default.css">
    <link rel="stylesheet" type="text/css" href="/css/roboto.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
</head>
<body class="mdc-typography">
    <div id="app"></div>
    <script src="/js/admin/login.js"></script>
</body>
</html>
