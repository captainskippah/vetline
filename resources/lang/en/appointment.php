<?php

$message = 'Good evening Mr/Ms :recipient, ';
$message .= 'this is a reminder that your pet :pet ';
$message .= 'has an appointment at :time for tomorrow at the Vetline. ';
$message .= 'Thank you.';

return [
	'sms' => $message
];
