import Vue from 'vue';
import VueMaterial from 'vue-material'
import ContactForm from './components/contact'
import axios from 'axios';

axios.defaults.headers.common = {
  'Accept': 'application/json',
  'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
  'X-Requested-With': 'XMLHttpRequest'
}

Vue.use(VueMaterial)

Vue.prototype.$http = axios

new Vue({
  el: '#vue',
  
  components: {
    ContactForm
  },

  mounted () {
    let gmaps = new GMaps({
        div: '#location',
        lat: 15.154354,
        lng: 120.589989
    });

    gmaps.addMarker({
        lat: 15.154354,
        lng: 120.589989,
        title: 'Vetline Animal Clinic & Petshop'
    })
  }
})
