import Vue from 'vue';
import axios from 'axios';
import App from './App.vue';
import router from './router';
import NProgress from 'vue-nprogress';
import VueMaterial from 'vue-material'

Vue.use(VueMaterial);
Vue.use(NProgress, { showSpinner: false });

axios.defaults.headers.common = {
	'Accept': 'application/json',
  'Authorization': 'Bearer ' + window.localStorage.getItem('token'),
	'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
	'X-Requested-With': 'XMLHttpRequest'
}

Vue.prototype.$http = axios

const nprogress = new NProgress({ parent: '.nprogress-container' })

new Vue({
	router,
	nprogress,
	el: '#app',
	render: h => h(App)
})
