import Vue from 'vue';
import axios from 'axios';
import Login from './Login.vue';

axios.defaults.headers.common = {
  'Accept': 'application/json',
  'X-Requested-With': 'XMLHttpRequest'
}

Vue.prototype.$http = axios;

new Vue({
	el: '#app',
	render: h => h(Login)
});
