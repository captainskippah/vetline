import axios from 'axios';
import {format} from 'date-fns';

const API = '/api/v1/appointments';

export class Appointment {
  static get statusCompleted () {
    return 'completed'
  }

  static get statusCancelled () {
    return 'cancelled'
  }

  static get statusScheduled () {
    return 'scheduled'
  }

  constructor () {
    this.appointments = []
  }

  edit (id, formData) {
    let original = this.appointments.find(appt => appt.id ===id)
    
    /**
     * Append `status` and `end_time` property to the `formData`
     * because the form does present these properties.
     *
     * `status` has a different API for changing it
     * `end_time` is server generated
     */
    Object.assign(formData, {
      status: original.status,
      end_time: original.end_time
    })

    return new Promise((resolve, reject) => {
      axios.put(`${API}/${id}`, formData).then(({ data }) => {
        Object.assign(original, this.transform(data))
        resolve()
      }).catch(({ response }) => {
        if (response.status === 422) {
          reject(response.data)
          return;
        }
      })
    })
  }

  create (formData) {
    return new Promise((resolve, reject) => {
      axios.post(API, formData).then(({ data }) => {
        this.appointments.push(this.transform(data))
        resolve()
      }).catch(({ response }) => {
        if (response.status === 422) {
          reject(response.data)
          return;
        }
      })
    })
  }

  fetch (month = (new Date).getMonth()) {
    return new Promise((resolve, reject) => {
      axios.get(API, {
        /**
         * Need to plus 1 because javascript's
         * month is 0-based.
         */
        params: { month: month + 1 }
      }).then(({ data }) => {
        this.appointments = data.map(this.transform);
        resolve();
      }).catch(() => {
        reject()
      })
    })
  }

  changeStatus (id, status) {
    return new Promise((resolve, reject) => {
      axios.put(`${API}/${id}/status`, {
        status: status
      }).then(() => {
        let appt = this.appointments.find(appt => appt.id === id)
        Object.assign(appt, { status: status })
        resolve()
      }).catch(() => {
        reject()
      })
    })
  }

  remove (id) {
    return new Promise((resolve, reject) => {
      axios.delete(`${API}/${id}`).then(() => {
        let index = this.appointments.findIndex(appt => appt.id === id)
        this.appointments.splice(index, 1)
        resolve()
      }).catch(() => {
        reject()
      })
    })
  }

  transform (appt) {
    return Object.assign({}, appt, {
      date: appt.start_time,
      start_time: format(new Date(appt.start_time), 'hh:mm A')
    })
  }
}

export default new Appointment()
