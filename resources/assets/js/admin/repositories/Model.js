import axios from 'axios';
import isEmpty from 'lodash/isEmpty';
import omit from 'lodash/omit';

export default class Model {

  static all() {
    return new Promise((resolve, reject) => {
      axios.get(this.API).then(({ data }) => {
        let models = data.map(model => {
          return new this(model);
        })
        resolve(models);
      }).catch((response) => {
        console.log(response)
        reject(response.data)
      })
    })
  }

  constructor(attributes) {
    if (! this.constructor.hasOwnProperty('API')) {
      throw new Error('A model must define its API')
    }

    if (! this.constructor.hasOwnProperty('attributes')) {
      throw new Error('A model must define its attributes');
    }

    this.exists = false;

    // Define properties of the model
    [this.getKeyName(), ...this.constructor.attributes].forEach(attr => {
      Object.defineProperty(this, attr, { value: null, writable: true, enumerable: true })
    })

    // Fill our model if parameter is not empty
    if (! isEmpty(attributes)) {
      Object.assign(this, attributes)
      if (attributes.hasOwnProperty(this.getKeyName()) &&
        attributes[this.getKeyName()]) {
        this.exists = true
      }
    }
  }

  save() {
    return new Promise((resolve, reject) => {
      if (this.exists) {
        this.performUpdate_().then(({ data }) => {
          resolve(this)
        }).catch((error) => {
          reject(error)
        })
      }

      else {
        this.performInsert_().then(({ data }) => {
          // re-initialize data so constructor is ran again
          resolve(new this.constructor(data))
        }).catch((error) => {
          reject(error)
        })
      }
    })
  }

  performUpdate_() {
    let api = this.constructor.API + '/' + this.getKey()
    return axios.put(api, omit(this, ['id', 'exists']));
  }

  performInsert_() {
    return axios.post(this.constructor.API, omit(this, ['id', 'exists']));
  }

  performDelete_() {
    let api = this.constructor.API + '/' + this.getKey()
    return axios.delete(api);
  }

  getKeyName() {
    return this.constructor.hasOwnProperty('primaryKey')
      ? this.constructor.primaryKey
      : 'id'
  }

  getKey() {
    return this[this.getKeyName()]
  }

  delete() {
    return new Promise((resolve, reject) => {
      this.performDelete_().then(() => {
        resolve()
      }).catch(({ response }) => {
        reject(response.data)
      })
    })
  }
}
