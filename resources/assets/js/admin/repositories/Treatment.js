import Model from './Model';

export default class Treatment extends Model {
  static get API() {
    return '/api/v1/treatments'
  }

  static get attributes() {
    return ['name']
  }
}
