import axios from 'axios';

const API = '/api/v1/pets';

let cancel;

export class Pet {
  constructor () {
    this.pets = []
  }

  create (data) {
    return new Promise((resolve, reject) => {
      axios.post(API, data).then(({ data }) => {
        resolve(data)
      }).catch(({ response }) => {
        reject(response.data)
      })
    })
  }

  fetch (id) {
    return new Promise((resolve, reject) => {
      axios.get(`${API}/${id}`).then(({ data }) => {
        resolve(data);
      }).catch(() => {
        reject();
      })
    })
  }

  search (query) {
    return new Promise((resolve, reject) => {
      if (cancel) {
        cancel()
      }

      if (!query) {
        reject()
        return;
      }

      axios.get(`${API}/search`, {
        params: { query: query },
        cancelToken: new axios.CancelToken(c => cancel = c)
      }).then(({ data }) => {
        resolve(data)
      }).catch(() => {
        reject()
      })
    })
  }
}

export default new Pet()
