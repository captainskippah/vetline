import Model from './Model';

export default class Procedure extends Model {
  static get API() {
    return '/api/v1/procedures'
  }

  static get attributes() {
    return ['name']
  }
}
