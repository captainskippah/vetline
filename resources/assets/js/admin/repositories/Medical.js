import axios from 'axios';

const API = '/api/v1/medicals';

export class Medical {
  constructor () {
    this.medicals = []
  }

  fetchById(id) {
    if (!id) {
      throw new Error('ID is required');
    }

    return new Promise((resolve, reject) => {
      let record;

      if (this.medicals.length > 0) {
        record = this.medicals.find(m => m.id === id);
      }

      if (record) {
        resolve(resolve);
      } else {
        axios.get(`${API}/${id}`).then(({ data }) => {
          resolve(data);
        }).catch(() => {
          reject();
        })
      }
    })
  }

  fetchByPet (pet) {
    if (!pet) {
      throw new Error('Pet ID is required');
    }

    return new Promise((resolve, reject) => {
      axios.get(API, {
        params: { pet_id: pet }
      }).then(({ data }) => {
        this.medicals = data;
        resolve();
      }).catch(() => {
        reject();
      });
    });
  }

  remove (id) {
    if (!id) {
      throw new Error('ID is required');
    }

    return new Promise((resolve, reject) => {
      axios.delete(`${API}/${id}`).then(() => {
        let index = this.medicals.findIndex(m => m.id === id);
        this.medicals.splice(index, 1);
        resolve();
      }).catch(() => {
        reject();
      });
    });
  }

  update(id, data) {
    if (!id) {
      throw new Error('ID is required');
    }

    if (!data) {
      throw new Error('Data is required');
    }

    return new Promise((resolve, reject) => {
      axios.put(`${API}/${id}`, data).then(({ data }) => {
        if (this.medicals.length > 0) {
          let medical = this.medicals.find(m => m.id === Number(id));
          Object.assign(medical, data);
        }
        resolve();
      }).catch(() => {
        reject();
      })
    });
  }

  create (data) {
    return new Promise((resolve, reject) => {
      axios.post(API, data).then(({ data }) => {
        this.medicals.push(data);
        resolve();
      }).catch(({ response }) => {
        if (response.status === 422) {
          reject(response.data);
          return;
        }
        reject();
      });
    });
  }
}

export default new Medical()
