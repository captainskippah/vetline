import axios from 'axios';

let cancel;
const API = '/api/v1/customers'

export function transform (data) {
  return {
    firstname: data.firstname,
    middlename: data.middlename,
    lastname: data.lastname,
    phones: data.phones,
    street: data.address.street,
    barangay_id: data.address.barangay_id.toString()
  }
}

export class Customer {
  constructor () {
    this.customers = [];
  }

  search (query, archived = false) {
    return new Promise((resolve, reject) => {
      if (cancel) {
        cancel();
      }

      if (!query) {
        reject();
        return;
      }

      axios.get(`${API}/search`, {
        params: { query: query, archived: archived },
        cancelToken: new axios.CancelToken(c => cancel = c)
      }).then(({ data }) => {
        this.customers = data
        resolve(data)
      }).catch(() => {
        reject()
      })
    })
  }

  find (id) {
    return new Promise((resolve, reject) => {
      if (this.customers.length > 0) {
        resolve(this.customers.find(c => c.id === id))
      } else {
        axios.get(`${API}/${id}`).then(({ data }) => {
          resolve(data);
        }).catch(() => {
          reject();
        })
      }
    })
  }

  getActive () {
    return new Promise((resolve, reject) => {
      axios.get(API).then(({ data }) => {
        this.customers = data;
        resolve();
      }).catch(() => {
        reject();
      })
    })
  }

  getArchived () {
    return new Promise((resolve, reject) => {
      axios.get(API + '/?archived=true').then(({ data }) => {
        this.customers = data;
        resolve();
      }).catch(() => {
        reject();
      })
    })
  }

  create (data) {
    return new Promise((resolve, reject) => {
      axios.post(API, data).then(({ data }) => {
        this.customers.push(data);
        resolve();
      }).catch(({ response }) => {
        let { status, data: errors } = response;
        reject({ status, errors });
      })
    })
  }

  update (id, data) {
    return new Promise((resolve, reject) => {
      axios.put(`${API}/${id}`, data).then(({ data }) => {
        let index = this.customers.findIndex(c => c.id === id)
        this.customers[index] = data
        resolve()
      }).catch(({ response }) => {
        let { status, data: errors } = response;
        reject({ status, errors });
      })
    })
  }

  delete(id) {
    return new Promise((resolve, reject) => {
      axios.delete(`${API}/${id}/?force=true`).then(() => {
        let index = this.customers.findIndex(c => c.id === id);
        this.customers.splice(index, 1);
        resolve();
      }).catch(() => {
        reject();
      })
    })
  }

  archive (id) {
    return new Promise((resolve, reject) => {
      axios.delete(`${API}/${id}`).then(() => {
        let index = this.customers.findIndex(c => c.id === id);
        this.customers.splice(index, 1);
        resolve();
      }).catch(() => {
        reject();
      })
    })
  }

  restore (id) {
    return new Promise((resolve, reject) => {
      let customer = this.customers.find(c => c.id === id);
      let requestData = transform(Object.assign({}, customer, { archived: false }));

      axios.put(`${API}/${id}`, requestData).then(({ data }) => {
        let index = this.customers.findIndex(c => c.id === id);
        // Assumes this.customers are all archived
        // since you can only view either active OR archived
        // Might change this to be agnostic of this.customers
        this.customers.splice(index, 1);
        resolve();
      }).catch(() => {
        reject();
      })
    })
  }
}

export default new Customer();
