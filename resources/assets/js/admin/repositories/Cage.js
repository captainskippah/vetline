import axios from 'axios';

const API = '/api/v1/cages';
const BOARDED = 'boarded';
const RESERVED = 'reserved';

export class Cage {
  constructor () {
    this.cages = []
  }

  board (pet) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/v1/cages/board`, {
        pet_id: pet
      }).then((response) => {
        this.cages.push(response.data)
        resolve()
      }).catch(({ response }) => {
        // only two possible errors
        // No selected pet and general error.
        reject(response.data.pet_id || response.data)
      })
    })
  }

  vacate (id) {
    return new Promise((resolve, reject) => {
      axios.put(`/api/v1/cages/vacant/${id}`).then(() => {
        let index = this.cages.findIndex(cage => cage.id === id)
        this.cages.splice(index, 1)
        resolve()
      }).catch(({ response }) => {
        reject(response.data)
      })
    })
  }

  reserve (customer) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/v1/cages/reserve`, {
        customer_id: customer
      }).then((response) => {
        this.cages.push(response.data)
        resolve()
      }).catch(({ response }) => {
        // only two possible errors
        // No selected customer and general error.
        reject(response.data.customer_id || response.data)
      })
    })
  }

  fetch (status) {
    return new Promise((resolve, reject) => {
      axios.get(API + (status ? `/?status=${status}` : '')).then(({ data }) => {
        this.cages = data
        resolve()
      }).catch(() => {
        reject()
      })
    })
  }

  fetchBoarded () {
    return this.fetch(BOARDED)
  }

  fetchReserved () {
    return this.fetch(RESERVED)
  }
}

export default new Cage()
