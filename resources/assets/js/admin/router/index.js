import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

let Customers = {
	create: () => import(/* webpackChunkName: "customers" */ '../views/customers/create'),
	view: () => import(/* webpackChunkName: "customers" */ '../views/customers/view'),
}

let Medical = {
	index: () => import(/* webpackChunkName: "medicals" */ '../views/medicals/index'),
	create: () => import(/* webpackChunkName: "medicals" */ '../views/medicals/create'),
	view: () => import(/* webpackChunkName: "medicals" */ '../views/medicals/view'),
}

let Settings = {
	index: () => import(/* webpackChunkName: "settings" */ '../views/settings/index'),
	procedures: () => import(/* webpackChunkName: "settings" */ '../views/settings/procedures'),
	treatments: () => import(/* webpackChunkName: "settings" */ '../views/settings/treatments'),
	cages: () => import(/* webpackChunkName: "settings" */ '../views/settings/cages'),
}

export default new VueRouter ({
	mode: 'history',
	linkActiveClass: 'mdc-tab--active',
	routes: [
		{
			name: 'dashboard',
			path: '/admin/dashboard',
			meta: { title: 'Dashboard' },
			component: () => import('../views/dashboard/index')
		},

		{
			path: '/admin/customers',
			component: () => import('../views/Customers'),
			children: [
				{
					name: 'customers',
					path: '',
					meta: { title: 'Customers' }
				},
				{
					name: 'customer-create',
					path: 'create',
					meta: { title: 'New Customer' },
					component: Customers.create
				},
				{
					name: 'customer',
					path: ':id',
					meta: { title: 'View Customer' },
					component: Customers.view,
					props: true
				},
				{
					name: 'customer-edit',
					path: ':id/edit',
					meta: { title: 'Edit Customer' },
					component: Customers.create,
					props: true
				},
			]
		},

		{
			path: '/admin/pets/:id',
			props: true,
			component: () => import(/*webpackChunkName: "pets" */ '../views/pets/view'),
			children: [
				{
					name: 'pet',
					path: '',
					meta: { title: 'Pets' },
					component: Medical.index
				},
				{
					path: 'medicals/create',
					meta: { title: 'Create Medical Record' },
					component: Medical.create
				},
				{
					path: 'medicals/:mID',
					meta: { title: 'View Medical Record' },
					component: Medical.view },
				{
					path: 'medicals/:mID/edit',
					meta: { title: 'Edit Medical Record' },
					component: Medical.create
				}
			]
		},

		{
			name: 'boarding',
			path: '/admin/boarding',
			meta: { title: 'Boarding' },
			component: () => import(/* webpackChunkName: "cages" */ '../views/cages/index-boarding')
		},

		{
			name: 'reservation',
			path: '/admin/reservation',
			meta: { title: 'Reservation' },
			component: () => import(/* webpackChunkName: "cages" */ '../views/cages/index-reservation')
		},

		{
			name: 'appointments',
			path: '/admin/appointments',
			meta: { title: 'Appointments' },
			component: () => import(/* webpackChunkName: "appointments" */ '../views/appointments/index')
		},

		{
			name: 'settings',
			path: '/admin/settings',
			redirect: { name: 'settings-procedures' },
			component: Settings.index,
			children: [
				{
					name: 'settings-procedures',
					meta: { title: 'Settings' },
					path: 'procedures',
					component: Settings.procedures
				},
				{
					name: 'settings-treatments',
					meta: { title: 'Settings' },
					path: 'treatments',
					component: Settings.treatments
				},{
					name: 'settings-cages',
					meta: { title: 'Settings' },
					path: 'cages',
					component: Settings.cages
				}
			]
		}
	]
})
