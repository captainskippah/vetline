<?php

namespace App\Http\Requests;

use App\Models\Appointment;
use Illuminate\Foundation\Http\FormRequest;

class AppointmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'notes' => 'required',
            'pet_id' => 'required|exists:pets,id',
            'date' => 'required|date_format:Y-m-d',
            'time' => 'required|date_format:h:i a',
            'status' => $this->isMethod('post') ? '' : 'required'
        ];
    }

    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($v) {
            if (in_array('date', $v->valid()) && in_array('time', $v->valid())) {
                $raw = "{$this->input('date')} {$this->input('time')}";

                $dateTime = \DateTime::createFromFormat('Y-m-d h:i a', $raw)
                                ->format(\DateTime::ATOM);

                if (!Appointment::checkDateTime($dateTime)) {
                    $v->errors()->add('time', 'Appointment with the same date and time exists');
                }
            }
        });
    }

    public function messages()
    {
        return [
            'pet_id.required' => 'Please select a pet from the list',
            'time.date_format' => 'Time must be in a valid 12-hr format (ex. 03:00 pm)'
        ];
    }
}
