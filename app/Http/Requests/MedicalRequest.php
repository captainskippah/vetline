<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class MedicalRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'complaint' => 'required',
            'date' => 'required|date',
            'procedures.*' => 'sometimes|exists:procedures,id',
            'treatments.*' => 'sometimes|exists:treatments,id',
            'vaccines.*.name' => 'required',
            'vaccines.*.next' => 'required|date'
        ];
    }

    public function attributes()
    {
        return [
            'vaccines.*.name' => 'vaccine name',
            'vaccines.*.next' => 'vaccine\'s next date'
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        $errors = $validator->errors()->toArray();

        foreach ($errors as $field => $message) {
            if (count($parsed = explode('.', $field)) > 1) {
                $errors[$parsed[0]][$parsed[1]] = $message[0];
                unset($errors[$field]);
            } else {
                $errors[$field] = $message[0];
            }
        }

        return $errors;
    }
}
