<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CustomerRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'firstname' => 'required',
            'lastname' => 'required',

            'phones' => 'required|array|min:1',
            'phones.0' => 'required',

            'street' => 'required',
            'barangay_id' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'firstname' => 'first name',
            'middlename' => 'middle name',
            'lastname' => 'last name',

            'phones.*' => 'phone',

            'street' => 'street',
            'barangay_id' => 'barangay'
        ];
    }

    public function messages()
    {
        return [
            'phones.0.required' => 'At least 1 phone is required'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->messages()->toArray();

        foreach ($errors as $error => $message) {
             // Check if error should be an array
            if (strpos($error, '.')) {
                // Ex: {parent}.0
                $parent = explode('.', $error)[0];

                // Ex: phones.{nth}
                $nth = explode('.', $error)[1];

                unset($errors[$error]);
                $errors[$parent][$nth] = $message[0];
            } else {
                $errors[$error] = $message[0];
            }
        }

        // This sorts array errors
        // to make sure the return response
        // is an array, not a number-based object.
        // This just ensures consistency because
        // it can mess up the front-end
        foreach($errors as $key => $message) {
            if (is_array($message)) {
                ksort($errors[$key]);
            }
        }

        $response = response()->json($errors, 422);

        throw new HttpResponseException($response);
    }
}
