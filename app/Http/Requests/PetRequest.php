<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'species' => 'required',
            'breed' => 'required',
            'gender' => 'required|in:1,2',
            'birthday' => 'required|date|before_or_equal:' . date('Y-m-d'),
            'weight' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'birthday.before_or_equal' => 'The :attribute is not a valid date.'
        ];
    }
}
