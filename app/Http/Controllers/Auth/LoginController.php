<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Models\Phone;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/sample';

    protected function attemptLogin(Request $request)
    {
        $phone = Phone::with(['customer'])->where('number', $request->input('phone'))->first();
        if (!is_null($phone)) {
            $customer = $phone->customer;
            if (Hash::check($request->input('password'), $customer->password)) {
                Auth::login($customer);
                return true;
            }
        }
        throw new AuthenticationException;
    }

    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);
        return $this->authenticated($request, $this->guard()->user())
                ?:  $this->generateTokenResponse();
    }

    protected function generateTokenResponse()
    {
        $user = $this->guard()->user();
        $token = null;

        try {
            $token = JWTAuth::fromUser($user);
            if (!$token) {
                return response()->json('Invalid username/password', 401);
            }
        } catch (JWTException $e) {
            return response()->json('Could not create token', 500);
        }

        return response()->json(compact('token'));
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/');
    }

    public function username() {
        return 'phone';
    }
}
