<?php

namespace App\Http\Controllers\Api;

use App\Models\Treatment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TreatmentController extends Controller
{
    public function index()
    {
        $treatments = Treatment::orderBy('name', 'ASC')
            ->get()
            ->map([Treatment::class, 'transform']);

    	return response()->json($treatments);
    }

    public function store(Request $request)
    {
    	$treatment = new Treatment;
    	$treatment->name = $request->input('name');
    	$treatment->save();

    	return response()->json($treatment);
    }

    public function update(Request $request, Treatment $treatment)
    {
        $attributes = $request->only(array_keys($treatment->getAttributes()));

        foreach ($attributes as $attr => $value) {
            if ($attr !== $treatment->getKeyName()) {
                $treatment->{$attr} = $value;                
            }
        }

        $treatment->save();

        return response()->json(Treatment::transform($treatment));
    }

    public function destroy(Treatment $treatment)
    {
        if ($treatment->medical()->count() > 0) {
            return response()->json('Cannot be deleted. Some medical record uses this treatment.', 422);
        }

        $treatment->delete();

        return response()->json();
    }
}
