<?php

namespace App\Http\Controllers\Api;

use App\Models\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProvinceController extends Controller
{
    public function index()
    {
    	return response()->json(Province::get());
    }

    public function show(Request $request, $provCode)
    {
    	$province = Province::where('provCode', $provCode)->first();

    	if (!$province) {
    		return response()->json('', 404);
    	}

    	return response()->json($province);
    }

    public function showCities(Request $request, $provCode)
    {
        $province = Province::where('provCode', $provCode)->first();

        if (!$province) {
            return response()->json('', 404);
        }

        $cities = $province->cities;

        return response()->json($cities);
    }
}
