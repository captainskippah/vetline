<?php

namespace App\Http\Controllers\Api;

use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppointmentRequest;

class AppointmentController extends Controller
{
    protected $api = '/api/v1/appointments';

	public function index(Request $request, Appointment $appointment)
	{
        $month = $request->input('month', date('n'));

        $results = $appointment->with(['pet'])
                    ->whereRaw('Extract(MONTH FROM start_time) = ?', $month)
                    ->orderBy('start_time', 'ASC')
                    ->get()
                    ->toArray();

        $response = array_map(['App\Models\Appointment', 'transform'], $results);

        return response()->json($response);
	}

    public function store(AppointmentRequest $request, Appointment $appointment)
    {
        $dt = $request->input('date').' '.$request->input('time');
        $appointment->start_time = \DateTime::createFromFormat('Y-m-d h:i a', $dt)->format(\DateTime::ATOM);

        $appointment->notes = $request->input('notes');
        $appointment->pet_id = $request->input('pet_id');
        $appointment->end_time = null;
        $appointment->status = 'scheduled';
        
        $appointment->save();

        $result = $appointment->load(['pet'])->toArray();
        $response = Appointment::transform($result);

        return response()->json($response, 201);
    }

    public function update(AppointmentRequest $request, Appointment $appointment)
    {
        $dt = $request->input('date').' '.$request->input('time');

        $appointment->start_time = \DateTime::createFromFormat('Y-m-d h:i a', $dt)->format(\DateTime::ATOM);

        $appointment->notes = $request->input('notes');
        $appointment->pet_id = $request->input('pet_id');
        
        $appointment->save();

        $result = $appointment->load(['pet'])->toArray();
        $response = Appointment::transform($result);

        return response()->json($response, 201);
    }

    public function destroy(Appointment $appointment)
    {
    	$appointment->delete();
    }

    public function status(Request $request, Appointment $appointment)
    {
        $appointment->status = $request->input('status');
        $appointment->end_time = (new \DateTime())->format(\DateTime::ATOM);
        $appointment->save();
    }
}
