<?php

namespace App\Http\Controllers\Api;

use App\Models\Barangay;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BarangayController extends Controller
{
    public function show(Request $request, $brgyCode)
    {
    	$barangay = Barangay::where('brgyCode', $brgyCode)->first();

    	if (!$barangay) {
    		return response('', 404);
    	}

    	return response()->json($barangay);
    }
}
