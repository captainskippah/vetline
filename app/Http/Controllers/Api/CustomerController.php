<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Models\Address;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\Http\Requests\CustomerRequest;
use Elasticsearch\Common\Exceptions\NoNodesAvailableException;

class CustomerController extends Controller
{

    protected $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function index(Request $request)
    {
        if ($request->input('archived') === 'true') {
            $this->customer = $this->customer->onlyTrashed();
        }

        $results = $this->customer
            ->with(['phones', 'address.barangay', 'pets'])
            ->orderBy('firstname')
            ->orderBy('middlename')
            ->orderBy('lastname')
            ->get();

        return response()->json($results->map([Customer::class, 'transform']));
    }

    public function search(Request $request)
    {
        $concat = implode(',', ['firstname', 'middlename', 'lastname']);
        $query = "%{$request->input('query')}%";

        if ($request->input('archived') === 'true') {
            $this->customer = $this->customer->onlyTrashed();
        }

        $results = $this->customer
            ->whereRaw("concat_ws(' ',{$concat}) ILIKE ?", $query)
            ->orderBy('firstname')
            ->orderBy('middlename')
            ->orderBy('lastname')
            ->get();

        if (!empty($results)) {
            $results->load(['phones', 'address.barangay']);
             return response()->json($results->map([Customer::class, 'transform']));
        }

        return response()->json([]);
    }

    public function store(CustomerRequest $request)
    {
        return DB::transaction(function() use ($request) {
            $this->customer->firstname = $request->input('firstname');
            $this->customer->middlename = $request->input('middlename');
            $this->customer->lastname = $request->input('lastname');
            $this->customer->save();

            $phones = [];

            foreach($request->input('phones') as $phone) {
                if (!empty($phone)) {
                    $phones[]['number'] = $phone;
                }
            }

            $this->customer->phones()
                ->createMany($phones);

            $this->customer->address()
                ->create([
                    'street' => $request->input('street'),
                    'barangay_id' => $request->input('barangay_id')
                ]);

            // Relationship is included in the response.
            $this->customer->load(['phones', 'address.barangay']);

            return response()->json(Customer::transform($this->customer));
        });
    }

    public function show(Request $request, Customer $customer)
    {
        Log::debug($request->header());

        if (auth()->guard('api_customer')->check()) {
            $customer = auth()->guard('api_customer')->user();
        }

        $customer->load(['pets', 'phones', 'reservations', 'address.barangay']);

        return response()->json(Customer::transform($customer));
    }

    public function update(CustomerRequest $request, Customer $customer)
    {
        $attributes = $request->only(['firstname', 'middlename', 'lastname']);

        foreach ($attributes as $attribute => $value) {
            $customer->{$attribute} = $value;
        }

        // Make "deleted_at" null to remove it from softDelete status
        // in case we are restoring an archived record.
        $customer->deleted_at = null;
        $customer->save();

        $customer->address()->update($request->only(['street', 'barangay_id']));

        // Delete phone numbers removed from customer.
        $customer->phones()->whereNotIn('number', $request->input('phones'))->delete();

        // Add new numbers not currently in customer.
        foreach ($request->input('phones') as $phone) {
            if (is_null($customer->phones()->where(['number' => $phone])->first())) {
                $customer->phones()->firstOrNew(['number' => $phone])->save();
            }
        }

        // Relationship is included in the response.
        $customer = $customer->load(['phones', 'address.barangay']);

        return response()->json(Customer::transform($customer));
    }

    public function destroy(Request $request, Customer $customer)
    {
        if ($request->input('force') === true) {
            $customer->forceDelete();
        } else {
            $customer->delete();
        }

        return response()->json();
    }
}
