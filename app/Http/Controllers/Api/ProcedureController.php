<?php

namespace App\Http\Controllers\Api;

use App\Models\Procedure;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProcedureController extends Controller
{
	public function index()
	{
        $procedures = Procedure::orderBy('name', 'ASC')
            ->get()
            ->map([Procedure::class, 'transform']);

		return response()->json($procedures);
	}

    public function store(Request $request)
    {
    	$procedure = new Procedure;
    	$procedure->name = $request->input('name');
    	$procedure->save();

    	return response()->json($procedure);
    }

    public function update(Request $request, Procedure $procedure)
    {
        $attributes = $request->only(array_keys($procedure->getAttributes()));

        foreach ($attributes as $attr => $value) {
            if ($attr !== $procedure->getKeyName()) {
                $procedure->{$attr} = $value;                
            }
        }

        $procedure->save();

        return response()->json(Procedure::transform($procedure));
    }

    public function destroy(Procedure $procedure)
    {
        if ($procedure->medical()->count() > 0) {
            return response()->json('Cannot be deleted. Some medical record uses this procedure.', 422);
        }

        $procedure->delete();

        return response()->json();
    }
}
