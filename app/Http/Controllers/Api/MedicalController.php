<?php

namespace App\Http\Controllers\Api;

use App\Models\Pet;
use App\Models\Medical;
use App\Models\Procedure;
use App\Models\Treatment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\MedicalRequest;

class MedicalController extends Controller
{
    public function index(Request $request)
    {
        $query = new Medical;
        $query = $query->orderBy('date', 'DESC');

        if ($request->has('pet_id')) {
            $query = $query->where('pet_id', $request->input('pet_id'));
        }

        $results = $query->get();

        return response()->json($results->map([Medical::class, 'transform']));
    }

    public function store(MedicalRequest $request)
    {
        return DB::transaction(function () use ($request) {
            $attributes = $request->only([
                'date',
                'complaint', 
                'history',
                'diagnosis',
                'pet_id'
            ]);

            $medical = new Medical;
            $medical->author_id = auth()->guard('api_employee')->user()->id;
            $medical->observation = clean($request->input('observation'));

            foreach ($attributes as $attr => $value) {
                $medical->{$attr} = empty($value) ? null : $value;
            }

            $medical->save();

            $medical->procedures()->attach($request->input('procedures'));

            $medical->treatments()->attach($request->input('treatments'));

            $medical->vaccines()->createMany($this->getValidVaccines($request->input('vaccines')));

            // We need to load procedures and treatments manually
            // because `attach` only returns null unlike `createMany`
            $medical->load(['procedures', 'treatments', 'vaccines']);

            return response()->json(Medical::transform($medical));
        });

    }

    public function show(Request $request, Medical $medical)
    {
        $medical->load(['procedures', 'treatments', 'vaccines']);
        return response()->json(Medical::transform($medical));
    }

    public function destroy($id)
    {
        Medical::find($id)->delete();
        return response()->json();
    }

    public function update(MedicalRequest $request, Medical $medical)
    {
        return DB::transaction(function() use ($request, $medical) {
            $attributes = $request->only([
                'complaint',
                'history',
                'observation',
                'diagnosis',
                'date'
            ]);

            foreach($attributes as $key => $value) {
                $medical->{$key} = $value;
            }

            $medical->save();

            if (empty($request->input('vaccines'))) {
                $medical->vaccines()->delete();
            } else {
                // Remove old vaccines not in request
                $existing_vaccines = array_pluck($this->getExistingVaccines($request), ['id']);
                $medical->vaccines()->whereNotIn('id', $existing_vaccines)->delete();

                // Add new vaccines
                $new_vaccines = $this->getNewVaccines($request);
                $medical->vaccines()->createMany($new_vaccines);

                $medical->load(['vaccines']);
            }

            $medical->procedures()->sync($request->input('procedures', []));
            if ($request->has('procedures')) {
                $medical->load(['procedures']);
            }

            $medical->treatments()->sync($request->input('treatments', []));
            if ($request->has('treatments')) {
                $medical->load(['treatments']);
            }

            return response()->json(Medical::transform($medical));
        });

    }

    // SHOULD REMOVE. USE FORM REQUEST
    protected function getValidVaccines(array $vaccines)
    {
        return array_filter($vaccines, function ($vaccine) {
            return !empty($vaccine['name']);
        });
    }

    /**
     *  Will return vaccines with ID from request
     */
    protected function getExistingVaccines(Request $request)
    {
        $vaccines = $request->input('vaccines');

        return empty($vaccines) ? [] : array_filter($request->input('vaccines'), function ($v) {
            return !empty($v['id']);
        });
    }

    protected function getNewVaccines(Request $request)
    {
        return array_filter($request->input('vaccines'), function ($v) {
            return empty($v['id']);
        });
    }
}
