<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function smsAccount()
    {
    	$client = resolve('ClickSendClient');
    	$account = $client->getAccount()->getAccount();

    	return response()->json($account->data);
    }

    public function smsStatistics()
    {
    	$client = resolve('ClickSendClient');
    	$statistics = $client->getStatistics()->getSmsStatistics();

    	return response()->json($statistics->data);
    }
}
