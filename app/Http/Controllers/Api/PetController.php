<?php

namespace App\Http\Controllers\Api;

use App\Models\Pet;
use Illuminate\Http\Request;
use App\Http\Requests\PetRequest;
use App\Http\Controllers\Controller;

class PetController extends Controller
{
	protected $pet;

	public function __construct(Pet $pet)
	{
		$this->pet = $pet;
	}

    public function store(PetRequest $request)
    {
        $attributes = $request->only([
            'name',
            'breed',
            'gender',
            'species',
            'weight',
            'customer_id',
            'birthday'
        ]);

        foreach ($attributes as $attr => $value) {
            $this->pet->{$attr} = $value;
        }

        $this->pet->save();

        $response = Pet::transform($this->pet);

        return response()->json($response);
    }

    public function show(Pet $pet)
    {
        $pet->load('customer');
    	return response()->json(Pet::transform($pet));
    }

    public function update(Request $request, Pet $pet)
    {
    	$pet->update($request->all());
    }

    public function destroy(Pet $pet)
    {
    	$pet->delete();
    }

    public function search(Request $request)
    {
        if (! $request->has('query')) {
            return response()->json();
        }

        $query = "%{$request->input('query')}%";

        $results = $this->pet
            ->whereRaw("name ILIKE ?", $query)
            ->with(['customer' => function ($q) {
                $q->selectRaw('id, concat_ws(\' \', firstname,middlename,lastname) as name');
            }])
            ->get();

        return response()->json($results->toArray());
    }

    protected function checkSort($column)
    {
        return in_array($column, $this->pet->searchable);
    }
}
