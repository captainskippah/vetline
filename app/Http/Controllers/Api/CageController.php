<?php

namespace App\Http\Controllers\Api;

use App\Models\Cage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CageController extends Controller
{
	protected $cage;

	public function __construct(Cage $cage)
	{
		$this->cage = $cage;
	}

    public function index(Request $request)
    {
        $status_id = array_search(
            strtolower($request->input('status')),
            array_map('strtolower', Cage::STATUS)
        );

        // Check if false explicitly because if
        // array_search can return 0 which is
        // also a false-y value
        if ($status_id !== false) {
            $this->cage = $this->cage->where(compact('status_id'));

            if ($status_id === Cage::BOARDED) {
                $this->cage->with(['pet']);
            } else if ($status_id === Cage::RESERVED) {
                $this->cage->with(['customer']);
            }
        }

        $this->cage = $this->cage->orderBy('updated_at', 'DESC');

        return response()->json($this->cage->get()->map([Cage::class, 'transform']));
    }

    public function store(Request $request)
    {
        $this->cage->status_id = Cage::AVAILABLE;
    	$this->cage->save();

    	return response()->json([
            'id' => $this->cage->id
        ]);
    }

    public function destroy($id)
    {
    	$cage = $this->cage->find($id);

    	if (empty($cage)) {
    		return response()->json('', 404);
    	}

    	$cage->delete();

    	return response()->json();
    }

    public function board(Request $request)
    {
        $cage = Cage::available()->first();

    	if (empty($cage)) {
            if ($request->exists('cage_id')) {
                return response()->json('The cage does not exist', 404);
            }
            return response()->json('No more available cages', 422);
        }

    	if ($cage->board($request->input('pet_id'))) {
            $cage->load('pet');
    		return response()->json(Cage::transform($cage));
    	}

    	return response()->json('The cage is already boarded', 422);
    }

    public function vacate(Request $request, $id)
    {
        $cage = $this->cage->find($id);

        if (empty($cage)) {
            return response()->json('The cage does not exist', 404);
        }

        if ($cage->vacate()) {
            return response()->json(Cage::transform($cage));
        }

        return response()->json('The cage is already vacant', 422);
    }

    public function reserve(Request $request)
    {
        $cage = Cage::available()->first();

        if (empty($cage)) {
            if ($request->exists('cage_id')) {
                return response()->json('The cage does not exist', 404);
            }
            return response()->json('No more available cages', 422);
        }

        $customer_id = auth()->guard('api_customer')->check()
            ? auth()->guard('api_customer')->user()->id
            : $request->input('customer_id');

        if ($cage->reserve($customer_id)) {
            $cage->load('customer');
            return response()->json(Cage::transform($cage));
        }

        return response()->json('The cage is not available', 422);
    }
}
