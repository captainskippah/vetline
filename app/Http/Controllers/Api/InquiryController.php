<?php

namespace App\Http\Controllers\Api;

use App\Models\Inquiry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\InquiryRequest;

class InquiryController extends Controller
{
	public function index()
	{
		$inquiries = Inquiry::limit(50)->get();

		return response()->json($inquiries);
	}

    public function store(InquiryRequest $request)
    {
    	$inquiry = new Inquiry($request->only(['name', 'phone', 'message']));
        $inquiry->save();

    	return response()->json();
    }

    public function update() {

    }
}
