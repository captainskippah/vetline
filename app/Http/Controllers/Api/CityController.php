<?php

namespace App\Http\Controllers\Api;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function show(Request $request, $cityCode)
    {
    	$city = City::where('citymunCode', $cityCode)->first();

    	if (!$city) {
    		return response('', 404);
    	}

    	return response()->json($city);
    }

    public function showBarangays(Request $request, $cityCode)
    {
    	$city = City::where('citymunCode', $cityCode)->first();

    	if (!$city) {
    		return response('', 404);
    	}

    	$barangays = $city->barangays;

    	return response()->json($barangays);
    }
}
