<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PetController extends Controller
{
    public function index(Request $request)
    {
    	return view('admin.index');
    }

    public function create()
    {
        return view('admin.index');
    }

    public function edit(Request $request, $id)
    {
        return view('admin.index');
    }

    public function show(Request $request, $id)
    {
        return view('admin.index');
    }
}
