<?php

namespace App\Http\Middleware;

use Closure;

class VerifyRecaptcha
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'secret' => env('RECAPTCHA_SECRET'),
            'response' => $request->input('g-recaptcha-response')
        ]);

        $response = json_decode(curl_exec($ch));

        curl_close($ch);

        if ($response->success) {
            return $next($request);
        }

        return response()->json('Invalid captcha', 422);
    }
}
