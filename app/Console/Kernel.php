<?php

namespace App\Console;

use App\Models\Appointment;
use App\Jobs\SendAppointmentSms;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /**
         *  Mark incomplete appointment as cancelled
         *  daily for convenience in case a staff forget
         *  about it.
         */
        $schedule->call(function () {
            $appt = new Appointment();
            $appt->scheduled()
                ->whereRaw("appointments.start_time::date < DATE 'tomorrow'")
                ->update(['status' => Appointment::CANCELLED]);
        })->daily();

        $schedule->job(new SendAppointmentSms)->dailyAt('21:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
