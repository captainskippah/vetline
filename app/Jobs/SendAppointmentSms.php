<?php

namespace App\Jobs;

use App\Models\Appointment;
use Illuminate\Bus\Queueable;
use ClickSendLib\APIException;
use ClickSendLib\ClickSendClient;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendAppointmentSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle(Appointment $appointment)
    {
        $appointments = $appointment->tomorrow()->get();

        if (!empty($appointments)) {
            try {
                $appointments->load(['pet.customer']);
                $messages = $appointments->map([$this, 'createMessage'])->toArray();

                $client = new ClickSendClient(config('clicksend.username'), config('clicksend.api_key'));
                $client->getSms()->sendSms(['messages' => $messages]);
            } catch (APIException $e) {

            }
        }
    }

    public function createMessage(Appointment $appt, $key)
    {
        $phone = $appt->pet->customer->phones->first()->number;

        $pet = $appt->pet->name;

        $time = (new \Datetime($appt->start_time))->format('h:i A');

        $customer = implode(' ', [
            $appt->pet->customer->firstname,
            $appt->pet->customer->lastname
        ]);

        $body = str_replace(
            [':recipient', ':pet', ':time'],
            [$customer, $pet],
            trans('appointment.sms')
        );

        return [
            'to' => $phone,
            'from' => 'Vetline',
            'body' => $body
        ];
    }
}
