<?php

namespace App\Providers;

use ClickSendLib\ClickSendClient;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Migration error fix (5.4)
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $client = new ClickSendClient(
            config('clicksend.username'),
            config('clicksend.api_key')
        );

        $this->app->instance('ClickSendClient', $client);
    }
}
