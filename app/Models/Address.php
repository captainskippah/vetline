<?php

namespace App\Models;

use App\Models\Barangay;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public $timestamps = false;

    public $fillable = ['street', 'barangay_id'];

    protected $with = ['barangay'];

    public static function transform(Address $address)
    {
        if (! $address->relationLoaded('barangay')) {
            $address->load('barangay');
        }

    	return [
    		'street' => $address->street,
    		'barangay' => $address->barangay->brgyDesc,
    		'barangay_id' => $address->barangay_id
    	];
    }

    public function barangay()
    {
    	return $this->belongsTo(Barangay::class, 'barangay_id', 'brgyCode');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
