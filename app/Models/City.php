<?php

namespace App\Models;

use App\Models\Barangay;
use App\Models\Province;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $primaryKey = 'citymunCode';

    public function province()
    {
    	return $this->belongsTo(Province::class, 'provCode', 'provCode');
    }

    public function barangays()
    {
    	return $this->hasMany(Barangay::class, 'citymunCode');
    }
}
