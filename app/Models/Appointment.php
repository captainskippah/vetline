<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Pet;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
    use SoftDeletes;

    const STATUS = ['scheduled', 'completed', 'cancelled'];

    const SCHEDULED = 0;

    const COMPLETED = 1;

    const CANCELLED = 2;

    public $timestamps = false;

    protected $guarded = ['id'];

    protected $appends = ['status'];

    protected $casts = [
    	'cancelled' => 'boolean'
    ];

    /**
     *  Check if given DateTime is available
     */
    public static function checkDateTime($dateTime)
    {
        return (new static)->where('start_time', '=', $dateTime)
            ->count() === 0;
    }

    public static function transform(array $appointment)
    {
        return [
            'id' => $appointment['id'],
            'notes' => $appointment['notes'],
            'status' => $appointment['status'],
            'start_time' => $appointment['start_time'],
            'end_time' => $appointment['end_time'],
            'pet' => [
                'id' => $appointment['pet']['id'],
                'name' => $appointment['pet']['name']
            ]
        ];
    }

    public function scopeScheduled($query)
    {
        return $query->where('status_id', '=', self::SCHEDULED);
    }

    public function scopeTomorrow($query)
    {
        return $query->whereRaw("appointments.start_time::date = DATE 'tomorrow'");
    }

    public function pet()
    {
        return $this->belongsTo(Pet::class);
    }

    public function getStartTimeAttribute($date)
    {
        return (new Carbon($date))->toISO8601String();
    }

    public function getEndTimeAttribute($date)
    {
        return (new Carbon($date))->toISO8601String();
    }

    public function getStatusAttribute()
    {
        return self::STATUS[$this->status_id];
    }

    public function setStatusAttribute($value)
    {
        $this->attributes['status_id'] = array_search($value, self::STATUS);
    }
}
