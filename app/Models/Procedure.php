<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Procedure extends Model
{
    public $timestamps = false;

    public static function transform(Procedure $procedure)
    {
    	return array_only($procedure->toArray(), ['id', 'name']);
    }

    public function medical()
    {
    	return $this->belongsToMany(Medical::class);
    }
}
