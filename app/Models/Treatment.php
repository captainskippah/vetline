<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Treatment extends Model
{
    public $timestamps = false;

    public static function transform(Treatment $treatment)
    {
    	return array_only($treatment->toArray(), ['id', 'name']);
    }

    public function medical()
    {
    	return $this->belongsToMany(Medical::class);
    }
}
