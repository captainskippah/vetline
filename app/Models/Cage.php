<?php

namespace App\Models;

use App\Models\Pet;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Cage extends Model
{
    const AVAILABLE = 0;
    const BOARDED = 1;
    const RESERVED = 2;

    const STATUS = [
        'Available',
        'BOARDED',
        'Reserved'
    ];

    public $guarded = [];

    public static function transform(Cage $cage)
    {
        $formatted = [
            'id' => $cage->id,
            'status' => static::STATUS[$cage->status_id],
            'updated_at' => $cage->updated_at->toDateTimeString()
        ];

        if ($cage->isBoarded() && $cage->relationLoaded('pet')) {
            $formatted['pet'] = Pet::transform($cage->pet);
        } else if ($cage->isReserved() && $cage->relationLoaded('customer')) {
            $formatted['customer'] = Customer::transform($cage->customer);
        }

        return $formatted;
    }

    public function board($pet_id)
    {
        Validator::make([
            'pet_id' => $pet_id,
            'status_id' => $this->status_id
        ], [
            'pet_id' => 'required|exists:pets,id|unique:cages',
            'status_id' => 'not_in:'.static::BOARDED
        ], [
            'pet_id.required' => 'A pet is required',
            'pet_id.unique' => 'This pet has boarded already'
        ])
        ->validate();

        $this->status_id = static::BOARDED;
        $this->pet_id = $pet_id;

    	return $this->save();
    }

    public function vacate()
    {
        $this->update([
            'status_id' => static::AVAILABLE,
            'pet_id' => null,
            'customer_id' => null
        ]);

        return true;
    }

    public function reserve($customer_id)
    {
        Validator::make([
            'customer_id' => $customer_id,
            'status_id' => $this->status_id
        ], [
            'customer_id' => 'required|exists:customers,id',
            'status_id' => 'in:' . static::AVAILABLE
        ])->validate();

        if (! $this->isAvailable()) {
            return false;
        }

        $this->update([
            'status_id' => static::RESERVED,
            'customer_id' => $customer_id
        ]);

        return true;
    }

    public function isBoarded()
    {
        return $this->status_id === static::BOARDED;
    }

    public function isReserved()
    {
        return $this->status_id === static::RESERVED;
    }

    public function isAvailable()
    {
        return $this->status_id === static::AVAILABLE;
    }

    public function scopeAvailable($query)
    {
        return $query->where(['status_id' => static::AVAILABLE]);
    }

    public function pet()
    {
        return $this->belongsTo(Pet::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function setCreatedAt($value)
    {
        return $this;
    }
}
