<?php

namespace App\Models;

use App\Models\Vaccine;
use App\Models\Auth\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medical extends Model
{
	use SoftDeletes;

    public $timestamps = false;

	protected $with = ['pet', 'author'];

	public static function transform(Medical $medical)
	{
		return [
			'id' => $medical->id,
			'complaint' => $medical->complaint,
			'history' => $medical->history,
			'observation' => $medical->observation,
			'diagnosis' => $medical->diagnosis,
			'date' => (new \Datetime($medical->date))->format('M d, Y'),
			'author' => $medical->author->toArray(),

			'pet' => $medical->relationLoaded('pet')
				? Pet::transform($medical->pet)
				: [],

			'procedures' => $medical->relationLoaded('procedures')
				? $medical->procedures->map([Procedure::class, 'transform'])
				: [],

			'treatments' => $medical->relationLoaded('treatments')
				? $medical->treatments->map([Treatment::class, 'transform'])
				: [],

			'vaccines' => $medical->relationLoaded('vaccines')
				? $medical->vaccines->map([Vaccine::class, 'transform'])
				: [],
		];
	}

	public function author()
	{
		return $this->belongsTo(Employee::class);
	}

	public function pet()
	{
		return $this->belongsTo(Pet::class);
	}

	public function procedures()
	{
		return $this->belongsToMany(Procedure::class);
	}

	public function treatments()
	{
		return $this->belongsToMany(Treatment::class);
	}

	public function vaccines()
	{
		return $this->hasMany(Vaccine::class);
	}
}
