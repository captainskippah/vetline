<?php

namespace App\Models;


use App\Models\Pet;
use App\Models\Phone;
use App\Models\Address;
use Laravel\Scout\Searchable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable implements JWTSubject
{
    use SoftDeletes;

    protected $fillable = [
        'firstname', 'middlename', 'lastname', 'phone', 'username'
    ];

    protected $hidden = ['password'];

    protected $dates  = ['deleted_at'];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [
             'user' => [ 
                'id' => $this->id
             ]
        ];
    }

    public static function transform(Customer $customer)
    {
        return [
            'id' => $customer->id,
            'username' => $customer->username,

            'firstname' => ucwords(strtolower($customer->firstname)),
            'middlename' => ucwords(strtolower($customer->middlename)),
            'lastname' => ucwords(strtolower($customer->lastname)),

            'link' => route('customers.show', ['customer' => $customer->id], false),

            'archived' => !is_null($customer->deleted_at),

            'reservations' => $customer->relationLoaded('reservations')
                ? $customer->reservations->map([Cage::class, 'transform'])
                : [],

            'address' => $customer->relationLoaded('address')
                ? Address::transform($customer->address)
                : [],

            'phones' => $customer->relationLoaded('phones')
                ? $customer->phones->map([Phone::class, 'transform'])
                : [],

            'pets' => $customer->relationLoaded('pets')
                ? $customer->pets->map([Pet::class, 'transform'])
                : [],
        ];
    }

    public function address()
    {
    	return $this->hasOne(Address::class);
    }

    public function phones()
    {
        return $this->hasMany(Phone::class);
    }

    public function pets()
    {
        return $this->hasMany(Pet::class);
    }

    public function reservations()
    {
        return $this->hasMany(Cage::class);
    }

    public function getMiddlenameAttribute($value)
    {
        return $value ? $value : '';
    }
}
