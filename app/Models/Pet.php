<?php

namespace App\Models;

use DateTime;
use App\Models\Customer;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pet extends Model
{
	use SoftDeletes;
    use Searchable;

    public $timestamps = false;

    public $asYouType = true;

    public $guarded = [];

    public $searchable = [
    	'name', 'breed', 'description', 'birthday', 'gender'
    ];

    const GENDER = ['Unknown', 'Male', 'Female', 'N/A'];

    public static function transform(Pet $pet)
    {
        return [
            'id' => $pet->id,
            'name' => $pet->name,
            'species' => $pet->species,
            'breed' => $pet->breed,
            'gender' => self::GENDER[$pet->gender],
            'birthday' => (new DateTime($pet->birthday))->format('M  d, Y'),
            'weight' => $pet->weight,
            'link' => route('pets.show', ['pet' => $pet->id], false),
            'image' => asset('/images/pets/sample.jpg'),
            'customer' => $pet->relationLoaded('customer')
                ? Customer::transform($pet->customer)
                : []
        ];
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function toSearchableArray()
    {
        return array_only($this->toArray(), ['id', 'name']);
    }
}
