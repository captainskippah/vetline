<?php

namespace App\Models;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
	public $timestamps = false;

	public $fillable = ['number'];

	public static function transform(Phone $phone)
	{
		return $phone['number'];
	}

    public function customer()
    {
    	return $this->belongsTo(Customer::class);
    }
}
