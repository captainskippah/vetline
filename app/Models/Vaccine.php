<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vaccine extends Model
{
    public $guarded = [];
    public $timestamps = false;

    public static function transform(Vaccine $vaccine)
    {
    	return [
    		'id' => $vaccine->id,
    		'name' => $vaccine->name,
    		'next' => (new \Datetime($vaccine->next))->format('M d, Y')
    	];
    }
}
