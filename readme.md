## Dependencies
- PHP version that laravel supports
- Postgresql

## Installation

- composer install
- npm install
- npx mix
- php artisan migrate
- [generate sample admin](#generating-sample-data)
- php artisan serve
- visit localhost:8000/admin

## Generating sample data
- php artisan db:seed --class DefaultAdmin (user: admin; password: password)
- php artisan db:seed --class SampleData (Will also generate default admin)

## Running tests
- php artisan migrate --database pgsql_testing
- php artisan db:seed --class SampleData --database pgsql_testing (Some tests still rely on sample data. To be fixed soon)
- vendor/bin/phpunit
