module.exports = {
	extends: [
		'eslint:recommended',
		// 'plugin:vue/recommended'
	],

	plugins: [
		'vue'
	],

	parser: 'vue-eslint-parser',
	parserOptions: {
		parser: 'babel-eslint'
	},

	env: {
		commonjs: true,
		browser:true,
		es6: true
	}
}
