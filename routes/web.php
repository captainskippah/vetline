<?php

/*
 * Juls Side Routes
 */
Route::get('/', function () {
    return view('home');
});


Route::get('/sample', function() {
	return view('client.index');
});


Route::get('/developer/profile', function () {
	return view('developer.profile');
});

/*
 * Lemuel Side Routes
 */

Route::post('client/login', 'Auth\LoginController@login');

Route::group(['prefix' => 'admin'], function() {

	// Unauthenticaed Routes
	Route::group(['middleware' => 'guest.employee'], function() {
		Route::get('login', 'Admin\Auth\LoginController@showLoginForm');
		Route::post('login', 'Admin\Auth\LoginController@login');
	});

	// Authenticated Routes
	Route::group(['middleware' => 'auth.employee'], function() {
		Route::get('', function() {
			return redirect('admin/dashboard');
		});

		Route::get('logout', 'Admin\Auth\LoginController@logout');

		Route::get('dashboard', function() {
			return view('admin.index');
		});
		
		Route::resource('customers', 'Web\CustomerController', ['only' => [
			'index', 'create', 'show', 'edit']
		]);

		Route::get('/pets/{pet}/medicals/{medical}/edit', 'Web\MedicalController@show');
		Route::get('/pets/{pet}/medicals/{medical}', 'Web\MedicalController@show');
		Route::get('pets/{pet}/medicals/create', 'Web\MedicalController@create');
		Route::resource('pets', 'Web\PetController', ['only' => [
			'index', 'create', 'show', 'edit']
		]);

		Route::get('boarding', 'Web\CageController@boarding');
		Route::get('reservation', 'Web\CageController@reservation');

		Route::get('appointments', 'Web\AppointmentController@index');

		Route::get('settings/{any}', function () {
			return view('admin.index');
		});
	});
});
