<?php

Route::group(['prefix' => 'v1'], function(){
	Route::get('customers/auth/user', 'Api\CustomerController@show');
	Route::post('customers/auth/register', 'Auth\RegisterController@register');

	Route::get('auth/user', function () {
		$user = auth()->guard('api_employee')->user();
		return response()->json($user);
	});

	Route::get('inquiries', 'Api\InquiryController@index');
	Route::post('contact', 'Api\InquiryController@store')->middleware('recaptcha');

	Route::get('sms/account', 'Api\DashboardController@smsAccount');
	Route::get('sms/statistics', 'Api\DashboardController@smsStatistics');

	Route::post('login', 'Auth\LoginController@login');

	Route::get('provinces', 'Api\ProvinceController@index');
	Route::get('provinces/{province}', 'Api\ProvinceController@show');
	Route::get('provinces/{province}/cities', 'Api\ProvinceController@showCities');

	Route::get('cities/{city}', 'Api\CityController@show');
	Route::get('cities/{city}/barangays', 'Api\CityController@showBarangays');

	Route::get('barangays/{barangay}', 'Api\BarangayController@show');

	Route::get('customers/search', 'Api\CustomerController@search');
	Route::resource('customers', 'Api\CustomerController', ['except' => ['edit', 'create']]);

	Route::get('pets/search', 'Api\PetController@search');
	Route::resource('pets', 'Api\PetController', ['except' => ['index', 'edit', 'create']]);

	Route::post('cages/add', 'Api\CageController@store');
	Route::post('cages/board', 'Api\CageController@board');
	Route::post('cages/reserve', 'Api\CageController@reserve');

	Route::put('cages/vacant/{cage}', 'Api\CageController@vacate');

	Route::resource('cages', 'Api\CageController', ['except' => ['edit', 'create', 'update', 'store']]);

	Route::put('appointments/{appointment}/status', 'Api\AppointmentController@status');
	Route::resource('appointments', 'Api\AppointmentController');

	Route::resource('medicals', 'Api\MedicalController');
	Route::resource('procedures', 'Api\ProcedureController');
	Route::resource('treatments', 'Api\TreatmentController');
});
